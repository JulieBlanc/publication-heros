
/* HEADER NAV TEXTES (LIST) */

function navTexteList(){

    textesList = document.getElementById("section-textes-list");   
    if(textesList){
        nbrArticles = parseFloat(getComputedStyle(textesList).getPropertyValue('--nbr-articles'));;

        /* Set article counter */
        let numArticle = document.getElementById("num-article").getElementsByTagName("span")[0];
        let newNum = articleCountScroll();
        numArticle.innerHTML = newNum;

        /* Add onclick functions */
        let prev = document.getElementById("home-prev");
        prev.addEventListener("click", prevFunction);

        let next = document.getElementById("home-next");
        next.addEventListener("click", nextFunction);

        /* CATEGORIES */

        let inputRadio = document.getElementsByName('input-category');
        for(let i = 0; i < inputRadio.length; i++){
            inputRadio[i].onclick = function(){
                /* scroll to start */
                window.scrollTo(0, 0);
                /* resize main list */
                nbrArticles = parseFloat(inputRadio[i].dataset.len);
                textesList.style.setProperty('--nbr-articles', nbrArticles);
                /* set article counter */
                let numTag = document.getElementById("num-article-tags");
                numTag.innerHTML = newNum;
            };
        }

    } 



}


/* --- ARTICLE COUNT ----------------------------------------------------------------------------------- 
   -----------------------------------------------------------------------------------------------
   ----------------------------------------------------------------------------------------------- */


function articleCountScroll(){
    /* Select only article with display block */
    let articles = document.querySelectorAll("article");
    let articlesVisible = [];
    for(let i = 0; i < articles.length; i++){
        let display = window.getComputedStyle(articles[i], null).getPropertyValue("display");
        if(display == 'block'){
            articlesVisible.push(articles[i]);
        }
    }
    /* Check if article if visible on window */
    let numsArticles = [];
    for(let i = 0; i < articlesVisible.length; i++){
        let rect = articlesVisible[i].getBoundingClientRect();
        let articleNbr = i + 1;
        if(rect.right <= window.innerWidth){
            numsArticles.push(articleNbr);
        }
    }
    /* get num of the last article visible */ 
    let num = numsArticles[numsArticles.length - 1];
    return num;
}

function setArticleCounter(){
    let numArticle = document.getElementById("num-article").getElementsByTagName("span")[0];
    let numArticleTag = document.getElementById("num-article-tags"); 
    let newNum = articleCountScroll();
    if(numArticleTag){
        numArticleTag.innerHTML = newNum;
    }else{
        numArticle.innerHTML = newNum;

    }
}




/* --- PREV & NEXT ARROWS ----------------------------------------------------------------------------------- 
   -----------------------------------------------------------------------------------------------
   ----------------------------------------------------------------------------------------------- */


function prevFunction() {
    let buttonPrev = document.getElementById("home-prev");

    /* Scroll calculation */
    let scrollX = window.scrollX;
    let articleW = document.querySelectorAll("article")[0].offsetWidth;
    let modulo = scrollX % articleW;

    let newScroll =  scrollX - modulo - 26/2;
    let scrollPrev = scrollX - modulo - articleW - 26/2;

    let maxScroll = Math.max(document.body.scrollWidth, document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth );
    maxScroll = maxScroll - window.innerWidth - 10;
    

    if(window.scrollX == newScroll){
        window.scrollTo(scrollPrev, 0);
    }else if(window.scrollX == 0){
        buttonPrev.classList.add("button-no");
    }else{
        window.scrollTo(newScroll, 0);
    }
}


function nextFunction() {
    let buttonPrev = document.getElementById("home-prev");

    /* Scroll calculations */
    let scrollX = window.scrollX;
    let articleW = document.querySelectorAll("article")[0].offsetWidth;
    let modulo = scrollX % articleW;

    let newScroll =  scrollX - modulo + articleW - 26/2;
    let scrollNext = scrollX - modulo + articleW*2 - 26/2;

    let maxScroll = Math.max( document.body.scrollWidth, document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth );
    maxScroll = maxScroll - window.innerWidth - 10;
    buttonPrev.classList.remove("button-no");

    /* Do this on click */
    if(window.scrollX == newScroll){
        window.scrollTo(scrollNext, 0);    
    }else if(window.scrollX > maxScroll){
        /* go to start */
        window.scrollTo(0, 0);
    }else{
        // window.scrollTo(newScroll, 0);
        window.scrollTo(scrollNext, 0);
    }
    
}


/* go to INTRODUCTION ----------------------------------------------------------------- */

function intro(){
    localStorage.setItem('goToIntro', true);
    window.location = '/';  
}


