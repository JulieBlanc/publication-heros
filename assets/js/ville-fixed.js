function villeFixed(){

    let anchors = document.getElementsByClassName("anchor-scrolly-ville");

    if(anchors){
        for(let a = 0; a < anchors.length; a++){ 

            let topAnchor = anchors[a].getBoundingClientRect().top;
            let dataAnchor = anchors[a].getAttribute('data-ville');
            let batiment = document.getElementById(dataAnchor).getElementsByClassName("batiment")[0];
            
            if(topAnchor < 460){
                let topBatiment = 100 - ((topAnchor - 460) * -0.2);
                if(topBatiment > 0){
                    batiment.style.top = topBatiment + "%";
                }
                
            }
         
        }
    }
 
}


