function parallax(){
    let parallaxElems = document.querySelectorAll(".parallax");
    for(let i = 0; i < parallaxElems.length; i++){
        let elem = parallaxElems[i];
        let layer1 = parallaxElems[i].querySelector(".layer1");
        let elemHeight = elem.getBoundingClientRect().height;
        let topElem = elem.getBoundingClientRect().top;
        let topLine = window.innerHeight*0.7;
        let bottomLine = 100;
        
        if(topElem < topLine){
            let dist = (0 - ((topElem - topLine) * -0.010))*-1;
            if(dist > -1){
                layer1.style.left = dist + "%";
            }
        }

   
    }


}