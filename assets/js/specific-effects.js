/* ScollyAlone -------------------------------------------------------------------- */

function buttonFullScreen(){
    let articles = document.querySelectorAll("article");
        for(let i = 0; i < articles.length; i++){
            let buttonFull = articles[i].querySelectorAll(".button-full-screen")[0];
            let allButtonFull = document.querySelectorAll(".button-full-screen");
            let limit = 100;
            articles[i].addEventListener('scroll', function(event){
                let scrollTop = articles[i].scrollTop;
                for(let a = 0; a < allButtonFull.length; a++){
                    allButtonFull[a].classList.remove("button-visible");
                }
                if(scrollTop > limit){
                    buttonFull.classList.add("button-visible");
                }
                
            });
        
        }
}



/* ScollyAlone -------------------------------------------------------------------- */

let iconeScrolly =  '<div class="icon-scroll">\
<div class="mouse">\
    <div class="wheel"></div>\
</div>\
<div class="icon-arrows">\
    <span></span>\
</div>\
</div>';


function scrollyAlone(pageType){
    let scrolly = document.getElementsByClassName("scrollyAlone");

    if(scrolly.length >= 1){
        for(let n = 0; n < scrolly.length; n++){
            
            if(pageType == "single" && window.innerWidth > 741){
               

            /* Récupérer les valeurs */
            let scrollyId = scrolly[n].id;
            let imgSrc = scrolly[n].dataset.imgLink;
            let imgType = scrolly[n].dataset.imgType;
            let imgNbr = parseInt(scrolly[n].dataset.imgNbr, 10);
            let vitesse = 10;
            let realWidth = parseInt(scrolly[n].dataset.w, 10);
            let realHeight = parseInt(scrolly[n].dataset.h, 10);

            /* créer la structure  */
            var container1 = document.createElement("div");
                container1.classList.add("container");
            var container2 = document.createElement("div");
                container2.classList.add("scrolly-container");
            var hack = document.createElement("div");
                hack.classList.add("hack-scroll");
      
            /* icone de scroll */
            var icone = document.createElement("div");
                icone.classList.add("container-icone");
                icone.innerHTML = iconeScrolly;

            /* ajouter les élements à la structure*/
            scrolly[n].appendChild(icone);
            scrolly[n].appendChild(container1).appendChild(container2);
            container1.appendChild(hack);

             /* hack scroll height */
            let heightHack = parseInt(vitesse * imgNbr);;
            hack.style.height = heightHack + "px";

        
            /* Ajouter les images */
            for(let i = 0; i < imgNbr; i++){
                let num = i + 1;
                var img = document.createElement("img");
                if(i == 0){
                    img.classList.add("img-visible");
                }
                img.src = "/uploads/" + imgSrc + num + "." + imgType;
                container2.appendChild(img);
            }

            /* modifier taille du conteneur */
            let firstimg = container2.querySelectorAll("img")[0];
            let firstimgWidth = firstimg.clientWidth;
            let imgNewHeight = firstimgWidth * realHeight / realWidth;
            container1.style.height = imgNewHeight + "px";

            /*  MOUSE ENTER */
            scrolly[n].addEventListener("mouseenter", function( event ) {
                event.preventDefault();
                this.classList.add("ready-to-scroll");
            }, false);


            /* ON SCROLL */
            scrolly[n].getElementsByClassName("container")[0].addEventListener('scroll', function() {
                if(scrolly[n].classList.contains("ready-to-scroll")){
                    scrollElem(this, imgNbr, 10);
                }
                
            });

           
            }


        }


    }
}




function scrollElem(e, imgNbr, vitesse){
    let max = imgNbr - 1;
    for(let n = 0; n < max; n++){
        if(e.scrollTop > n*vitesse && e.scrollTop <= (n+1)*vitesse){
            let images = e.getElementsByTagName("img");
            let visible = n + 1;
            for(let p = 0; p < images.length ; p++){
                images[p].classList.remove("img-visible");
            }
            images[visible].classList.add("img-visible");
            if(n == max - 1){
                e.style.cursor = "url('/images/scroll-top.png'), pointer";
            }else if(n > 0 && n < max - 1){
                e.style.cursor = "url('/images/scroll-bottom.png'), pointer";
            }        
        }
    } 
}



function resizeScrollyAlone(){
    let scrolly = document.getElementsByClassName("scrollyAlone");
    if(scrolly.length >= 1){
        for(let n = 0; n < scrolly.length; n++){
            let img = scrolly[n].getElementsByClassName("img-visible")[0];
            let container = scrolly[n].getElementsByClassName("container")[0];
            let realWidth = parseInt(scrolly[n].dataset.w, 10);
            let realHeight = parseInt(scrolly[n].dataset.h, 10);
            let firstimgWidth = img.clientWidth;
            let imgNewHeight = firstimgWidth * realHeight / realWidth;
            console.log(imgNewHeight);
            container.style.height = imgNewHeight + "px";
        }
    }
}



/* Display video on click on keyword -------------------------------------------- */

function keywordsVideo(){

    let keywords = document.querySelectorAll(".keyword");
    let single = document.getElementById("page-textes");
    
    if(keywords.length >= 1 && single){

        let aside = document.getElementById("aside-column");

        for(let i = 0; i < keywords.length; i++){
            let keyword = keywords[i];
            let video1 = keyword.getAttribute('data-video-1');
            let video2 = keyword.getAttribute('data-video-2');
            
            let newid = video1.split(".")[0];
            keyword.id = newid + "_" + i;

            var group = document.createElement("group");
            group.classList.add("group-video");
            group.id = "group-" + newid + "_" + i;

            if(video2){
                group.style.height = 'var(--height-group-videos)'
                group.classList.add("two-videos");
                group.innerHTML = '<video class="video-1" loop style="height: var(--height-videos)">\
                <source src="/uploads/dimitri-tati/' + video1 + '" type="video/mp4">\
                Your browser does not support the video tag.\
              </video>\
              <video class="video-2" loop style="height: var(--height-videos)">\
                <source src="/uploads/dimitri-tati/' + video2 + '" type="video/mp4">\
                Your browser does not support the video tag.\
              </video>'
            } else {
                group.innerHTML = '<video class="video-1" loop>\
                <source src="/uploads/dimitri-tati/' + video1 + '" type="video/mp4">\
                Your browser does not support the video tag.\
              </video>'
            }
            aside.appendChild(group);
            group.style.display = "none";
            

            

            keyword.onclick = function(){ 

                aside.style.display = "block";

                let allVideos = document.querySelectorAll(".group-video");
                let groupVideo = document.getElementById("group-" + this.id);

                let videoArticle = document.getElementsByClassName("content")[0].getElementsByTagName("video");
                for(let a = 0; a < videoArticle.length; a++){
                    videoArticle[a].pause();
                }


                if(groupVideo.classList.contains("play")){
                    groupVideo.style.display = "none";
                    stopVideos(groupVideo);
                    groupVideo.classList.remove("play");
                } else {
                    for(let v = 0; v < allVideos.length; v++){
                        allVideos[v].style.display = "none";
                        stopVideos(allVideos[v]);
                        allVideos[v].classList.remove("play");
                    }
                    groupVideo.style.display = "block";
                    playVideos(groupVideo);
                    groupVideo.classList.add("play");
                }
               
                
            };

            aside.onclick = function(){ 
               aside.style.display = "none"; 
               let allVideos = document.querySelectorAll(".group-video");
               for(let v = 0; v < allVideos.length; v++){
                allVideos[v].style.display = "none";
                stopVideos(allVideos[v]);
                allVideos[v].classList.remove("play");
                    }
            }

        }

        let groupVideoClick = document.querySelectorAll(".group-video");
        for(let v = 0; v < groupVideoClick.length; v++){
            groupVideoClick[v].onclick = function(){
            };
        }


    }
    

}

function playVideos(elem){
    let videos = elem.querySelectorAll("video");
    for(let i = 0; i < videos.length; i++){
        videos[i].currentTime = 0;
        videos[i].play();
    }
}

function stopVideos(elem){
    let videos = elem.querySelectorAll("video");
    for(let i = 0; i < videos.length; i++){
        videos[i].pause();
        videos[i].currentTime = 0;
    }
}



/* video Play on scroll ---------------------------------- */

function videoPlayOnScrollSingle(){
    let videos = document.querySelectorAll(".videoPlay");
    let windowH;
    for(let i = 0; i < videos.length; i++){
        let video = videos[i];
        let videoTop = video.getBoundingClientRect().top;
        let videoHeight = video.getBoundingClientRect().height;
        windowH = window.innerHeight;
        if(videoTop <= windowH*0.6){
            video.classList.remove('smallscale');
            if(!video.classList.contains('playvideo')){
                video.classList.add('playvideo');
                video.currentTime = 0;
                video.play();
            } 
        }
        if(videoTop >= windowH*0.6){
            videos[i].pause();
        }
        if(videoTop <= videoHeight*-0.5){
            videos[i].pause();            
        }
        if(videoTop >= videoHeight*-0.5 && videoTop <= windowH*0.6){
            videos[i].play();   
        }
        
       
    }

    
}