
//Function to handle swipes
function handleTouch(startX, startY, endX, endY, cbL, cbR){
  var xDist = endX - startX;
  var yDist = endY - startY;
   if(endX - startX < 0){
      cbL();
    }else{
      cbR();
    }
}




//writing the callback fn()
var left = () =>{   
    console.log('You swipped left!');
    let containerSingle = document.getElementsByClassName("container-main")[0];
    let prevURL = containerSingle.dataset.prev;
    window.location.href = prevURL;
}
var right = () =>{
    console.log('You swipped right!');
    let containerSingle = document.getElementsByClassName("container-main")[0];
    let nextURL = containerSingle.dataset.next;
    window.location.href = nextURL;

}
var up = () =>{
    console.log('You swipped up!');

}
var down = () =>{
    console.log('You swipped down!');

}

