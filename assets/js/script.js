
let click = 0;
let opacity = 0.3;
let nbrArticles;
let textesList;



window.onload = function(){


    let body = document.getElementsByTagName('body')[0].id;
    let pageType ;
    if(body && body == "page-textes"){
        pageType = "single";
    } else if(body && body == "page-list-textes") {
        pageType = "list";
    } else if(body && body == "page-home"){
      pageType = "home";
    } else {
        pageType = "other";
    }

    var userAgent = window.navigator.userAgent;
    if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
        /* SAFARI OS Mobile : mix-blend-mode on video doesn't works so replace with GIF */
        let headerFigures = document.querySelectorAll(".toggle-safari");
        for(let i = 0; i < headerFigures.length; i++){
            let video = headerFigures[i].querySelectorAll("video")[0];
            video.style.display = "none";
            let img = headerFigures[i].querySelectorAll("img")[0];
            img.style.display = "block";
        }
    }

   




    /* --- HOME ----------------------------------------------------------------------------------- 
    -----------------------------------------------------------------------------------------------
    ----------------------------------------------------------------------------------------------- */

    if(pageType == "home"){
        // afficher introduction si demandé depuis une autre page
       if(localStorage.getItem('goToIntro') === "true"){
           document.getElementById("input-introduction").checked = true;
           localStorage.setItem('goToIntro', false);
       }else{
        document.getElementById("input-introduction").checked = false;
       }
       // reset translate-texte
       localStorage.setItem('translate-texte', 0);
   }

    
   

   /* --- TEXT LIST ----------------------------------------------------------------------------------- 
   -----------------------------------------------------------------------------------------------
   ----------------------------------------------------------------------------------------------- */

    if(pageType == "list") {
        if (!userAgent.match(/iPad/i) || !userAgent.match(/iPhone/i)) { 
            navTexteList();
            buttonFullScreen();
            window.onscroll = function(){
                setArticleCounter();
            } 
        }
       
   }

  




   /* --- SINGLE ----------------------------------------------------------------------------------- 
   -----------------------------------------------------------------------------------------------
   ----------------------------------------------------------------------------------------------- */
   
   lazyVideo();


   if(pageType == "single"){


   
    var gesuredZone = document.getElementById('main-textes');
    console.log(gesuredZone);

    var startX
    var startY
    var endX
    var endY
    var treshold = 100; //this sets the minimum swipe distance, to avoid noise and to filter actual swipes from just moving fingers
    
    
    gesuredZone.addEventListener('touchstart', function(event){
       startX = event.touches[0].clientX;
       startY = event.touches[0].clientY;
     })
      
     gesuredZone.addEventListener('touchend', function(event){
       endX = event.changedTouches[0].clientX;
       endY = event.changedTouches[0].clientY;
       handleTouch(startX, startY, endX, endY, left, right)
     })


       /* ON SCROLL*/
       window.addEventListener('scroll', function() {
           videoPlayOnScrollSingle(); 
           villeFixed();
           parallax();
       });


       videoPlay(); 
       scrollyAlone(pageType);

   }


   /* --- ALL ----------------------------------------------------------------------------------- 
   -----------------------------------------------------------------------------------------------
   ----------------------------------------------------------------------------------------------- */
   

   regexTypo();
   exposants();
   footnotes();
   keywordsVideo(pageType); 
    
    

} /* window onload */




window.onresize = function(){
    resizeScrollyAlone();
    if(document.getElementsByTagName('body')[0].id == "page-list-textes"){
        setArticleCounter();
    }
}

