function lazyVideo(){

    var lazyVideos = [].slice.call(document.querySelectorAll("video.lazy"));
  
    if ("IntersectionObserver" in window) {
      var lazyVideoObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(video) {
          if (video.isIntersecting) {
            for (var source in video.target.children) {
              var videoSource = video.target.children[source];
              if (typeof videoSource.tagName === "string" && videoSource.tagName === "SOURCE") {
                videoSource.src = videoSource.dataset.src;
              }
            }
  
            video.target.load();
            video.target.classList.remove("lazy");
            lazyVideoObserver.unobserve(video.target);
          }
        });
      });
  
      lazyVideos.forEach(function(lazyVideo) {
        lazyVideoObserver.observe(lazyVideo);
      });
    }
}

function videoPlay(){
  let videoPlay = document.querySelectorAll(".videoPlay");
        
        if(videoPlay.length >= 1){
            for(let i = 0; i < videoPlay.length; i++){
                let video = videoPlay[i];
                video.classList.add('smallscale');
                video.muted = "muted";

                /* mettre le son au premier clic */
                let button = video.parentElement.querySelectorAll(".click-unmuted")[0];               

                if(button){
                    video.addEventListener('mouseenter', function() {
                        button.style.height = video.parentNode.getBoundingClientRect().height + "px";
                        button.style.width = video.parentNode.getBoundingClientRect().width + "px";
                        button.style.opacity = 0.8;
                    });
                    button.addEventListener('click', function() {
                        button.style.zIndex = "-1";
                        video.muted = !video.muted;
                    });        
                }

              
            }
            
        } 
}