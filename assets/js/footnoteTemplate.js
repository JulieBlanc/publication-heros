
/* FUNCTION FOOTNOTE */

function footnotes(){
    let articles = document.getElementsByTagName("article");
    
    for(let i = 0; i < articles.length; i++){
        let id = articles[i].id;
        footnoteTemplate(articles[i], id);
    }

}

function footnoteTemplate(article, id){
    let stringftnref = '#_ftnref';
    let stringftncall = '#_ftn';
    let footnotes = article.querySelectorAll('*[href^="' + stringftnref + '"]');
    
    if(footnotes.length >= 1){

        /* create container */
        let containerfootnotes = document.createElement("aside");
        containerfootnotes.classList.add("footnote-container");
        footnotes[0].parentNode.parentNode.insertBefore(containerfootnotes, footnotes[0].parentNode);
        let asideFootnote = article.querySelectorAll(".footnote-container")[0];
        
        for(let i = 0; i < footnotes.length; i++){
            let footnote = footnotes[i].parentNode;
            let linkCall = footnotes[i].href.split(stringftnref)[1];
            let num = i + 1;

            footnotes[i].remove();

            /* footnote call */
            let ftnCall = article.querySelectorAll('*[href^="' + stringftncall + linkCall + '"]')[0];
            if (ftnCall){

       
            ftnCall.innerHTML = num;
            ftnCall.classList.add("fnt-call");
            ftnCall.href = "#" + id + "_ftn_" + num;
            ftnCall.id = id + "_ftn-call_" + num;
        }
           
            /* add footnote id + counter */
            footnote.id = id + "_ftn_" + num;
            footnote.innerHTML = '<span class="ftn-marker">' + num + '</span>' + footnote.innerHTML + '<span class="ftn-return"><a href="#' + id + "_ftn-call_" + num + '">↩</a></span>';
            
            /* move footnote into container */
            asideFootnote.appendChild(footnote);
        }
    }

}