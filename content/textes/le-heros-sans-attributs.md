+++
author = "Clément Schneider"
bio = "Né en 1989, Clément Schneider entre à la Fémis en 2009, dans le département Réalisation. À la sortie de l’école, il fonde Les Films d’Argile, afin de préserver indépendance et autonomie dans son activité de cinéaste, tout en travaillant comme projectionniste. En 2018, il réalise _Un violent désir de_ bonheur, sélectionné à Cannes (ACID). Il mène au sein de l’université PSL, un travail de thèse sur un sujet qui le questionne depuis longtemps : _les relations entre le cinéma et l’utopie._"
categories = ["cinema", "pratique"]
class = "center-content"
contribution = true
cover = "clement/derniere-douane_cover.jpg"
creditsPhoto = "Figure 1 : Malo Martin, Rosalie Comby, in <em>La dernière douane</em>, 2020, © Les Films d'Argile"
date = ""
identifiant = "clement"
subtitle = ""
title = "Le héros sans attributs"
weight = 1

+++
> Pendant ce temps-là, vêtu d'un simple jean bleu, Orphée descend très haut, jusqu'au vouvoiement[\[1\]](#_ftn1).

Lorsque le cinéma convoque des figures mythologiques, la question qui se pose immanquablement est celle du degré de transposition du mythe dans la diégèse. Il y aurait par exemple, d'un côté la Junon du _Conte de Noël_ d'Arnaud Desplechin (2008), incarnée par Catherine Deneuve : le prénom étrange du personnage souligne sa puissance de matriarche, l'ambivalence du rapport à ses enfants… mais le mythe reste de l'ordre de l'allusif : juste un prénom. De l'autre côté du spectre, il y aurait un film comme _Parking_ de Jacques Demy (1985) dans lequel le cinéaste a cherché une équivalence, terme à terme, du récit mythique et de ses signes : Orphée, interprété par Francis Huster, y est une rock-star, il joue de la guitare, la sortie des Enfers est figurée par un tunnel de parking au bout duquel brille la lumière. Entre ces deux pôles existe, bien entendu, une grande variété de gradients.

Pris dans cette tension entre allusivité et littéralité, le cinéaste qui invoque un héros mythique est confronté à un dilemme complexe : trop d'allusivité risque de décharger le personnage de son potentiel mythologique (ces couches invisibles qui lui donnent une épaisseur que sa normalité apparente masque) ; à l'inverse, trop de littéralité verrouille le personnage dans une pure fonction allégorique, qui le désincarne et ne laisse au spectateur plus aucun espace pour élaborer une interprétation…

En entreprenant _La dernière douane_ (2020), court-métrage qui se voulait une variation autour du mythe d'Orphée et d'Eurydice, j'ai été confronté à ce dilemme-là. Comment signaler la part du mythe dans le récit sans la signer lourdement ? Comme souvent au cinéma, la solution est venue des contraintes économiques dans lesquelles le film a été tourné. Le budget réel de fabrication s'est avéré très en dessous de ce que nous avions imaginé et il a fallu raisonner dans une logique de dénuement, réduisant le film à ses éléments essentiels. J'ai donc décidé de récrire complètement le scénario.

Dans ces moments, on revient au désir premier, aux premières images, aux intuitions initiales. Je me suis alors rappelé que le mythe d'Orphée n'était pas **vraiment** le point de départ du film. Le point de départ était un « nom de pays », pour parler comme Proust, en l'occurrence Cerbère, petite station balnéaire des Pyrénées-Orientales, au bout d'une ligne de train de nuit dont la SCNF venait d'annoncer la fin prochaine de l'exploitation. C'était en 2017.

Ce train de nuit, je l'avais beaucoup pris plus jeune, j'y avais le souvenir de sensations de voyage très précises, cinématographiques : celles de passer d'un monde à l'autre, le temps d'une nuit. Or, que le point d'arrivée de ce train soit Cerbère, à la **frontière** entre la France et l'Espagne, m'avait toujours semblé une coïncidence trop belle pour être aléatoire. Comme Proust face à l'indicatif des chemins de fer, le nom de la station dépliait dans mon esprit un monde imaginaire où il était question d'Enfers, de passage, de seuils… Et alors, presque « naturellement », Orphée s'était invité dans ce désir de film, comme en contrebande.

L'autre désir présidant à la naissance de ce film était celui d'un médium, en l'occurrence, la 3D-relief. Parce que la 3D, en ce qu'elle ouvre au regard du spectateur un espace à creuser, à fouiller, à explorer, me semblait la forme la plus adéquate pour matérialiser le désir. Dans le mythe d'Orphée, le désir est lié à ce qui, pour Bataille, fonde l'érotisme, à savoir la transgression d'un interdit : regarder Eurydice, derrière lui… Or, quelle plus belle manière de figurer cette transgression que la 3D, la profondeur de champ ?

Autour de ces quelques intuitions s'est construite la forme poétique finale du film, tout en jeux de rimes, d'échos, une image en appelant une autre par association d'idées, entre rêve et souvenir. L'Orphée du film, si c'en est un, n'a pas de nom, pas de lyre et il n'endort pas les animaux par le simple pouvoir de sa voix. C'est un jeune homme au regard triste, qui part en train de nuit à la recherche d'une femme, dans ce bout du monde qui s'appelle Cerbère ; il erre dans les limbes de chambres d'hôtels ; un regard, pourtant, un regard interdit fera disparaître la femme aimée. Ainsi dépouillé de ses attributs, cet homme a-t-il encore à voir avec le héros du mythe ? À défaut de répondre, je dirai simplement, qu'il en est l'empreinte, le souvenir : une ombre invoquée par le pouvoir magique de la mémoire et du film.


{{% figure class="" img="clement/derniere-douane" %}}{{% /figure %}}


***

[\[1\]](#_ftnref1) Majerská, Silvia, « Bleu » in _Matin sur le soleil_, Le Cadran Ligné, Saint-Clément, 2020.