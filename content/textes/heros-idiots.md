+++
author = "Maxime Berthou"
bio = "Maxime Berthou, Français né en 1981, sort diplômé de l’école supérieure d’art d’Aix-en-Provence avant d’intégrer le post-diplôme du Fresnoy Studio National des Arts contemporain de Tourcoing puis de suivre une formation pré-doctorale aux Arts Décoratifs de Paris. Sa pratique artistique consiste à réaliser des essais cinématographiques à partir de l’expérience vécue lors de gestes performatifs. Son travail s’inscrit dans un contexte de recherche basé sur la pratique superposant un cadre artistique à un cadre scientifique."
categories = ["pratique", "performance"]
class = "has-aside-content"
contribution = true
coverVideo = "maxime/bateau-cover.mp4"
cover = "maxime/bateau-cover.gif"
date = ""
identifiant = "maxime"
subtitle = ""
title = "Héros idiots"
weight = 9

+++
{{% divclass class="group" %}}
{{% divclass class="group-content" %}}

> Ils ne savaient pas que c'était impossible, <br>alors ils l'ont&nbsp;fait.

Cette citation apocryphe attribuée à [Mark Twain](https://dicocitations.lemonde.fr/citations/citation-29739.php) a peut-être été imaginée alors qu'il écrivait les aventures de _Tom Sawyer_ ou d’_Huckelberry Finn_ sur les rives du Mississippi. Elle fait écho à deux projets de recherche-création — _Hogshead 733_ et _Southwind —_ associant performance et cinéma, que j’ai menés entre 2012 et 2020, dont l’un à bord d’un bateau sur ce même fleuve. Ces deux projets dessinent ensemble la figure d’un « héros idiot », un artiste fort de son ignorance, navigant entre la réalisation d’une forme d’exploit nourri par des récits d’aventures et la confrontation avec un réel souvent plus dur et complexe.

Le travail artistique que je propose s'appuie sur des performances impliquant un investissement de ma personne allant parfois jusqu’à la mise en danger. J’utilise invariablement, pour retranscrire ces expériences, les outils du cinéma, comme l’ont fait avant moi Bas Jan Ader, Chris Burden ou plus récemment Guido Van der Werve.

{{% /divclass %}}
{{% /divclass %}}


{{% divclass class="group" %}}

<div class="group-aside" id="bateau">
<div class="scrollyAlone" id="scrollbateau" data-img-link="maxime/scrollbateau/scrollbateau_" data-img-type="png" data-img-nbr="200" data-vitesse="10" data-w="381" data-h="800"></div>
    <img src="/uploads/maxime/bateau.gif" loading="lazy" class="gif-page-column">
</div>

{{% divclass class="group-content" %}}

C'est dans ce contexte où l’artiste est à la fois sujet, auteur et réalisateur de leur travail que se situe le premier projet dont il est question aujourd’hui, [_Hogshead 733_](https://web.archive.org/web/20201128194650/http://www.hogshead733.com/) .

Initié en 2012, il a consisté à acheter un vieux voilier bigouden en bois de 1941 avant de décontaminer patiemment sa coque pendant deux ans. Après l'avoir libéré de toute trace de peintures et d'hydrocarbures, Mark Pozlep et moi-même avons pris la mer à son bord depuis Trebeurden en Bretagne jusqu'à l'île d'Islay en Écosse. Un voyage de 45 jours dans une mer capricieuse pour arriver au pied d'une distillerie. Là-bas, le bateau a été immédiatement sorti de l'eau et sa coque découpée afin de construire deux fûts : bâbord et tribord. Les deux tonneaux ont ensuite été remplis de whisky pour qu'ils s’imprègnent de l'eau de mer du voyage, du vent, du sel et de l'aventure.

À partir de ce projet a été réalisé le film, _Soutien de famille,_ essai cinématographique ancré dans la temporalité de _Hogshead 733_. Sans être simplement une documentation de la performance, il s'agit d'un essai fondé sur la relation entre mon coéquipier et moi même. L’essai se définissant comme une démarche introspective dont le but est de prendre la mesure de sa propre pensée. Les voyages en bateau sont réputés pour exacerber les caractères : des gens qui se connaissent très bien peuvent rapidement perdre leur sang froid lors d'une traversée. Lorsque nous avons commencé notre périple, nous nous connaissions à peine et le voyage a été tellement éprouvant que cela nous a forcés à construire et entretenir un lien très fort, premièrement pour survivre, deuxièmement pour aller au bout du projet.

Le film retrace cette évolution.

{{% /divclass %}}
{{% /divclass %}}

{{% divclass class="full-video" %}}
<video controls class="videoPlay lazy" poster="/uploads/maxime/extrait-1_poster.png">
    <source data-src="/uploads/maxime/extrait-1.mp4" type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>
<button class="click-unmuted">Cliquez pour entendre le son</button>
{{% /divclass %}}

{{% divclass class="group" %}}
{{% divclass class="group-content" %}}

Cette séquence clôt la description de la première étape du voyage qui a consisté à traverser la Manche depuis Trébeurden en Bretagne jusqu’à Plymouth en Angleterre. Ponctuée par deux tempêtes, la totalité du matériel de sécurité – réflecteurs radars, radio VHF, moteur, lampes, etc. a été détruite lors de cette navigation. Le bateau et son équipage étant devenus invisibles pour les tankers croisant à cet endroit, nous n’eûmes d'autre choix que de compter l'un sur l'autre pour traverser de nuit sains et saufs la route maritime la plus fréquentée du monde.

Au petit matin, Mark apparaît marqué par la nuit, effrayé et hagard. Se sont les seules images de cette nuit dont nous disposons : aucune image n'a été tournée lors des tempêtes, car le danger imposait de nous concentrer sur notre sécurité. Pourtant, à plusieurs reprises, j'ai essayé de sortir la caméra mais Mark me disait aussitôt d’arrêter car nous risquions notre vie. Il a ensuite perdu connaissance. Après avoir vérifié qu'il était toujours vivant, je l'ai laissé assommé quelques minutes, espérant qu'il reprenne des forces. Lorsque je lui ai crié qu'il était temps d'essayer de remettre la voile, Mark s'est réveillé, ses vêtements trempés par sa propre urine. Il est tout à fait possible de traverser la Manche, même si les conditions dans lesquelles nous nous sommes retrouvés dépassaient largement nos capacités — rendant la modalité de notre voyage impossible. Ce sont les mêmes difficultés que rencontrent les vaisseaux de fortunes utilisés par les migrants sur cette même route – ce qui achève de distinguer notre expérience d'un exploit.

{{% /divclass %}}
{{% /divclass %}}


{{% divclass class="group" %}}

<!-- <div class="group-aside">
<p>//gif roue colonne//</p>
</div> -->

<div class="group-aside" id="roue">
<div class="scrollyAlone" id="scrollroue" data-img-link="maxime/scrollroue/ScrollRoue_" data-img-type="jpg" data-img-nbr="50" data-vitesse="30" data-w="800" data-h="450"></div>
    <img src="/uploads/maxime/roue.gif" loading="lazy" class="gif-page-column">
</div>

{{% divclass class="group-content" %}}

Avec le second projet, [_Southwind_](https://web.archive.org/web/20201128084559/http://www.southwind-project.com/), je m’étais posé comme défi de descendre à l'automne 2019 le Mississippi dans un petit bateau à vapeur. Ce projet nous positionne Mark et moi à nouveau en « héros-idiots » du récit. Voyager sur ce fleuve puissant à bord d'un bateau à la motorisation archaïque, sans aucune expérience du fonctionnement d'une machine à vapeur, trahissant un certain manque de bon sens. Pourtant, cette posture maladroite était volontaire et assumée. Elle est même l'élément fondamental de ma méthodologie de recherche-action. En nous plaçant ainsi, il est en effet devenu possible d’entrer en interaction avec l’environnement social du fleuve, avec les populations attirées par la présence anachronique du bateau – à la fois symbole de la culture populaire et objet de fantasme. Chaque rencontre a été un prétexte pour évoquer l'histoire du fleuve et [collecter]( http://www.southwind-project.com/en/collected-data/collected-data) [des](https://www.facebook.com/outofsightantwerp/videos/880178312555659) [données]( https://www.centrepompidou.fr/cpv/agenda/event.action?param.id=FR_R-754bbd17a7339d3573a1740d110f62c&param.idSource=FR_E-754bbd17a7339d3573a1740d110f62c) .

Nous avons procédé également à un autre type de collecte : celle des 42 variétés de maïs présente sur les rives du Mississippi pour _in fine_ produire du [_moonshine_](https://flaviar.com/southwind-project), alcool modeste de la Prohibition devenu un alcool populaire emblématique de la culture américaine.

Le projet fut méthodiquement filmé depuis la rénovation du bateau jusqu'à la transformation du maïs en passant par les 65 jours de voyage à 6 km/h sur l'un des plus longs fleuves du monde. À cette matière s'ajouteront d'autres données collectées pour construire un essai cinématographique encore en devenir.

{{% /divclass %}}
{{% /divclass %}}


{{% divclass class="full-video" %}}
<video controls class="videoPlay lazy" poster="/uploads/maxime/mississipi02_poster.png">
    <source data-src="/uploads/maxime/mississipi02.mp4" type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>
<button class="click-unmuted">Cliquez pour entendre le son</button>
{{% /divclass %}}