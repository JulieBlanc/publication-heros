+++
author = "Dimitri Martin Genaudeau"
bio = "Dimitri Martin Genaudeau a étudié la philosophie et l’histoire du cinéma à l’Université Paris 1 Panthéon-Sorbonne et à la City University de Hong Kong, il a passé un an à Prague dans la section de réalisation documentaire de la FAMU avant de rejoindre le doctorat SACRe à la Fémis en 2018 où il mène depuis une thèse de recherche et de création sur le cinéma burlesque ; il est membre du laboratoire SACRe."
categories = ["cinema", "analyse"]
class = "has-aside-content"
contribution = true
cover = "dimitri-1/gifTheRink_column.gif"
coverVideo = "dimitri-1/gifTheRink.mp4"
creditsPhoto = "<em>The Rink, The Floorwalker, The Fireman, The Vagabond, The Count, The Pawnshop</em> © Charlie Chaplin, Arte France Développement – Lobster Films, 2013. Versions restaurées et trade marks © Film Preservation Associates Inc, 2013. <br> <em>City Light </em>© Charlie Chaplin, Roy Export S.A.S., 1931. Renewed : © Roy Export S.A.S., 1958. Licensed Rights : © A-Film Distribution, 2011. <br> <em>The Gold Rush</em>© Charlie Chaplin Roy Export S.A.S., 1942. Renewed : © Roy Export S.A.S., 1969. Licensed Rights : © A-Film Distribution, 2010."
date = ""
identifiant = "dimitri-1"
subtitle = "À propos d’un plan de *The&nbsp;Rink* (*Charlot&nbsp;patine*, 1916) de&nbsp;Charlie Chaplin"
title = "L’homme hors de lui-même"
weight = 15

+++
{{% divclass class="group" %}}

<div class="group-aside group-only-aside">
<div class="scrollyAlone sticky-aside" id="dimitri-chaplin-img" data-img-link="dimitri-1/dimitri-chaplin/dimitri-chaplin-img" data-img-type="png" data-img-nbr="55" data-vitesse="50" data-w="1310" data-h="1080">
<!-- <img src="/uploads/dimitri-1/gifTheRink.gif" loading="lazy"> -->
</div>
</div>

{{% divclass class="group-content" %}}


Le chapeau droit sur le crâne, la badine oblique, garrotté d’un col blanc à la lisière du menton, Charlot, dynamique, quitte les cuisines à la fin de son service. Il avance jusqu’au premier plan, jette son pied gauche vers la droite d’un coup sec, entraînant le reste du corps dans un virage à angle droit d’une précision géométrique, et disparaît derrière une porte battante. On sait la détermination qu’il fallut à Chaplin pour imposer ce type de plan ; deux ans plus tôt, en 1914, sous contrat à la Keystone, l’acteur est dirigé par des metteurs en scène comme Henry Lehrman, peu sensibles à ses talents de mime – les comédies sont trépidantes, le montage, convulsif. Très vite, Chaplin comprend que son talent ne peut s’exprimer à une telle cadence : pour déjouer les ciseaux des monteurs qui coupent ses meilleures scènes, l’acteur entre par le fond du décor, occupe la profondeur de champ et la largeur du cadre pour allonger au maximum sa présence à l’écran[\[1\]](#_ftn1). En 1916, lorsqu’il tourne _The Rink_ (_Charlot patine_), dont ce plan est extrait, il est l’acteur le mieux payé d’Hollywood, réalise ses propres comédies et sa seule démarche suffit à provoquer l’hilarité des foules.

<div class="group-aside figure-desktop aside-center">
<div class="scrollyAlone sticky-aside" id="dimitri-chaplin-img" data-img-link="dimitri-1/dimitri-chaplin/dimitri-chaplin-img" data-img-type="png" data-img-nbr="55" data-vitesse="50" data-w="1310" data-h="1080">
</div>
</div>

<figure class="figure figure-phone">
  <img src="/uploads/dimitri-1/gifTheRink_column.gif" loading="lazy">
</figure>

<!-- <figure class="figure column-only">
  <img src="/uploads/dimitri-1/gifTheRink_column.gif" loading="lazy">
</figure> -->

Tout dans sa gestuelle est une danse[\[2\]](#_ftn2) : les pieds en canard forcent l’arc des genoux qui entraîne l’ondulation des hanches, donne à la démarche son rythme chaloupé et détermine cet espace triangulaire qui jamais ne se comble entre les pattes du vagabond – une jambe droite et l’autre pliée – cette fente ou cette fracture de la silhouette, qui prolonge celle de la veste, ouverte au niveau du ventre, répète les multiples contradictions du personnage, abondamment commentées, froc trop grand et veston trop petit du vagabond-gentleman. Ce costume, cette silhouette et cette démarche nous sont si familiers qu’il est n’est pas aisé de remarquer cette soudaine dissonance : dans les dernières secondes de ce plan, lorsque Charlot se tourne de profil face à la caméra pour quitter la cuisine du restaurant, il marche autrement. Les pieds reviennent légèrement dans leur axe naturel, les genoux se resserrent, le vagabond allonge le pas et cette démarche tantôt si singulière semble tout à coup presque normale, un peu banale.

Il ne s’agit pas d’une curiosité isolée dans l’œuvre : que l’on prenne chaque plan, de chaque film, dans lequel Charlot se déplace et l’on verra que le personnage, filmé de profil ou de face, ne marche pas tout à fait de la même manière – il fallait l’œil expert d’Adolphe Nysenholc pour relever un tel détail, geste constant et pourtant imperceptible[\[3\]](#_ftn3). Verrait-on Marlon Brando ou Clint Eastwood changer de la sorte un trait si essentiel de leur maintien suivant la façon dont ils sont cadrés ? La cohérence psychologique ne voudrait-elle pas qu’un personnage se déplace toujours de la même manière, quel que soit l’angle selon lequel il est filmé ? Certes, mais ce qui importe avant tout à Chaplin c’est de maintenir l’intégrité de sa silhouette en toutes circonstances, et notamment l’écart qui sépare les jambes de son personnage : que l’on fasse marcher un homme en canard en le filmant de profil, chaque jambe se pliant sur le côté, son corps formera une masse indistincte – en modifiant sa démarche lorsqu’il apparaît de profil, Chaplin préserve cet écart qui le rend reconnaissable. Si nous ne percevons pas ce changement c’est qu’il conserve la silhouette de Charlot d’un plan à l’autre, indépendamment des choix de cadrage et des films eux-mêmes, il instaure une continuité en introduisant une rupture et confirme, pour un spectateur imprégné du folklore chaplinien, la stature mythologique du personnage, figure autonome, libérée de son ancrage filmique.

<video controls class="video-figure lazy single-only" preload="none" poster="/uploads/dimitri-1/charlot2_poster.png">
    <source data-src="/uploads/dimitri-1/charlot2.mp4" type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>
<video controls class="video-figure lazy column-only" preload="none" poster="/uploads/dimitri-1/charlot2_poster.png">
    <source data-src="/uploads/dimitri-1/charlot2_400.mp4" type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>

Bien sûr, ce n’est pas un trait saillant, il ne s’agit pas d’un effet comique, comme l’une de ses aberrations physiques que les grands techniciens du rire prennent plaisir à mettre en scène, mais d’un détail insensible qui n’a pas vocation à être remarqué par le spectateur – Chaplin souhaite en ce point une certaine discrétion ; pourtant, c’est dans cette vétille que se dévoile toute l’originalité du héros burlesque : le ciment de son être n’est ni son humeur, ni tellement ses émotions, ce n’est pas la psychologie du personnage qui le rend si singulier et si reconnaissable, c’est sa forme ; avant toute chose, Charlot est un chapeau, une badine, des bottines trop larges, une certaine manière de marcher et de hausser les épaules et si l’on doit le trouver espiègle, parfois lâche et cruel ou délicieusement charmeur ce n’est qu’en second lieu – comme si ses goûts, ses angoisses et ses caprices n’étaient qu’une conséquence de son apparence ; la fente qui déchire la silhouette du vagabond n’est en somme que le signe extérieur du déchirement intérieur au prix duquel, paradoxalement, le personnage éprouve sa constance et sa plénitude formelle et se maintient, entier, semblable à lui-même, d’un plan à l’autre.

Curieusement, le héros burlesque, chez Chaplin ou chez d’autres (notamment Buster Keaton), ne semble percevoir son propre corps qu’en spectateur, à travers l’œil de la caméra, en deux dimensions. C’est en raison de cette discontinuité psychique – un personnage dont la conscience se partage, en somme, entre le film et la salle de cinéma – que l’impertinence du héros se manifeste en premier lieu, sinon exclusivement, dans l’indifférence qu’il manifeste à l’égard de la situation dramatique dans laquelle son corps est investi et des motifs psychologiques qui devraient normalement diriger son action. Il ne cesse d’en remettre en cause la réalité, le sérieux et l’importance, d’en bousculer la crédibilité, par des clins d’œil et des apostrophes adressés à la caméra, par la surprise qu’il affiche souvent devant les illusions et les cadrages trompeurs dont la mise en scène réserve en principe le privilège au spectateur[\[4\]](#_ftn4), ou simplement par une discrète variation de sa dégaine, comme dans le plan qui nous occupe.

C’est avec raison que l’on a parlé de Charlot comme d’un homme-montage, une machine vivante, une incarnation humaine du cinématographe : montage des attractions que le corps de l’acteur prend en charge, à l’intérieur du film, par la libre association d’êtres et de choses à laquelle s’emploie la malice du personnage[\[5\]](#_ftn5). Grand artificier de l’espace du cadre et spectateur complaisant de sa propre virtuosité, le narcissisme apparent du héros burlesque passerait à première vue pour l’éloge cynique d’un anthropocentrisme carnassier : il n’en est rien. L’indéniable humanisme qui jaillit de cette célébration des possibilités du corps, de la poésie du geste pur, n’est jamais dominateur ; au contraire, le burlesque se révèle un genre singulièrement perméable au monde extérieur et d’un appétit peu commun pour le dépaysement radical des êtres et des choses _autres qu’humains_ : s’il est technophile, c’est par goût de la fantaisie mécaniste, sans autre but ni raison d’être que la beauté des engrenages et des manivelles, loin de la mythologie stérile du progrès et des rêves insipides du posthumanisme où se ternit le lyrisme des machines au profit d’un utilitarisme glaçant ; de même est-il zoophile (les animaux abondent dans le burlesque) car c’est bien l’animal qui offre l’image la plus fidèle de ce héros à la psychologie déliée, mais dont la silhouette familière inspire une fraternité naturelle : la conscience des bêtes, comme la sienne, est hors de portée, mais leurs vulnérabilités sensibles, leurs souffrances et leurs plaisirs, sont les nôtres.

C’est tout l’exotisme bizarre du burlesque qui est en embuscade dans ce seul plan de Chaplin et dans l’évolution indiscernable de sa démarche : si les objets, les machines, les animaux existent avec un tel réalisme dans ces films, une brutalité authentique, une gratuité que ne viennent gâter ni les idéologies, ni les aspirations politiques, ni aucune autre motivation que celle du plaisir et de la jouissance, c’est que le burlesque a su regarder l’homme, avant toute chose, comme la bête la plus étrange de toute, mettre l’épopée dans l’ombre d’un héros sans dessein tout en célébrant l’exil, le déracinement, le voyage de l’homme hors de lui-même.

{{% /divclass %}}
{{% /divclass %}}

***

[\[1\]](#_ftnref1) Francis Bordat, _Chaplin cinéaste_, Éditions du Cerf, Collection « 7ème Art », Paris, 1998, pp. 103-104 et Charles Chaplin, _Histoire de ma vie_, Éditions Robert Laffont, Collection « Presses Pocket », Paris, 1964, p. 181.

[\[2\]](#_ftnref2) Vaslav Nijinsky, que Chaplin invite à son studio la même année, ne s’était pas trompé sur ce point ; après l’avoir observé au travail une journée durant, il déclare : « Votre comédie tient du ballet… Vous êtes un danseur », Charles Chaplin, _ibid._, p. 234.

[\[3\]](#_ftnref3) Adolphe Nysenholc, _Charles Chaplin : l’âge d’or du comique_, Éditions L’Harmattan, Collection « L’Œuvre et la Psyché », Paris, 2002, pp. 17-18.

[\[4\]](#_ftnref4) Dans _Sherlock Jr._ (_Sherlock Junior_, 1924), Buster Keaton prend un homme en filature, il le suit pas à pas jusqu’au moment où celui-ci emprunte un petit escalier pour atteindre le quai d’une gare : alors même que Buster semble se tenir exactement derrière lui, il manque l’escalier et continue tout droit jusqu’à heurter un mur de plein fouet ; filmé de profil, l’escalier cachait un espace invisible entre les marches et le mur, Buster réagit comme s’il était victime d’une illusion qui en réalité ne peut être que l’illusion du spectateur puisqu’elle est entièrement due au cadrage et à la superposition des plans dans la profondeur de champs. On trouve des effets similaires chez Harold Lloyd, Jean-Pierre Coursodon parle à ce sujet de « cadrage mensonger », in _Buster Keaton_, Éditions Atlas et Éditions Pierre Lherminier, Paris, 1986, p. 256.

[\[5\]](#_ftnref5) Adolphe Nysenholc, _ibid._, p. 64