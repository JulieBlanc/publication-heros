+++
author = "Natalia Baudoin"
bio = "Natalia Baudoin est artiste designer bolivienne et vénézuélienne, actuellement doctorante SACRe au sein des groupes Symbiose et Soft Matters de ENSADLab. Sa thèse « Design Convergent, Bricologies Symbiotiques » étudie la valorisation de l’artisanat traditionnel en France et en Amérique Latine par le design. Son travail de recherche-création consiste à interroger les domaines d’application de ces savoirs artisanaux autochtones pour les intégrer à notre système de production tout en respectant leur culture et leur cosmologie. Elle travaille également en partenariat avec des banques solidaires en vue de favoriser l’intégration socio-économique des communautés d’artisans d’Amérique Latine."
categories = ["design", "analyse"]
class = "center-content"
contribution = true
cover = "natalia/natalia-cover.png"
creditsPhoto = "Figure 1: Ph. Freddy Cutili Chuquimia Source: https://commons.wikimedia.org/<wbr>wiki/<wbr>File:ILLIMANI_DESDE_EL_ALTO.JPG <br> Figure 2: Ph. Natalia Baudoin<br> Figure 3: Teresa Gisbert, <em>Iconografía y mitos indígenas en el arte</em>, 2004, page 61.<br> Figure 4: Teresa Gisbert, <em>Iconografía y mitos indígenas en el arte</em>, 2004, fig.59. <br> Figure 5: Ph. Alfredo Zeballos <em>Catalogue de l’exposition Géométries Sud, Du Mexique à la Terre de feu </em>, Fondation cartier pour l’Arts Contemporain, Paris, 2018, p. 29."
date = ""
identifiant = "natalia1"
subtitle = ""
title = "Architectures <span style=\"hyphens: none; white-space: nowrap;\">néo-andines</span>"
weight = 16

+++
<div id="ville-fixed">
<div class="ville" id="ville-01" data-link="">
<img src="/uploads/natalia/natalia-scrolly-01_batiment.png" loading="lazy" class="batiment" />
<img src="/uploads/natalia/natalia-scrolly-01.png" loading="lazy" />
</div>
<div class="ville" id="ville-02">
<img src="/uploads/natalia/ST02-Natalia-Batiment.png" loading="lazy" class="batiment" />
<img src="/uploads/natalia/ST02-Natalia-Ville.png" loading="lazy" />
</div>
<div class="ville" id="ville-03">
<img src="/uploads/natalia/ST03-Natalia-Batiment.png" loading="lazy" class="batiment" />
<img src="/uploads/natalia/ST03-Natalia-Ville.png" loading="lazy" />
</div>
<div class="ville" id="ville-04">
<img src="/uploads/natalia/ST04-Natalia-Batiment.png" loading="lazy" class="batiment" />
<img src="/uploads/natalia/ST04-Natalia-Ville.png" loading="lazy" />
</div>
</div>

Du haut de ses 4149 mètres d’altitude, El Alto, métropole indigène surplombe La Paz, capitale de la Bolivie. Que l’on arrive par avion ou en voiture, El Alto apparaît comme une ville qui s’extrude de la terre. Ses maisons et bâtiments en brique et terre crue sans finition se fondent dans le paysage environnant. Pendant des décennies, cette ville a vu des castes commerçantes indigènes grandir et développer une richesse camouflée sous l’austérité apparente de son architecture. (Fig.1)


{{% figure class="" img="natalia/fig1" %}}Figure 1: Vue d'El Alto, Bolivie{{% /figure %}}

Ce n’est que dans les années 2000 qu’un nouveau style d’architecture appelée Neo-Andine a vu le jour au cœur de la ville d’El Alto, faisant apparaître dans l’espace urbain la présence affirmée de ces castes commerçantes. « Ces immeubles, dont le style est très éloigné de la discrétion et la sobriété qui caractérisent l’esthétique moderne, surprennent par leurs couleurs insolentes et leur fantaisie ludique, ainsi que leur côté kitsch[\[1\]](#_ftn1). » Freddy Mamani, auteur de ce nouveau style architectural l’appelle architecture Néo-Andine. Certains, plus méprisants l’appellent « architecture _chola_ » ou « _cholets_ », résultat de l’union du mot _cholo_, qui désigne de manière péjorative les populations indigènes de Bolivie[\[2\]](#_ftn2), et _chalet_, en référence aux luxueux chalets qui couronnent ces constructions.

<div class="anchor-scrolly-ville" data-ville="ville-01"></div>

Comment expliquer cet avènement (tardif) dans un pays dont 70% de la population est indigène ?

{{% figure class="" img="natalia/fig2" %}}Figure 2: Facades architecture Neo-Andine d’El Alto, Bolivie{{% /figure %}}

La Bolivie est un pays qui depuis l’époque coloniale a été gouverné par une oligarchie raciste pour qui les populations indigènes n’avaient aucune autre valeur que celle de main d’œuvre et de service domestique. Considérés sauvages, mal éduquées, ces populations étaient soumises à des conditions de vie et d’emploi proches de l’esclavage. Que ce soit dans les mines ou dans les foyers de la classe moyenne ascendante, « _los Indios_ » , « _los cholos_ » passaient même après les animaux domestiques.

Pour atténuer la violence sociale qui leur était infligée quotidiennement, ces populations se sont fait « invisibles », elles se sont mimétisées avec leur environnement. Cela passait autant par la couleur de leurs habits que par la couleur de leur architecture. En effet, la plupart des constructions des _laderas_[\[3\]](#_ftn3) autour de La Paz, el Alto inclus, ne comportaient pas de finitions extérieures. Ceci était peut-être dû à une volonté inconsciente de ne pas se faire remarquer, mais aussi à des raisons économiques. Encore aujourd’hui tant que la maison est considérée « en construction » et donc « inachevée », ses propriétaires ne payent pas d’impôts, même s’ils y habitent.

Cette forme de camouflage pourrait être considérée comme un héritage colonial puisqu’il participe de ce que Michel De Certeau qualifie de _recours tactique_ dans _L’invention du quotidien I, Arts de faire_. Pour De Certeau, les actions quotidiennes, dans leur mise en œuvre, peuvent être des moyens de résistance dans la société.

<div class="anchor-scrolly-ville" data-ville="ville-02"></div>

> _Beaucoup de pratiques quotidiennes (parler, lire, circuler, faire le marché ou la cuisine, etc.) sont de type tactique. Et aussi, plus généralement, une grande partie des « manières de faire » : réussites du « faible » contre le plus « fort » (les puissants, la maladie, la violence des choses ou d'un ordre, etc.), bons tours, arts de faire des coups, astuces de « chasseurs », mobilités manœuvrières, simulations polymorphes, trouvailles jubilatoires, poétiques autant que guerrières. Ces performances opérationnelles relèvent des savoirs très anciens. Les Grecs les désignaient par la métis. Mais elles remontent à bien plus haut, à d’immémoriales intelligences avec les ruses et les simulations de plantes ou de poissons. Du fond des océans aux rues des mégapoles, les tactiques présentent des continuités et des permanences_[\[4\]](#_ftn4)_._

En effet, les peuples indigènes d’Amérique Latine dans leurs actions quotidiennes ont trouvé des formes de résistance face aux Colons. L’artisanat constituait une pratique d’ordre tactique qui permettait d’instaurer cette résistance silencieuse, presque imperceptible. L’évangélisation de ces populations supposait pour les Espagnols l’annulation complète des croyances indigènes. Dans ce processus d’évangélisation, les indigènes étaient utilisés comme main d’œuvre esclave pour construire des Églises. Les façades taillées de ces églises sont un des témoignages les plus vivants des comportements tactiques, qui a donné naissance à ce que l’on appelle aujourd’hui le style _Baroque Métis_. Ces façades en pierre taillée, destinées à l’apprentissage de la religion catholique ont été « polluées » de plantes, animaux fantastiques et d’autres éléments faisant appel aux croyances indigènes. Face à l’impossibilité de reconstruire leur propre cosmologie, les populations autochtones résistaient à son effacement total en la réintroduisant dans ces façades par des éléments décoratifs.

{{% figure class="" img="natalia/fig3" %}}Figure 3: Façade de l’église de Santo Tomás de Chumbivilcas, exemple typique “d’architecture métisse”{{% /figure %}}

De Certeau explique l’existence de ces pratiques tactiques lors de la colonie :

> _Il y a longtemps qu’on a étudié, par exemple, quelle équivoque lézardait de l'intérieur la "réussite" des colonisateurs espagnols auprès des ethnies indiennes: soumis et même consentants, souvent ces Indiens faisaient des actions rituelles, des représentations ou des lois qui leur étaient imposées autre chose que ce que le conquérant croyait obtenir par elles; ils les subvertissaient non en les rejetant ou en les changeant, mais par leur manière de les utiliser à des fins et en fonction de références étrangères au système qu’ils ne pouvaient fuir. Ils étaient autres, à l’intérieur même de la colonisation qui les "assimilait" extérieurement ; leur usage de l'ordre dominant jouait son pouvoir, qu'ils n’avaient pas les moyens de récuser ; ils lui échappaient sans le quitter_[\[5\]](#_ftn5)_._

<div class="anchor-scrolly-ville" data-ville="ville-03"></div>

C’est à partir de ces tactiques que les cultures originales indigènes ont subsisté, elles aussi « polluées » par les croyances héritées de la colonie, ce qui donna naissance à un syncrétisme[\[6\]](#_ftn6) qui reste de nos jours très présent dans la culture bolivienne.

{{% figure class="" img="natalia/fig4" %}} Figure 4: Myriapode: puma avec le corps d’un millepattes, d’antécédent précolombien, qui décore la façade de l’Église de la Compagnie de Jésus à Arequipa (1680){{% /figure %}}

En janvier 2006, après une carrière syndicale de plusieurs années, un certain Evo Morales Ayma devint le premier président indigène de la Bolivie. L’ascension au pouvoir d’une personne issue du milieu rural aymara de la Bolivie a marqué un tournant pour le pays, en particulier pour 70 % de sa population qui se reconnaît comme indigène. Avec ce premier gouvernement, Evo Morales instaura une nouvelle constitution qui transforma la Bolivie en État Plurinational de Bolivie, reconnaissant ainsi sa grande diversité de peuples indigènes comme des nations.

Malgré les réticences de certains secteurs de la population, la population indigène commença à prendre conscience de ses droits et commença à sortir de sa soumission. Petit à petit, les populations indigènes devinrent de moins en moins invisibles. Au début, cela passait par l’accès d’élus indigènes à des postes de la haute fonction publique. Cette visibilité s’est ensuite étendue aux gestes quotidiens affirmant l’identité de ces populations. Les vêtements traditionnels des _Cholas_ ont commencé à devenir un signe distinctif et source de fierté. Des créateurs et créatrices de mode ont développé des collections d’habits de chola en exaltant les couleurs typiques du textile traditionnel andin. C’est dans cette nouvelle vague de revendication identitaire que naît l’architecture Néo-Andine de Freddy Mamani. « Je veux donner une identité à El Alto, ma ville trop souvent méprisée. Une identité qui soit l’expression de notre culture, à nous Alteños[\[7\]](#_ftn7). »

<div class="anchor-scrolly-ville" data-ville="ville-04"></div>

L’architecture de Freddy Mamani est bien la traduction de la structure sociale des populations Aymara de la ville de El Alto. « Profitant de l’inhabituelle stabilité économique et politique dont jouit le pays depuis 2006, avec le début de la présidence d’Evo Morales, El Alto est désormais une ville totalement transformée et le berceau d’une classe émergente : la nouvelle bourgeoisie Aymara. C’est précisément cette nouvelle classe sociale, développant des relations commerciales avec l’Asie et l’ensemble du pays, qui a plébiscité l’architecture de Freddy Mamani dans le but d’affirmer son identité et d’améliorer son statut social. »

Ces bâtiments surprennent par leurs façades colorées reprenant des éléments d’iconographie Tiwanaku avec les couleurs du textile traditionnel autochtone bolivien. Selon Mamani, « Mon architecture s’efforce de donner une identité à ma ville en reprenant des éléments de ma culture indigène. [\[8\]](#_ftn8).»

L’organisation du bâtiment est en accord avec les activités économiques et sociales des commanditaires. Ainsi, au centre du bâtiment, occupant les deuxième et troisième étages, un Salón de Eventos, salle de réception d’une exubérance colorée exceptionnelle, est loué à des familles, des organisations syndicales ou autres associations locales pour célébrer divers événements comme des fêtes, des mariages et plus particulièrement des _Prestes_ [\[9\]](#_ftn9).  
Au rez-de-chaussée une petite galerie commerçante permet aux propriétaires de poursuivre leur activité commerçante et de louer des espaces commerciaux à d’autres commerçants.
Au dernier étage, le chalet, maison-appartement de luxe destinée à être la résidence privée des commanditaires du bâtiment. Son emplacement sur la partie supérieure du bâtiment lui permettrait d’être au plus près de l’alaqpacha (le monde supérieur, selon la cosmologie aymara).
Pour finir, entre le salon et le chalet, se trouvent généralement des appartements destinés aux enfants des commanditaires.

{{% figure class="" img="natalia/fig5" %}}Figure 5: Intérieur d'un Salón de Evento{{% /figure %}}


Les figures héroïques se construisent à partir de récits. Celui de l’architecture andine nous raconte l’histoire de deux figures héroïques. D’une part, celle d’un homme aymara, qui accompli l’impensable : devenir président de la république dans un pays profondément raciste. D’autre part, celle d’un artiste aymara, qui se profile sous la figure de l’artiste génie et qui, à travers son œuvre architecturale, donne corps aux revendications de cette population indigène majoritaire, mise à l’écart depuis la fin de la colonie.  
La première figure représente une première prise de position qui ose montrer au statu quo bolivien et au monde qu’il est possible de faire bouger les lignes et que les populations autochtones ne sont pas condamnées au servilisme.  La deuxième figure se dessine comme une déclaration : « Nous sommes ici, nous existons. Voilà ce que nous sommes et nous en sommes fiers.», laissant les recours tactiques dans le passé colonial.

***

[\[1\]](#_ftnref1) Elisabetta Andreoli, “_Freddy Mamani, une architecture néo-andine.”_ In Catalogue de l’exposition Géométries Sud, di Mexique à la Terre de feu, éd. Fondation Cartier pour l’art Contemporain, Paris, 2018.

[\[2\]](#_ftnref2) À l’origine, pendant la période coloniale, ce mot désignait des personnes métisses, descendantes d’un espagnol et d’un indigène ou métisse. Tiré du mot aymara _chhulu_ qui signifie métisse.

[\[3\]](#_ftnref3) Quartiers populaires en périphérie de la ville, les banlieues.

[\[4\]](#_ftnref4) Michel de Certeau, _L’invention du quotidien, I. Arts de faire_ (1980), éd. Gallimard, coll. « Folio Essais », Paris, 1990, p. XLVII.

[\[5\]](#_ftnref5) _Ibid._, p. XXXVII-XXXVIII.

[\[6\]](#_ftnref6) Selon le dictionnaire de la langue française d’Alain Rey, le syncrétisme “désigne une combinaison difficilement cohérente entre plusieurs doctrines religieuses ou philosophiques très différentes.”

[\[7\]](#_ftnref7) Freddy Mamani cité par Elisabetta Andreoli dans “Freddy Mamani, une architecture néo-andine.” In _Catalogue de l’exposition Géométries Sud, di Mexique à la Terre de feu_, éd. Fondation Cartier pour l’art Contemporain, Paris, 2018.

[\[8\]](#_ftnref8) _Idem._

[\[9\]](#_ftnref9) Nom donné à la fois à la fête religieuse locale et à la personne de la communauté qui prend en charge toutes les dépenses liées à la célébration à laquelle tous les membres de la communauté participent. La responsabilité du preste est rotative entre les différentes familles composant la communauté.