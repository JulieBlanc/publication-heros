+++
author = "Clémence Hallé, Christophe Guérin"
bio = "Clémence Hallé prépare un doctorat à l'École normale supérieure au sein du laboratoire \"Sciences, Arts, Création, Recherche\" (SACRe - PSL) sur une histoire esthétique de l’Anthropocène, poursuivant ses recherches sur la notion de représentation écologique. <br> Christophe Guérin est designer, entrepreneur et doctorant de l'ENSAD au sein du programme SACRe. Sa thèse porte sur \"l'incidence de l'infime\" dans la conception en design."
categories = ["analyse"]
class = "center-content"
contribution = true
cover = "heros-mille-et-un-visages_cover.png"
date = ""
identifiant = "clemence"
subtitle = ""
title = "Le héros aux mille et un visages"
weight = 6

+++
<figure id="mille-et-un-visages">
<img src="/uploads/heros-mille-et-un-visages.png" class="single-only" loading="lazy">
<a href="/textes/mille-et-uns-visages/" class="column-only">
<img src="/uploads/heros-mille-et-un-visages_400.png" loading="lazy">
</a>
</figure>

# Bibliographie

CAMPBELL Joseph, _Le héros aux mille et un visages_, traduit par Henri Crès, Escalquens, Oxus, 2010.

HARAWAY Donna J., _Staying With the Trouble: Making Kin in the Chthulucene_, Durham London, Duke University Press, 2016.

LE GUIN Ursula K., "Le fourre-tout de la fiction : une hypothèse (1986)", in _Danser au bords du monde : mots, femmes, territoires_, traduction de par Hélène Collon, Éditions de L’éclat, Paris, 2020.

RUSS Joanna, “What can a heroine do? Or why women can’t write”, in _To Write Like a Woman: Essays in Feminism and Science Fiction_, Indiana University Press, Bloomington, 1995.