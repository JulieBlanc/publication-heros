+++
author = "Amanda Gann"
bio = "Amanda Gann est une artiste de théâtre doctorante à l'Université de Harvard. Son travail actuel, à l'intersection de la performance et de la recherche, porte sur les manifestations sociales et théâtrales du deuil dans la France de l'après-guerre. En tant qu'actrice, elle collabore avec des compagnies internationales telles que le Poets' Theatre, avec lequel elle a récemment joué \"Not I/Pas moi\" de Samuel Beckett en anglais et en français (MAC, Belfast, NI). Elle travaille également comme traductrice littéraire : sa traduction de \"L'Adoration\" de Jean-René Lemoine a fait l'objet d'une lecture scénique au Martin E. Segal Theatre Center en décembre 2019."
categories = ["pratique", "performance"]
class = "has-aside-content"
contribution = true
cover = "amanda/les-goules_cover.jpg"
creditsPhoto = "Figure 1 : Affiche pour la pièce <em>Le Jardin des Supplices</em>, drame d'après Octave Mirbeau, 1921. <br> Figure 2 : Gilbert Galland, Affiche pour la pièce <em>Les Goules</em>, comédie de Jean Lorrain et de Charles Esquière, La Nouvelle Comédie, vers 1900. <br> Figure 3 : Eugène Delacroix, <em>La Liberté guidant le peuple</em>, 1830. Huile sur toile (H. 2,60 ; L. 3,25), Musée du Louvre, Paris. "
date = ""
identifiant = "amanda"
subtitle = ""
title = "La fin de l'histoire"
weight = 12

+++
{{% divclass class="group" %}}
{{% divclass class="group-content" %}}

Je vis aux États-Unis, où l’on parle beaucoup de toutes les façons qu’il y a de mourir aujourd’hui. Par intérêt anthropologique, voici un petit recensement des formes qui prédominent dans la presse et dans les conversations, sans prétention aucune à l’exhaustivité : on meurt d’une mort subite, lente, impersonnelle, personnelle, accidentelle, prévisible, évitable, inattendue, et/ou injuste. On peut aussi parfois mourir tranquillement, pendant que tout le monde regarde ailleurs.

Afin de conjurer toute cette mort, l’on identifie partout des « héros », un phénomène que je trouve plus inquiétant que rassurant. Mon projet de thèse, bien qu’il se focalise sur la difficulté de représenter et de vivre avec la mort au sortir de la Première Guerre mondiale en France, m’a fait prendre conscience de certains problèmes qui accompagnent un culte du héros. Il est plus facile, certes, d’envoyer des individus se battre s’ils sont imprégnés de belles histoires de sacrifice, mais que faire quand ils constatent que cette mort qu’ils sont censés recevoir et donner n’a rien d’esthétique ni de cohérent ? Dans les faits, elle est arbitraire, équivoque, et brutale. Aucune reconnaissance officielle, aucune campagne de fêtes d’honneur ne peut surpasser la monstruosité des évènements, même lorsqu’ils mènent à une victoire. Selon plusieurs récits et témoignages, beaucoup ont vécu la guerre comme une expérience non pas de leur force mais de leur défaillance, au moins en ce qui concerne les valeurs qui leur avaient été inculquées. Gabriel Chevallier, un ancien combattant, décrit ainsi ce constat, et son effet profondément déstabilisant : « Voilà ce que je suis, un type qui a peur, une peur insurmontable \[…\] Comment pourrais-je encore montrer de l’assurance, sachant ce que je sais sur moi »[\[1\]](#_ftn1). Là où l’on voit un discours public sur les « héros », on doit aussi chercher une raison d’État qui préférerait occulter les conséquences de son exhortation à l’héroïsme : à force de trop tester son courage, l’on se découvre humain, fragile et secoué. Cette connaissance altérée de nous-mêmes peut nous habiter bien au-delà de l’épreuve.

Alors que la tendance historiographique actuelle va vers l’exhumation et la valorisation des « petites » vies, les représentations institutionnelles de la Grande Guerre résistent encore à une exploration de cette vie intérieure de sensation et de sentiment, ne s’en approchant que d’une manière oblique. Même à l’Historial de la Péronne, un lieu d’une grande utilité pédagogique et commémorative, la vie sensible du corps humain n’est évoquée que par l’absence. Le local est tout blanc, propre, silencieux. Les corps des combattants sont figurés par des torses de marbre. On a soigneusement regroupé uniformes et effets personnels de quelques soldats individuels dans des boites séparées, présentées à même le sol ; ces sépultures symboliques ont sans doute une valeur respectueuse, restitutrice même, mais elles gomment le chaos et l’horreur des corps meurtris. L’installation réussit parfaitement sa mission: « to \[invite\] visitors to use their intelligence and sensitivity in order to develop a coherent approach to the architecture, the museography and the collection[\[2\]](#_ftn2). » C’est savant et solennel, une affaire de sensibilité et non pas de sensation. Ce qui pourrait dégouter, par exemple, n’a pas sa place. Ce qui résiste à la cohérence non plus.

J’admets volontiers la valeur d’un lieu de recueillement et réconciliation sobre, organisé selon les conventions disciplinaires de l’historien-chercheur. Il ne revient pas à l’historien de répondre aux questions de la vie sensible et affective du passé, ces questions qui, quoique spéculatives, continuent pourtant à nous intéresser. Mais je crois que nous, artistes-chercheurs, avons la possibilité d’aborder cette matière, d’interroger le passé en tenant compte justement de « ce qu’il y a, en chacun de nous, de vie émotionnelle toujours prête à déborder la vie intellectuelle (…) pour opérer un brusque renversement de cette évolution dont nous étions si fiers: de l’émotion à la pensée, du langage émotionnel au langage articulé[\[3\]](#_ftn3) », selon la formulation de Lucien Febvre. Et puisqu’il s’agit du passé, il nous revient aussi d’éviter la fausse reconstruction historique, de ne pas réduire ce qui excède à la cohérence pour construire encore une épopée héroïque, esthétisant la mort et le sacrifice.

{{% /divclass %}}
{{% /divclass %}}

{{% divclass class="group" %}}

<!-- // Ici ajouter les trois images en N&B dans les dernières version uploadées dans "Médias"(normalement j'ai changé les adresses déjà), qui défilent à droite dans la version pleine page de l'article // -->

<div class="group-aside">  
<img id="amanda_img-1" src="/uploads/amanda/le_jardin_des_supplices.jpg" loading="lazy">
<img src="/uploads/amanda/les_goules_drame.jpg" loading="lazy">
<img src="/uploads/amanda/la-liberte-guidant-le-peuple-eugene-delacroix.jpg" loading="lazy">
</div>

{{% divclass class="group-content" %}}

Dans mon travail de recherche-création, je cherche à mener l’enquête là où l’on était avide de sensations, où le corps pouvait parler pour lui-même. Au Théâtre du Grand-Guignol, il n’y avait pas de héros, peu de bienséance, et un goût douteux. Cette petite salle de l’impasse Chaptal, avec son architecture, son répertoire et son public hétéroclites, fut un des plus grands succès de la scène théâtrale parisienne des années 1920. Pour être parfaitement honnête, ne le connaissant que par sa réputation, j’imaginais que cela devait être du mauvais théâtre - une orgie de tortures spectaculaires et sanglantes sans intérêt pour quelqu'un qui cherchait une vérité.

Car on avait toujours cultivé en moi, et je l’ai cultivé moi-même par la suite, un goût pour la grande littérature. Dans mon travail de comédienne, je m’étais souvent retrouvée à mettre mon corps, ma voix, et toute ma sensibilité au service des textes classiques. Ce n’est pas que je prenais ces fictions comme « vraies », pour ainsi dire, mais à force de les incarner, elles s’étaient glissées en moi d’une façon insoupçonnée. Alors que je me croyais purement rationnelle, circonspecte, j’avais tant d’autres figures qui flottaient en moi, et je puisais dans leur langage pour sublimer mes petites douleurs. Or, quand la vraie catastrophe est venue, tout s’est mis à chavirer. Le décès soudain de mon fiancé était inassimilable. Toute cette littérature héroïque me semblait vide, au fond. Rien ne m’indiquer ce qu’il fallait penser ou faire. Je ne pouvais pas mourir avec lui, car aucune barricade ne s’offrait à moi. Hors de question de lui restituer à la vie en me laissant à sa place, car la catabase n’est qu’un motif. J’étais risible, une Andromaque inadéquate; mes douleurs de veuve n’avaient rien de cette « immense majesté » que Baudelaire leur avait attribuée. Cela me semblait comme une énorme erreur dramaturgique, que de me laisser, moi, seule sur le plateau, alors que je n’étais pas même à la hauteur de comprendre ni d’endosser un rôle.

Quand je repense à ce que j’ai fait ensuite, je comprends la valeur que je trouve maintenant dans ce répertoire assez méconnu du Grand-Guignol, car il comporte beaucoup plus que des fantasmes violents et grotesques. Bien souvent, ce sont des histoires de personnages, assez ordinaires en fin de compte, qui se confrontent à des situations morbides et désespérées. Parfois, quand le langage nous fait défaut, il ne nous reste que la sensation. J’ai fini par passer d’innombrables nuits à m’enivrer avec des inconnus dans des bars minables à Londres, où je vivais à l’époque. Dans cet espace à la lisière du public et de l’intime, parmi les confiances partagées, les blagues grivoises, les scènes impudiques, les récits de déceptions, de désirs, et de rêves, nous avions le droit d’être ce que nous sommes, de petites gens avec de grandes émotions. Au moment critique, nous sommes capables d’hésiter, de défaillir, de tomber dans l’erreur, d’avoir peur. C’est alors que les figures héroïques nous hantent. Mais c’est toujours nous qui leur donnons de la matière, pas l’inverse. Nous pouvons les jouer, les déjouer, ou jouer avec elles jusqu’à ce qu’elles deviennent méconnaissables. Est-ce indécent ? Peut-être. Mais quand je me réveille le matin, entortillée dans les draps mais avec les seins à l’air, je me souviens de mon fiancé défunt qui me disait « tu ressembles à _La liberté guidant le peuple_ ». Cela me fait souvent rire, car je trouve maintenant que toutes les iconographies se ressemblent trop.

Mais enfin, avec toutes les contradictions qui me collent à la peau, pourquoi pas aussi du courage ?

{{% /divclass %}}
{{% /divclass %}}

***

[\[1\]](#_ftnref1) Cité par Carine Trevisan, « On ne sait pas à quoi on appartient : Le roman du retour », in Bruno Cabanès et Guillaume Piketty (dir.), _Retour à l’intime au sortir de la guerre_, Paris, Éditions Tallandier, 2009, p. 86 : Gabriel Chevallier, _La Peur_ \[1930\], Paris, Le Dilettante, 2008, p. 250.

[\[2\]](#_ftnref2) Texte affiché à l’Historial, cité dans David Williams, _Media, Memory, and the First World War_, Montreal, McGill-Queen’s Press, 2009, p. 256.

[\[3\]](#_ftnref3) Lucien Febvre, « La sensibilité et l'histoire : Comment reconstituer la vie affective d'autrefois ? », _Annales d'histoire sociale_ (1939-1941), T. 3, No. 1/2 (Jan. - Jun., 1941), p. 19.