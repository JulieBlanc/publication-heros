+++
author = "Joseph Minster"
bio = "Joseph Minster quitte les Alpes en 2005 pour étudier la littérature et le cinéma à Paris. Titulaire d’un Master de littérature comparée et diplômé du département Réalisation de La Fémis, il écrit pour le web et le cinéma et revient régulièrement tourner des films dans les paysages de son enfance. Il a réalisé plus d’une douzaine de courts-métrages, projetés dans de nombreux festivals. Depuis 2019, il est doctorant au sein du laboratoire SACRe (Université PSL – La Fémis) dans le cadre duquel il mène un travail de recherche sur la notion de variation cinématographique."
categories = ["cinema", "pratique"]
class = "center-content"
contribution = true
cover = "joseph-2/joseph-2_cover.jpg"
creditsPhoto = "Figure 1 : Moussa Sylla, Bellamine Abdelmalek, in <em>Ubac</em>, 2018, © Palikao films <br> Figure 2 et 3 : <em>Ubac</em>, 2018, © Palikao films"
date = ""
identifiant = "joseph-2"
subtitle = "À propos d'*Ubac&nbsp;* "
title = "Dans l'ombre, les&nbsp;héros"
weight = 7

+++
> Les réfugiés qui risquent leur vie pour des raisons politiques sont des héros.
> {{% footer %}}Emmanuel Macron, ministre de l’Économie, _Le Soir_, 20 avril 2016{{% /footer %}}

> Le sujet des migrations n'est pas derrière nous. Il faut se garder des faux bons sentiments.
> {{% footer %}}Emmanuel Macron, Président de la République, Rome, 11 janvier 2018{{% /footer %}}

J’ai grandi en Haute-Savoie. À plusieurs reprises, au cours de ma scolarité, je me suis rendu avec ma classe sur le plateau des Glières. L’école de la république m’a appris à admirer les hommes qui y sont morts au nom de l’idéal exprimé par leur devise : « Vivre libres ou mourir ». Elle m’a appris à voir en eux des héros – ces arbres, je l’ai compris plus tard, qui cachent la forêt des peurs, des lâchetés, et des compromissions de tous les autres. « Vivre libre ou mourir » : n’est-ce pas ce même idéal qui anime les réfugiés et les migrants ? Qu’ils soient « économiques » ne change rien à l’affaire…

Tous les jours, au cours de l’hiver 2018, on entendait parler dans la presse de leurs tentatives : des hommes, souvent, des femmes aussi parfois, s’efforçaient de passer la frontière franco-italienne au cœur des Alpes, en se lançant à l’assaut de la montagne dans la nuit. De longs reportages photographiques permettaient de voir qu’ils étaient mal équipés pour ce périple, vêtus d’un blouson trop étroit, d’un jean et d’une paire de baskets. Souvent, ils découvraient la neige.

{{% figure class="" img="joseph-2/mousssa-sylla" %}}{{% /figure %}}

Que faire de ces histoires que je lisais, que j’entendais et que je voyais tous les jours dans les journaux, à la radio ou à la télévision ? Que faire de ces récits tellement récurrents qu’on finit par ne plus les lire, par ne plus les écouter, par ne plus les regarder – ou alors à peine, d’un œil distrait, avant de passer à autre chose ? Ils constituent l’envers de notre vie protégée, la face nord, l’_ubac_[\[1\]](#_ftn1), celle qui reste dans l’ombre, et qu’on laisse dans l’ombre puisque le scandale s’y voit moins.

Cette ombre, il est peut-être possible de la dissiper, l’espace d’un instant : pour voir, pour entendre à nouveau. Pour se souvenir de ce que c’est qu’un héros : un homme ou une femme qui rêve de pouvoir vivre sa vie, et qui affronte le risque de la mort pour le faire. Le cinéma sait faire cela : nous obliger à regarder ce que l’on ne veut plus voir, et proposant une autre image que celles auxquelles on a l’habitude d’être confronté. J’ai donc imaginé le film _Ubac_ (2018) en réaction aux images médiatiques, comme une sorte d’épure, comme un poème cinématographique, presque sans parole.

{{% divclass class="full-video" %}}
<video controls loop class="videoPlay lazy" poster="/uploads/joseph-2/ubac-extrait1_poster.jpg">
<source data-src="/uploads/joseph-2/ubac-extrait1.mp4" type="video/mp4">
Sorry, your browser doesn't support embedded videos.
</video>
<button class="click-unmuted single-only">Cliquez pour entendre le son</button>
{{% /divclass %}}



Qu’y a-t-il à voir, donc ? Deux hommes avancent dans la neige. On ne sait rien d’eux, sinon qu’ils cheminent ensemble, et qu’ils se cachent. Ils glissent le long de pentes enneigées, ils évitent les chasseurs et les chiens qui pourraient leur courir après, ils fuient les gendarmes qui les ramèneraient de l’autre côté d’une frontière qu’ils viennent de franchir, ils disparaissent dans la nuit qui tombe et les empêche de voir où poser leurs pieds. Ils se logent au cœur du froid. Dans le silence ouaté de la montagne en hiver, leur seul dialogue est le bruit de leurs pas. Ils avancent, coûte que coûte, échangeant parfois un regard pour se donner du courage, en espérant parvenir au terme de leur voyage : quelque part en France.

{{% divclass class="full-video" %}}
<video controls loop class="videoPlay lazy" poster="/uploads/joseph-2/ubac-extrait2_poster.jpg">
<source data-src="/uploads/joseph-2/ubac-extrait2.mp4" type="video/mp4">
Sorry, your browser doesn't support embedded videos.
</video>
<button class="click-unmuted single-only">Cliquez pour entendre le son</button>
{{% /divclass %}}

Les héros sont invisibles. Est-ce un de leur pouvoir ? Ou une défaillance de ceux qu’ils croisent ? Un panoramique accompagne les deux hommes qui suivent un sentier escarpé dans une pente verglacée, et se hissent sur un chemin enneigé. Une silhouette s’avance depuis le fond du plan, glissant sur des skis de randonnée. Les deux voyageurs ralentissent, un peu désemparés. Mais le skieur indifférent continue son chemin, sans même un regard pour eux. Ils ne font pas partie de son monde – à cet instant précis, du moins. Il ne les voit pas, il ne soupçonne pas que se tiennent face à lui des héros. Parce qu’en fait, l’allure des héros n’a rien d’exceptionnel : ils nous ressemblent, à s’y méprendre. D’ailleurs, les voilà qui reprennent leur route, profitant de l’inattention du skieur, vaguement inquiets, tout de même, puisqu’ils ne peuvent s’empêcher de jeter un coup d’œil en arrière, des fois que le skieur leur ait joué un tour. Mais non, le crissement de ses bâtons et le cliquetis de ses chaussures s’éloignent. Alors les héros se mettent à courir. Parce que tout héros qu’ils sont, ils ont peur.

On se doute que d’autres obstacles se dresseront sur leur route : les ruisseaux gelés qu’il faut traverser, le vent qu’il faut supporter, et le froid, encore le froid, toujours le froid. Pour que les réfugiés soient des héros, il faut bien qu’ils aient des épreuves à surmonter. Mais puisque ce sont des héros, puisqu’on le reconnaît, puisque même le président l’a dit, avant d’être président, alors on est quitte. _Regarde les héros morfler_.

On devine que ça va mal finir, cette histoire. D’ailleurs, ça finit mal. Le reste est dans les journaux tous les jours.

***

[\[1\]](#_ftnref1) Dans les Alpes, le terme « ubac » désigne le versant le moins ensoleillé d'une vallée (généralement exposé au Nord), souvent forestier. Le terme est issu de l’ancien provençal _ubac_, du latin _opacus_, sombre.