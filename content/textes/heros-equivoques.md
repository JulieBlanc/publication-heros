+++
author = "Joseph Minster"
bio = "Joseph Minster quitte les Alpes en 2005 pour étudier la littérature et le cinéma à Paris. Titulaire d’un Master de littérature comparée et diplômé du département Réalisation de La Fémis, il écrit pour le web et le cinéma et revient régulièrement tourner des films dans les paysages de son enfance. Il a réalisé plus d’une douzaine de courts-métrages, projetés dans de nombreux festivals. Depuis 2019, il est doctorant au sein du laboratoire SACRe (Université PSL – La Fémis) dans le cadre duquel il mène un travail de recherche sur la notion de variation cinématographique."
categories = ["cinema", "analyse"]
class = "has-aside-content"
contribution = true
cover = "joseph-1/joseph-1_cover.jpeg"
creditsPhoto = "Figure 1 : Gilbert Melki, in <em>Un couple épatant</em>, 2003 © Studio Canal <br> Figures 2, 3, 4 : Alexis Tomassian, Dominique Blanc et Lucas Belvaux, in <em>Cavale</em>, 2003, © Studio Canal <br> Figures 5, 6, 7 : Dominique Blanc, Alexis Tomassian et Lucas Belvaux, in <em>Après la vie</em>, 2003, © Studio Canal <br> Figure 8 : Lucas Belvaux, in  <em>Cavale</em>, 2003 © Studio Canal"
date = ""
identifiant = "joseph-1"
subtitle = "Ce que nous apprend la Trilogie de&nbsp;Lucas Belvaux (*Un&nbsp;couple épatant* / *Cavale* / *Après la&nbsp;vie*) "
title = "Héros équivoques"
weight = 14

+++
{{% divclass class="group" %}}

<div class="group-aside group-only-aside single-only">
<div class="sticky-aside">
<img src="/uploads/joseph-1/couple-epatant.jpg" loading="lazy">
</div>
</div>

{{% divclass class="group-content" %}}

En 2002, trois longs-métrages de Lucas Belvaux, _Un couple épatant, Cavale,_ et _Après la vie,_ sortent en salle à quelques jours d’intervalle. Bien que constituant trois récits indépendants, ils sont articulés selon un principe balzacien dans lequel les personnages secondaires d’un film deviennent les personnages principaux des autres, et réciproquement. Gilbert Meki interprète par exemple un policier qui est engagé par la femme du couple bourgeois d’_Un couple épatant_ (Ornella Mutti) pour suivre son mari (François Morel) qu’elle soupçonne de la tromper, pourchasse le héros du second film, un révolutionnaire en cavale (Lucas Belvaux), tout en affrontant les problèmes de conscience qu’occasionne la souffrance de sa femme morphinomane (Dominique Blanc) dans le troisième film, _Après la vie_.

<div class="aside-center">
{{% figure class="" img="joseph-1/couple-epatant" %}}{{% /figure %}}
</div>

Comme le remarque la critique lors de la sortie en salle, « tout l’intérêt de ce triptyque consiste (…) dans la disparité qui existe entre sa fin et ses moyens. Composé de volets ancrés dans une tradition réaliste qui ambitionne de rendre compte d’un état de la société (l’amour, la politique, la drogue…), il n’en conquiert pas moins le statut d’une expérience conceptuelle, infiniment ouvert[\[1\]](#_ftn1). » Que cette expérience nous apprend-elle du héros — des héros ?

D’abord sans doute que le héros est celui à qui on consacre du temps. Comme l’affirme Lucas Belvaux, « Tous les seconds rôles peuvent devenir premier rôle à partir du moment où on s’intéresse à leur cas[\[2\]](#_ftn2). » S’_intéresser au cas_ d’un personnage, c’est le filmer, autant, sinon plus que les autres. Dans _Cavale_, Pascal Manise, l’inspecteur, apparaît à quatre reprises dans le film, mais toujours de façon rapide, comme une figure archétypale que l’on croise, chargée d’incarner l’état auquel le révolutionnaire évadé Bruno, présent dans la majeure partie des plans du film, souhaite échapper. Le rapport que l’on entretient avec ce personnage s’inverse complètement dans _Après la vie_ : en découvrant la vie privée de Pascal Manise, on apprend que son épouse se drogue, qu’un parrain local en profite pour le faire chanter, et qu’il se trouve de ce fait contraint de devoir arbitrer entre son devoir et l’amour qu’il porte à sa femme : conflit racinien, tragique, dont l’exposition permet de mettre en valeur les vertus héroïques du personnage. Peut-on pour autant affirmer que chacun est un héros qu’on ignore ? Il y a là un pas que la _trilogie_ ne permet pas de franchir.

{{% /divclass %}}
{{% /divclass %}}

{{% divclass class="group" %}}

<div class="group-aside">
<div class="sticky-aside trio single-only">
    <img src="/uploads/joseph-1/cavale1.jpg" loading="lazy">
    <img src="/uploads/joseph-1/cavale2.jpg" loading="lazy">
    <img src="/uploads/joseph-1/cavale3.jpg" loading="lazy">
</div>
<div class="trio column-only">
    <img src="/uploads/joseph-1/cavale1_column.jpg" loading="lazy">
    <img src="/uploads/joseph-1/cavale2_column.jpg" loading="lazy">
    <img src="/uploads/joseph-1/cavale3_column.jpg" loading="lazy">
</div>
</div>

{{% divclass class="group-content" %}}

En effet, la question de l’héroïsme est abordée frontalement à travers la figure de Bruno. Dans _Cavale_, ce révolutionnaire convaincu refuse de renoncer à l’idéal pour lequel il a commis des crimes l’ayant envoyé en prison pour des années. Dès qu’il le peut, il reprend la lutte, et poursuit sa trajectoire en marge du commun des mortels. Ce faisant, il affirme sa constance et son courage physique, et assume une forme d’héroïsme, au nom d’une cause qu’il semble désormais un peu seul à défendre. À l'isolement, il n'a pas vu le monde changer. Lors d’une confrontation terrible avec son ancienne amante et camarade de lutte (Catherine Frot), un long travelling avant sur le visage défait de cette dernière en témoigne, tandis que Bruno affirme, contre toute évidence, en s’enivrant de mots : « on n’a jamais été aussi forts, Jeanne… (…) Chaque fois qu’il y aura mille chômeurs de plus, on flinguera un patron au hasard. Les licenciements vont se faire rares… » Après que Jeanne a confronté Bruno aux morts inutiles dont il est responsable, ce dernier lui tourne le dos, se mettant face au mur, signifiant physiquement l’impossibilité du moindre dialogue, et l’impasse dans laquelle il se trouve dès lors que son héroïsme n’est pas reconnu comme tel. La solitude dans laquelle il s’enferme conduit _in fine_ à sa _disparition_ littérale, puisque dans la dernière séquence de _Cavale_, on voit son corps glisser lentement dans une crevasse au milieu d’un glacier immaculé, alors qu’il essayait de rejoindre l’Italie[\[3\]](#_ftn3). L’existence comme héros appelle la reconnaissance d’un tiers – que ce soit celle d’un autre personnage, ou celle du spectateur.

<p style="display: none"> </p>

{{% /divclass %}}
{{% /divclass %}}

{{% divclass class="group" %}}

<div class="group-aside">
<div class="sticky-aside trio single-only">
    <img src="/uploads/joseph-1/apres-la-vie-1.jpg" loading="lazy">
    <img src="/uploads/joseph-1/apres-la-vie-2.jpg" loading="lazy">
    <img src="/uploads/joseph-1/apres-la-vie-3.jpg" loading="lazy">
</div>
<div class="trio column-only">
    <img src="/uploads/joseph-1/apres-la-vie-1_column.jpg" loading="lazy">
    <img src="/uploads/joseph-1/apres-la-vie-2_column.jpg" loading="lazy">
    <img src="/uploads/joseph-1/apres-la-vie-3_column.jpg" loading="lazy">
</div>
</div>

{{% divclass class="group-content" %}}

Cette reconnaissance, Bruno l’acquiert plus ou moins dans _Après la vie_, peut-être justement parce qu’il n’est pas le personnage principal de l’histoire, et que cette attention moindre permet d’ellipser certaines de ses faiblesse. Alors qu’Agnès se fait tabasser par un dealer après qu’elle lui a demandé de la came, Bruno surgit du hors-champ, tel un _deus ex machina_, pour mettre en joue le jeune homme, et l’obliger ensuite à donner à Agnès ce qu’elle est venue chercher. Pour le spectateur, en empathie avec Agnès, son apparition est celle d’un héros surgi d’un univers parallèle et légendaire. L’expérience proposée par la trilogie est ici particulièrement fine, puisque cette même scène existe aussi dans _Cavale_, du point de vue de Bruno. Le passage d’un film à l’autre entraîne une recomposition temporelle des évènements autour de l’un ou de l’autre des personnages principaux. Le contenu premier du cinéma, nous rappelle Jacques Aumont, n’est pas, « le drame », mais « le temps – le temps mis en forme[\[4\]](#_ftn4). » Un temps qui, chez Belvaux, épouse de façon mimétique le temps ressenti par le personnage principal. Or le temps n’est pas le même pour tous les personnages, ce qu’exprime Agnès de façon littérale lorsqu’elle crie sa douleur et son manque à son mari qui lui demande de patienter jusqu’à ce qu’il trouve la dose dont elle a besoin : « T’as pas encore compris que cinq jours, pour moi c’est dix ans ! ». Ainsi, la scène de l’accrochage avec le dealer est-elle précédée dans _Cavale_ d’une longue scène d’attente, montée en champ-contrechamp, en longue focale, au cours de laquelle Bruno observe le point de vente dans l’espoir de voir apparaître un ancien complice. L’arrivée d’Agnès vient parasiter cette situation d’attente, oblitérer ses chances de voir apparaître son ancien complice, et l’_oblige_ en quelque sorte à intervenir lorsque le dealer commence à la tabasser, s’il veut garder une chance d’obtenir l’information qu’il espère. L’héroïsme désintéressé dont il semble d’abord faire preuve dans _Après la vie_ n’apparaît ainsi jamais vraiment comme tel dans _Cavale_. Question de point de vue, question de temps vécu et ressenti. Question de récit, donc : la reconnaissance du héros dépend de l’histoire que l’on (se) raconte.

<p style="display: none"> </p>

{{% /divclass %}}
{{% /divclass %}}

{{% divclass class="group" %}}

<div class="group-aside">
<div class="sticky-aside">
    <img src="/uploads/joseph-1/gif-cavale.gif" class="single-only" loading="lazy">
    <img src="/uploads/joseph-1/gif-cavale_column.gif" class="column-only" loading="lazy">
</div>
</div>

{{% divclass class="group-content" %}}

Ici se condensent sans doute deux des enseignements de « l’expérience conceptuelle » que constitue la trilogie de Belvaux. Le premier est rappelé par Jacques Mandelbaum, qui souligne le paradoxe d’un projet qui feint de « multiplier les points de vue tout en démontrant, dans chaque film, qu’il ne saurait y en avoir plus d’un seul, celui du personnage qui le soutient[\[5\]](#_ftn5). » La comparaison des films de la _trilogie_ rend en effet particulièrement perceptible la façon dont le rythme sous-jacent de chaque scène épouse la perception du personnage principal de la scène – le processus est d’ailleurs dialectique : c’est aussi le rythme de la scène qui nous rappelle qui est son personnage principal. Le second tient au paradoxe suivant : même si l’existence du héros au cinéma est toujours affaire de _temps_, puisque seuls les personnages auxquels le film s’intéresse, ceux auxquels il consacre du temps, peuvent devenir des héros, il faut pourtant que le temps du héros ne coïncide pas exactement avec celui du film. En cela, héros et personnage principal ne sont pas synonymes. La question n’est pas tant celle du mystère, d’un trop-plein d’informations dont il faudrait se garder : un excès d’information n’empêche jamais le spectateur de percevoir les vertus héroïques d’un personnage, quand bien même ses faiblesses en feraient un être très imparfait. Après tout, depuis toujours, les héros se situent dans un entre-deux, mi-hommes, mi-dieux. Mais c’est précisément parce qu’ils se trouvent dans cet entre-deux équivoque que les héros, pour être perçus comme tels, doivent surprendre le spectateur, être en avance, en retard, en ternaire quand on est en binaire, en binaire quand on est en ternaire.

Les héros, ce sont donc les personnages auxquels le film consacre du temps, dont le film épouse parfois la perception temporelle, mais en échouant toujours à le faire parfaitement. C’est par cet écart irréductible entre le temps du film et leurs temps à eux que les héros se distinguent, et qu’on comprend qu’ils sont à la fois parmi nous, les humains, mais aussi un peu ailleurs.

{{% /divclass %}}
{{% /divclass %}}

***

[\[1\]](#_ftnref1) Jacques Mandelbaum, « Une trilogie kaléidoscopique pour mettre en doute le réel », _Le Monde_, 01/01/2003.

[\[2\]](#_ftnref2) Valérie Ganne, entretien avec Lucas Belvaux, _Synopsis_ n° 23, 2003, p. 18-25.

[\[3\]](#_ftnref3) « Le mauvais pas de Bruno en montagne engage tout son corps. _Enseveli vivant_, il lutte puis se laisse glisser, lâche prise, définitivement réduit au silence. », Fabienne Costa, « Solitudes. La trilogie de Lucas Belvaux : « Un couple épatant », « Cavale », « Après la vie » », _Vertigo_, vol. 23, n° 1, 2002, p. 93.

[\[4\]](#_ftnref4) Jacques Aumont, _Que reste-t-il du cinéma ?_, Vrin, Paris, 2013, p. 96.

[\[5\]](#_ftnref5) Jacques Mandelbaum, _op. cit_.