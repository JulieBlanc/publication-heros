+++
author = "Léandre Bernard-Brunel"
bio = "Diplômé des Beaux-Arts de Paris et historien de formation, Léandre Bernard-Brunel développe un travail à la lisière des arts plastiques et du cinéma. Ses films et installations vidéos ont été montrés notamment au festival Cinéma du réel, au Musée des beaux-arts d’Orléans, à la Maison européenne de la photographie ou encore à la Biennale de Belleville. Sa recherche par la pratique dans le cadre du doctorat SACRe/PSL se compose de trois récits phénoménologiques abordant pratiques vernaculaires, strates de paysages urbains, traités de géométrie, poésie et textures du temps en Inde."
categories = ["cinema", "analyse"]
class = "center-content"
contribution = true
cover = "leandre-1/anurag-kashyap_cover.jpg"
creditsPhoto = "Anurag Kashyap, <em>Raman Raghav 2.0</em>, Phantom Film, 127 minutes, 2016."
date = ""
identifiant = "leandre-1"
subtitle = "Nawazuddin Siddiqui, figure&nbsp;nouvelle&nbsp;du&nbsp;cinéma indien"
title = "Naayak"
weight = 8

+++
Il s’est glissé furtivement dans la peau d’un pickpocket, d’un nomade Rabari, d’un camé, ou encore d’un terroriste… Des identités de paria, de figurant effrayant ou d’homme du peuple tout simplement. Lui le musulman de Budhana, localité agricole de l’Uttar Pradesh. Lui dont la famille subissait encore hier la morgue ancestrale des castes supérieures de son _Nagar_ natal. Il faut voir sa petite taille, son teint sombre, ses poches sous les yeux et surtout son regard trouble de chiqueur de _paan_. Il s’est repu de toutes ces silhouettes, les agrégeant patiemment dans le millefeuille de son jeu. Chaque nouveau rôle s’est chargé du précédent. À Bollywood, il est désormais comme Ganesha dévorant l’ensemble du festin somptueux et immodeste offert par le Dieu Kubera. Le voilà donc à son tour sur le point d’avaler récipients, mobiliers et hôte inclus, car c’est la source même de cette lumière aveuglante qu’il s’apprête à ingurgiter, renvoyant les stars pâlottes dans les cordes de leur jeu surexposé.

{{% figure class="" img="leandre-1/anurag-kashyap" %}}{{% /figure %}}

Il a été un Ganesh sulfureux, prénom d’un patron mafieux et hypnotique dans un Mumbai corrompu par une soif insatiable d’argent et de pouvoir[1](#_ftn1). Comme il a été Shiva, prénom du gangster qu’un policier hésite à abattre dans le recoin sale d’une rue de cette _Maximum City_[2](#_ftn2). Il n’a jamais pu être en revanche le démon Maarich, celui qui meurt des mains de Lord Shiva, vieux rêve d’enfance que la fête Ram Ramli mettait et met encore en scène une fois par an dans son Uttar Pradesh natal. Des membres locaux du parti nationaliste hindou marathi Shriv Sena ont intimé à ce musulman de Budhana de ranger sa coiffe et son costume d’or. Ils ont rompu la coutume régionale qui autorisait quiconque à interpréter aussi bien les démons Maarich et Ravan que les dieux Lam ou Laxman peuplant les récits du Ramayana et du Mahabharata. Ne raconte-t-on pas qu’au milieu du XIXe siècle dans la Cité de Lucknow le dernier prince moghol du royaume d’Oudh, Wajid Ali Shah, se glissait dans les apparats de Lord Krishna pour célébrer en musique et en danse kathak ce Dieu d’une autre religion[3](#_ftn3) ?

Nawazuddin Siddiqui a été alors cette célébrité nouvelle de retour au pays, celui qui a refusé l’esclandre, prétextant l’incident technique pour préserver la paix dans sa localité. Ne rien envenimer. Sagesse de ceux qui se taisent. Mais, il sera alors, ironie froide et déroutante, celui qui, trois ans après cette censure, se fondra dans les traits du fondateur de ce même parti nationaliste hindou : le caricaturiste de presse Bal Thackereray[4](#_ftn4). Curieuse intrusion dans un film hagiographique ambigu produit à la demande du propre fils de cet ancien leader du Maharashtra, ce Bal Thackereray allergique à toutes différences, figure politique clivante et déroutante crânant d’avoir eu les honneurs de partager ses toilettes avec un Mickaël Jackson de passage à Mumbai. Trivialité. Démagogie. Identification épidermique. Peut-être aucune de ces postures ne s’emboîtent-elles véritablement… Alors, par un effet de miroir inversé, Nawazuddin Siddiqui, qui sera aussi Saadat Manto[5](#_ftn5), l’écrivain maudit contraint de s’exiler au Pakistan lors de la Partition, deviendra le temps d’une séquence de _Raees_[6](#_ftn6) le prince de la pop dans le Gujarat de la prohibition. Il sera alors dans un sommet de grotesque décousu et indolent, un commissaire de police grimé facétieusement afin de mieux surprendre les convives d’un contrebandier ressuscité par un Shahrukh Khan sans âge.

Des héros comme celui-ci, Nawazuddin Siddiqui ne cesse de s’y heurter. Littéralement, passant d’un côté ou d’un autre de la morale. On le croit effacé, en retrait. Il détient juste un secret sur le temps. Toujours une longueur d’avance. Une omniscience qui traverse les films. C’est le moment de dire ici que l’héroïne ou le héros en Inde, à Bollywood, Kollywood, Mollywood ou bien encore Tollywood, n’est pas l’éphémère incarnation d’un personnage, une enveloppe le temps d’un film, une seconde peau, souvent réversible à l’intérieur d’un même récit. Non, le héros c’est la star. Et _Nayak,_ le titre bengali d’un film de Satyajit Ray dans lequel s’étalent les tourments d’un acteur fendant l’armure et dévoilant ses faiblesses à une inconnue le temps d’un trajet en train, ne raconte que cela[7](#_ftn7).

Aussi lorsqu’un cordonnier homonyme du célèbre Dev Andand, se présente à vous dans une rue de Baroda en clamant qu’il n’est pas le héros annoncé par son nom, c’est encore la même histoire. Celle-ci se rejoue jusque dans ces espaces, sur ces trottoirs peuplés de projections désabusées où flottent l’aura de ces stars à demi-déifiés.

En deux décennies, Nawazuddin Siddiqui est donc devenu un héros. Bien malgré lui, peut-être pas. Bien malgré eux, sans doute. Il faut voir ce corps tragique et burlesque, tendu entre fausse nonchalance, engourdissement et soudaine célérité. Le voilà bousculé physiquement quand il n’est pas battu, voire abattu. Le voilà subalterne muté socialement quand il n’est pas congédié définitivement de ses emplois. Le voilà placé enfin hors cadre quand il n’est pas évincé du récit. Mais le voilà encore, revenant à la charge, comme le fantôme obsédant de l’éternel figurant bien décidé à faire effraction dans le champ et à y rester pour de bon. Là il pourra exercer son aura magnétique jusqu’à d’aimables comédies mélos. Il faut entendre sur ses deux décennies et sur ses plus de quatre-vingt-dix-sept films et séries, le grain éraillé de cette voix faussement indolente. Cette parole toujours un peu retardée — maîtresse du silence, capable de tomber en un couperet tantôt fatal, tantôt comique. Les deux visages de Siddiqui, formé notamment par la pratique du théâtre ambulant et par un professeur russe, Valentin Teplyakov.

Dans le court-métrage adapté d’une nouvelle de Satyajit Ray _(Patol Babu Film Star)_ mis en scène par Dibakar Banerjee pour le film à sketches _Bombay Talkies_, le voilà d’ailleurs sans emploi, qui se cogne à l’angle mort d’un tournage de rue croisé au hasard d’une déambulation[8](#_ftn8). Sa silhouette frêle est aussitôt saisie par l’équipe de cinéma. Il sera le figurant furtif qui doit percuter par distraction un héros pressé et relégué pour la demi-heure dans la sphère de l’invisible. Il faut alors le voir s’électriser, après un temps de fausse absence, puis finalement dévorer l’espace du cadre, le retourner littéralement et méthodiquement à son avantage, et enfin s’en extraire avec un dédain tout mélancolique pour rejouer plus tard cette collusion sur un mode parodique sensiblement chaplinien, grammaire qu’il maîtrise par ailleurs avec malice. Preuve qu’il se repaît de tout.

Ce jour-là, non seulement fut acté son statut de nouveau héros d’un cinéma _mainstream_ en pleine réinvention, mais fut aussi énoncé en creux la mise en cause de canons esthétiques qui devront désormais composer avec un trouble, une ambivalence et un sens du rythme qui étrangement absout Nawazuddin Siddiqui de tout spectacle chorégraphique. S’il est un des rares à pouvoir se passer d’intermèdes musicaux, c’est peut-être parce que son corps, brisé par les humiliations de ses personnages, dessine secrètement une cadence mobilisée par un subtil jeu de contrepoints. Une affaire d’ancrage au sol, de syncope et une manière de se préparer au combat à la façon d’un danseur khatak.

Nawazuddin Siddiqui incarne ainsi cette machine double fabriquant illusions et ruptures pour ne cesser de se heurter au réel. Il faut voir ses tours de bonimenteur et de lanceur de dés qu’il déploie au fond d’un bus. Il faut voir sa gestique virtuose permettant à la caméra de saisir à la volée d’autres visages anonymes qui disparaîtront aussitôt. Le sien flotte, résiste même une fois disparu de l’image. Il est la figure planante d’une altérité assumant aspérité et art de l’impur. Un héros lointain et en proximité avec ses failles vertigineuses. Un héros dont le seul horizon est de faire osciller les images. Un geste de cinéma qui ne cesse de s’annuler et de se réactiver à mesure qu’il migre de film en film.

Léandre BERNARD-BRUNEL, 25 octobre 2020

***

[1](#_ftnref1) Anurag Kashyap, Vikramaditya Motwane, _Sacred Games_, Phantom Films, série de 32 épisodes, 2018, adaptée du roman éponyme de Vikram Chandra, 2006.

[2](#_ftnref2) Amit Kumar, _Monsoon Shootout_, Yaffle Films Sikhya Entertainment, Inde, film 92 minutes, 2013.

[3](#_ftnref3) Rosie Llewellyn-Jones, _The Last King of India: Wajid 'Ali Shah, 1822-1887,_ Hurst Publishers, Londres, 2014.

[4](#_ftnref4) Abhijit Panse, _Thackeray_, Viacom18 Motion Pictures, 139 minutes, 2019.

[5](#_ftnref5) Nandita Das, _Manto_, HP Studois, FilmStoc, Nandita Das Initiatives, film 116 minutes, 2018.

[6](#_ftnref6) Rahul Dholakia, _Raees,_ Red Chillies Entertainment, film 142 minutes, 2017.

[7](#_ftnref7) Satyajit Ray, _Nayak_, R. D. Banshal & Co, film 117 minutes, 1966.

[8](#_ftnref8) Dibakar Banerjee, _Star_ court métrage in _Bombay Talkies,_ film à sketches indien réalisé par Zoya Akhtar, Dibakar Banerjee, Karan Johar, Anurag Kashyap, Viacom 18 Motion Pictures, 80 minutes, 2013.