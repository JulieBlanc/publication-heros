+++
author = "Natalia Baudoin"
bio = "Natalia Baudoin est artiste designer bolivienne et vénézuélienne, actuellement doctorante SACRe au sein des groupes Symbiose et Soft Matters de ENSADLab. Sa thèse « Design Convergent, Bricologies Symbiotiques » étudie la valorisation de l’artisanat traditionnel en France et en Amérique Latine par le design. Son travail de recherche-création consiste à interroger les domaines d’application de ces savoirs artisanaux autochtones pour les intégrer à notre système de production tout en respectant leur culture et leur cosmologie. Elle travaille également en partenariat avec des banques solidaires en vue de favoriser l’intégration socio-économique des communautés d’artisans d’Amérique Latine."
categories = ["design", "analyse"]
class = "has-aside-content"
contribution = true
cover = "natalia-2/natalia2-cover.gif"
coverVideo = "natalia-2/natalia-cover.mp4"
creditsPhoto = " « The Propeller »,Odo Fiovaranti, animation gif extraite de <em>Hephaestus - New Heroes</em> Source: https://www.youtube.com/watch?v=vJCVH0aKqgo&t=1s <br> « The Forecaster », Odo Fiovaranti, animation gif extraite de <em>Hephaestus - New Heroes</em> Source: https://www.youtube.com/watch?v=vJCVH0aKqgo&t=1s <br>  « The Connector », Odo Fiovaranti, animation gif extraite de <em>Hephaestus - New Heroes</em> Source: https://www.youtube.com/watch?v=vJCVH0aKqgo&t=1s <br>  « The Carrier », Odo Fiovaranti, animation gif extraite de <em>Hephaestus - New Heroes</em> Source: https://www.youtube.com/watch?v=vJCVH0aKqgo&t=1s <br>  « The Feeder », Odo Fiovaranti, animation gif extraite de <em>Hephaestus - New Heroes</em> Source: https://www.youtube.com/watch?v=vJCVH0aKqgo&t=1s  "
date = ""
identifiant = "natalia-2"
subtitle = ""
title = "Nouveaux héros, le&nbsp;pouvoir des&nbsp;objets"
weight = 10

+++
{{% divclass class="group" %}}

<div class="group-aside group-only-aside single-only">  
<img src="/uploads/natalia-2/the-propeller.gif" loading="lazy"> 
<img src="/uploads/natalia-2/the-forecaster.gif" loading="lazy"> 
<img src="/uploads/natalia-2/the-connector.gif" loading="lazy"> 
<img src="/uploads/natalia-2/the-carrier.gif" loading="lazy"> 
<img src="/uploads/natalia-2/the-feeder.gif" loading="lazy"> 
</div>

{{% divclass class="group-content" %}}


Lors de la 25e Biennale de design de Ljubljana, le designer Odo Fioravanti a été convoqué pour travailler, avec une personnalité slovène et une équipe de volontaires, sur la thématique des « Nouveaux héros » en relation avec la question de la migration.

_Aephaestus_, sa proposition, consiste en une collection de 5 objets dont la typologie peut être comparée à celle d’un sceptre ou d’une lance. Chacun de ces objets est associé à une fonction symbolique rappelant les difficultés rencontrées par les migrants lors de leur voyage.

Ainsi, « _The Propeller_ », l’hélice, représente le besoin de traverser l’océan. « _The Forecaster_ », le météorologue, représente le grand risque que courent les migrants face aux intempéries. _« The Connector »_, le connecteur, fait référence à la manière de se servir de la technologie des migrants : téléphones portables, réseaux sociaux, électricité, etc. L’accès aux réseaux à travers le portable constitue un outil essentiel à la survie. _« The Carrier »_, le transporteur, fait référence à ces quelques objets personnels que les migrants prennent dans leur voyage afin de garder des traces de leur identité. L’objet se dessine comme une aide au transport d’un petit ballot. _« The Feeder »,_ la source d’alimentation, représente les énormes difficultés rencontrées par les migrants pour s’alimenter lors de leur périple. L’objet se dessine comme un distillateur pouvant transformer de l’eau salée ou contaminée en eau potable.

<div class="aside-center">
<img src="/uploads/natalia-2/the-propeller.gif" class="single-only" loading="lazy">
<img src="/uploads/natalia-2/the-forecaster.gif" class="single-only" loading="lazy">
<img src="/uploads/natalia-2/the-connector.gif" class="single-only" loading="lazy">
<img src="/uploads/natalia-2/the-carrier.gif" class="single-only" loading="lazy">
<img src="/uploads/natalia-2/the-feeder.gif" class="single-only" loading="lazy">
<img src="/uploads/natalia-2/the-propeller_column.gif" class="column-only" loading="lazy">
<img src="/uploads/natalia-2/the-forecaster_column.gif" class="column-only" loading="lazy">
<img src="/uploads/natalia-2/the-connector_column.gif" class="column-only" loading="lazy">
<img src="/uploads/natalia-2/the-carrier_column.gif" class="column-only" loading="lazy">
<img src="/uploads/natalia-2/the-feeder_column.gif" class="column-only" loading="lazy"> 
</div>



Cette proposition questionne la figure du héros : qui sont les héros d’aujourd’hui et comment, à travers l’objet, un récit héroïque peut-il être construit ? Cela interroge également l’identité des destinataires de ce récit : s’agit-il des migrants, dont ces objets sont inspirés, ou d’un public plus large ? Quel est l’impact de la mise au monde de tels objets ?

Depuis la nuit des temps, les récits mythologiques ont accompagné toutes les cultures, leur permettant de se pérenniser. Le récit mythologique a été une manière de transmettre l’histoire des peuples tout en véhiculant les us et coutumes, les mœurs et les croyances. Ces récits décrivent souvent les exploits de figures héroïques qui ont marqué l’histoire des peuples. Ces figures héroïques servent souvent à la transmission de valeurs — l’étymologie du mot y fait référence. Selon le dictionnaire de la langue française d’Alain Rey, le mot héros « est un emprunt au latin classique _héros_ “demi-dieu”, “homme de grande valeur”, du grec _hêrôs_ “chef”, désignant les chefs militaires de la guerre de Troie comme Ulysse ou Agamemnon \[…\]. C'est avec le sens de “demi-dieu” que héros est introduit, aujourd'hui en emploi didactique ; héroïne est employé dans un contexte mythologique, au sens de “femme qui s'est distinguée par une grande action” \[…\][\[1\]](#_ftn1). »

Après avoir étudié de façon systématique de nombreux mythes issus de diverses civilisations, Joseph Campbell est parvenu à la conclusion que l’on pouvait identifier une structure récurrente et universelle commune à tous ces récits. C’est ce qu’il a appelé le « monomythe ». Selon lui, tous les mythes du monde racontent essentiellement la même histoire, le voyage épique d’un héros ou d’une héroïne. Dans son livre _Le Héros aux mille et un visages_, Campbell décrit la figure du héros à partir de ce récit de voyage, où le héros est engagé dans une entreprise ou une mission. Il devra quitter sa terre natale pour aller vers un territoire inconnu, inexploré, hostile. Une fois sa mission accomplie, le héros retournera à son lieu d’origine pour rejoindre ses proches, enrichi de ses aventures.

> Un héros s'aventure à quitter le monde du quotidien pour un territoire aux prodiges surnaturels : il y rencontre des forces fabuleuses et y remporte une victoire décisive. Le héros revient de cette mystérieuse aventure avec la faculté de conférer des pouvoirs à ses proches. [\[2\]](#_ftn2)

Pour la réalisation d’_Aephaestus_, Odo Fioravanti avait comme point de départ la commande des curatrices de la Biennale de design de Ljubljana : « Les cartes que j'avais à jouer étaient les suivantes : le thème « _New Heroes »_ qui impliquait de réfléchir à ce qui était considéré comme de l'héroïsme dans le passé et ce qu'il est aujourd'hui. Pour ce faire, j’ai été associé à Marin Medak, un kayakiste de 27 ans qui a traversé l’Atlantique avec trois autres personnes et aucun soutien au-delà de la force physique du groupe et du canoë lui-même. Les curatrices ont fait le lien entre un kayakiste traversant l'océan pour l'esprit sportif et des migrants entreprenant des traversées dangereuses par nécessité : dans les deux cas, des vies sont en danger, mais pour des raisons évidemment très différentes[\[3\]](#_ftn3). »

Le design, comme toutes les pratiques artistiques, raconte un récit, il porte un discours dans les objets qu’il produit à travers les formes et les matériaux choisis. Selon Odo Fioravanti, « \[…\] le design est, à mon avis, un outil de développement d'objets qui peuvent aussi provoquer et encourager la réflexion[\[4\]](#_ftn4). » Faire naître un objet pour un designer est aussi une manière de raconter une histoire. L’objet raconte un contexte social, des usages, des conditions de production, un environnement, etc. Pour Odo Fioravanti, la question était délicate. Que peut-on dire sur les situations tragiques qui poussent les migrants dans leur voyage ? Que peut-on dire de ce voyage entrepris dans des conditions extrêmement dangereuses, pour sortir de situations humainement insoutenables, dans l’espoir de retrouver, peut-être, un avenir meilleur ?

L’intuition des curatrices n’était pas anodine. En effet, le récit de la migration pourrait correspondre à la structure du monomythe[\[5\]](#_ftn5) : le récit d’un voyage épique où les héros sont des personnes poussées à quitter leur monde quotidien pour un monde inconnu et hostile avec pour mission de retrouver le bien-être de leurs familles. Dans le monomythe, le héros revient de son aventure enrichi de ses expériences. Pour les migrants, ce retour n’est pas possible.

Si le mythe sert à la transmission de notre histoire, quelle est l’histoire qui est racontée par les objets d’Odo Fioravanti ? Quelles sont les traces de cette vague migratoire qui seront découvertes pour finir dans des musées ethnographiques ?

_Aephaestus_ propose une narration symbolique de ce périple à travers des objets qui représentent les différents aspects de ce voyage sans pour autant prétendre être fonctionnels et proposer des solutions à ces problèmes. Tous ces objets sont volontairement inutiles. Ils se présentent peut-être comme une forme d’énonciation (ou de dénonciation ?) des difficultés auxquelles sont confrontés les migrants.

Leur forme allongée fait référence à des armes de guerriers mythologiques. Consulté à cet égard, Odo Fioravanti déclare : « \[…\] en fait, nous les appelons des “armes non offensives”, quelque chose comme des uniformes de cérémonie qui ne sont jamais utilisés en temps de guerre mais qui le représentent en temps de paix. En effet, ils sont très proches du concept d'offrande votive[\[6\]](#_ftn6). »

En effet, pour lui, « c'était un exercice d'expiation, une prière non religieuse du designer face à une tragédie incommensurable pour laquelle je ne peux même pas imaginer une solution possible — un exercice de réflexion, d'attention, une approche dans la mesure de mes possibilités. J'ai pensé à donner à ce projet le nom du dieu Héphaïstos qui, dans la mythologie grecque, fabriquait des armes pour les dieux et les héros, en les imprégnant d'instruments protecteurs pour les dieux[\[7\]](#_ftn7). »

Ces objets ne sont pas destinés aux migrants, mais racontent le récit de migration comme un récit héroïque dont les personnages entreprennent une mission pour sortir des dangers qui les menacent dans leurs pays d’origine. Alors qu’en Europe des discours de haine et des représentations fallacieuses des migrants se multiplient dans les médias, le travail d’Odo Fioravanti se présente comme une lecture alternative de la situation. Ces objets permettent de raconter un récit différent sur la migration, où l’on reconnaît le courage de ces personnes. Pour Odo Fioravanti, c’est une prière à travers laquelle il espère développer une lueur d’empathie et d’admiration pour ces populations. Cependant, ces objets vont au-delà de cette prière en faisant du terme « migrant » un synonyme de « héros », le transformant en un terme à valeur positive.

{{% /divclass %}}
{{% /divclass %}}

***

[\[1\]](#_ftnref1) Alain Rey, _Dictionnaire Historique de la Langue Française_, Le Robert, Paris, 2016.

[\[2\]](#_ftnref2) Joseph Campbell, _Le héros aux mille et un visages_, Oxus, Paris, 2010.

[\[3\]](#_ftnref3) Odo Fioravanti en entretien avec Domitila Dardi dans “Symbolic Tools for Fundamental Rights”, _Bio 25 Faraway, So Close_, Catalogue de la 25ème Biennale de Design de Ljubljana, Slovenie, 2017, p.269. Traduit par nos soins.

[\[4\]](#_ftnref4) _Ibid_, p.270. Traduit par nos soins.

[\[5\]](#_ftnref5) Selon Campbell, le monomythe décrit un voyage qui comprend 12 étapes. Voir schéma figure 2.

[\[6\]](#_ftnref6) _Ibid_, p.282. Traduit par nos soins.

[\[7\]](#_ftnref7) _Ibid_, p.282.