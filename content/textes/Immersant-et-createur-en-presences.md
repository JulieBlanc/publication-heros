+++
author = "Rémi Sagot-Duvauroux"
bio = "Rémi Sagot-Duvauroux commence comme assistant-monteur dans le cinéma d’animation sur les films : _Loulou, l'incroyable secret_ d'Éric Omond, _La Tortue rouge_ de Michael Dudok de Wit et _La fameuse Invasion des ours en Sicile_ de Lorenzo Mattotti. Par la suite, il monte des films documentaires : _Domba, la Goutte en or_ de Boubacar Coulibaly et _Les Damnés de la Commune_ réalisé par Raphaël Meyssan. En octobre 2020, il intègre le programme doctoral SACRe pour la réalisation de sa thèse : « Structures spatio-temporelles des réalités virtuelles ». Basée sur la création de dispositifs artistiques et expérimentaux, sa recherche explore la notion de montage comme moyen narratif et d’expression artistique dans des environnements virtuels."
categories = ["cinema", "performance", "pratique"]
class = "center-content"
contribution = true
cover = "remi/cover-ArticleRemi.png"
date = ""
id = "remi"
identifiant = "remi"
subtitle = ""
title = "Immersant·e et&nbsp;créateur·rice en présences"
weight = 5

+++
L’oscillation entre présence et absence lorsque nous lisons un livre ou lorsque nos pensées divaguent est un phénomène nous permettant d’établir une frontière poreuse entre le monde physique et celui de la fiction. L’immersion sensorimotrice en réalité virtuelle et les réponses comportementales qui en découlent redéfinissent profondément notre rapport à une diégèse et à notre identité en son sein. Cette nouvelle phénoménologie bouscule le contrat entre récepteur·rice et créateur·rice. Le dispositif _The Buster Experience_ (Figures 1 et 2) tente d'interroger ce contrat en proposant une construction bilatérale de la forme narrative et de l’identité du personnage. De manière empirique, cette expérience recherche les codes d’un nouveau design d’expérience révélant de nouvelles possibilités de percevoir un espace narratif transcendant le temps et l’espace de représentation.

<div id="remi_fig1" class="parallax figure">
<img class="layer-bg single-only" src="/uploads/remi/fig1/fig1-BG.png" loading="lazy"/>
<img class="layer1 single-only" src="/uploads/remi/fig1/fig1-OL.png" loading="lazy"/>
<img class="column-only" src="/uploads/remi/fig1/fig1.png" loading="lazy"/>
</div>
<p class="figcaption">Figure 1. Rémi Sagot-Duvauroux, <em>The Buster Experience</em>, Spatial Media / EnsadLab, 2020.</p>

Faire partie d’une histoire écrite par quelqu’un d’autre. Oublier son environnement physique et déplacer sa conscience dans l’environnement d’une histoire qui nous est contée. Avoir l’illusion d'appartenir à une diégèse, c'est-à-dire au monde de l’histoire que l'œuvre nous donne à lire. Faire tellement corps avec un espace de représentation qu’on oublie son appartenance à une réalité habituelle. Des livres dont vous êtes le héros aux expériences de réalité virtuelle en passant par les jeux de rôle, les jeux vidéos et le théâtre immersif, les tentatives pour immerger cognitivement et/ou corporellement des spectateur·rice·s dans des histoires ont été nombreuses et n’ont eu de cesse de questionner la relation entre créateur·rice·s et récepteur·rice·s.

Qu’il soit spectatoriel, fictionnel, ludique ou fonctionnel, un contrat tacite définit le statut et le rôle de chacun·e dans la lecture d’une œuvre et participe à l’émergence de la forme et de la nature de la médiation. Les dispositifs de réalité virtuelle permettent une activité sensorimotrice et cognitive[\[1\]](#_ftn1) dans une simulation numérique pouvant être l’espace de représentation d’une diégèse. Même si un environnement virtuel est imaginé, modélisé et codé préalablement par un·e créateur·rice, il ne peut exister qu’à travers son actualisation par un·e « immersant·e » : une personne immergée dans un espace numérique dont le statut dépend du lien qu’il·elle va créer avec l'œuvre tout au long de l’expérience.

Mais alors, en tant qu’immersant·e, quel point de vue acquiert-on ? Percevons-nous, jouons-nous, accomplissons-nous une performance ou bien restons-nous nous même ? En actualisant un monde virtuel fictif, ne cherche-t-on pas naturellement à incarner le personnage principal, le·la héros·ïne de notre expérience ? Mais ne se retrouve-t-on pas inexorablement contraint d’être le·la héros·ïne, demi·e-dieu·éesse archétypal·e fantasmé·e et façonné·e par un·e créateur·rice ? Les spécificités phénoménologiques de la réalité virtuelle nous amènent à repenser, par la pratique, notre distance à une diégèse et à son·sa créateur·rice. Cette réflexion ouvre ainsi de nouvelles perspectives sur la construction d’une identité héroïque tant du point de vue narratif (incarnation d’un personnage) que symbolique (idéalisation de cette incarnation).

<div id="remi_fig3" class="parallax figure">
<img class="layer-bg single-only" src="/uploads/remi/fig3/fig3-BG.png" loading="lazy" >
<img class="layer1 single-only" src="/uploads/remi/fig3/fig3-OL.png" loading="lazy" >
<img class="column-only" src="/uploads/remi/fig3/fig3_400.png" loading="lazy" >

</div> <p class="figcaption">Figure 2. L'immersante en interaction avec l'environnement virtuel.</p>

Les dispositifs de réalité virtuelle permettent la médiation d’environnements artificiels dans le temps qui sont perçus dans les mêmes dimensions que celles de notre système perceptif humain. Nous pouvons ainsi être immergés par certains de nos sens dans des espaces numériques avec lesquels nous pouvons interagir corporellement par notre activité motrice[\[2\]](#_ftn2). Cette médiation spatiale alliant nos sens extéroceptifs (qui perçoivent l’environnement) et proprioceptifs (qui perçoivent le corps par rapport à l’environnement) implique une reconsidération du point de vue narratif, notamment à cause du phénomène de présence qu’elle peut entraîner.

Le sens de la présence dans un environnement virtuel est décrit comme la sensation subjective « d’être là » lorsqu’on interagit avec un environnement médiatisé[\[3\]](#_ftn3). Selon les caractéristiques techniques du dispositif, les possibilités d’interactivité et les attributs de l’immersant·e (physiologie, expérience des mondes virtuels[\[4\]](#_ftn4)), ce·tte dernier·ère peut accéder à différentes « profondeurs de présence[\[5\]](#_ftn5) ». Palier par palier, le Soi se délocalise dans l’environnement virtuel à tel point qu’il peut être amené à avoir les mêmes émotions et réactions que dans une expérience réelle[\[6\]](#_ftn6).

Patrice Bouvier résume la différence entre la réalité virtuelle et les médias tels que la littérature ou le cinéma ainsi : « Si un feu surgit dans l’environnement virtuel, l’utilisateur pourrait alors se mettre à courir ou au moins avoir un mouvement de recul. Par contre, si un livre ou un film décrit un feu, le lecteur ne lâcherait pas le livre de peur de se brûler les mains et le spectateur n’évacuerait pas la salle de projection[\[7\]](#_ftn7) »_._ Mel Slater explique que le livre ne peut produire qu’une réponse émotionnelle puis physiologique, alors qu’un dispositif de réalité virtuelle, par le phénomène de présence, est susceptible de provoquer une réponse comportementale[\[8\]](#_ftn8).

Nous sommes habitués, par les immersions extéroceptives, à nous immerger cognitivement dans des histoires situées dans des ailleurs imaginés et représentés. Les réponses comportementales en réalité virtuelle ne rentrent-elles pas en conflit avec le recul nécessaire pour envisager une œuvre narrative par rapport à sa propre existence ? Quelle place est alors laissée à la perception d’un espace narratif, un espace hors champ se libérant du temps et de l’espace de la simulation ?

Selon Philippe Quéau, la distance entre le sujet et le monde virtuel, entre sa perception et sa compréhension nous permet de considérer le Soi, d’adopter une position et d’avoir une conscience du lieu[\[9\]](#_ftn9). Ne pas être trop présent physiquement dans une œuvre serait donc un moyen de rappel au monde et de prise de conscience de la médiation. De plus, comme l’espace virtuel diffère indéniablement de l’espace réel, la présence trop marquée de notre corps réel dans l’environnement virtuel peut mettre en évidence l’artificialité de cet espace. Il devient alors plus compliqué d’accepter cet espace de fiction et d’effectuer l’opération de suspension consentie de l’incrédulité[\[10\]](#_ftn10) et de mettre de côté notre scepticisme. L’évolution du niveau de présence au gré d’une expérience de réalité virtuelle semble donc être un outil dramaturgique essentiel dans la mise en œuvre d’une expérience narrative.

Cette gradation allant de la présence à l'absence dans l’environnement virtuel établit une distance variable vis-à-vis de la diégèse et pose donc la question de notre identité dans de telles expériences. Si je me sens trop peu présent dans l’environnement virtuel, il semblerait alors plus aisé de me désolidariser de l’espace diégétique pour avoir un point de vue détaché et désincarné. Mais ne perd-on pas ainsi un point d’accroche et d’entrée dans le médium, dont l’essence même est cette immersion proprioceptive qui inclut notre corps dans l’espace de représentation ? Et si j’oublie complètement la médiation au point d’avoir le sentiment d'appartenir à la diégèse, ai-je le recul nécessaire pour avoir conscience d’être un personnage fictif ? Ne retrouve-t-on pas là le « paradoxe sur le comédien » décrit en 1769 par Denis Diderot[\[11\]](#_ftn11) opposant le jeu théâtral et le degré de distance émotionnelle qu’un comédien établit avec son personnage ? Ce parallèle nous permet de questionner cet état intermédiaire dans lequel l’immersant·e se trouve, entre jeu et incarnation.

Comme une pièce de théâtre, une expérience de réalité virtuelle ne peut pas être étudiée comme une forme finie. C’est uniquement en étant actualisée par l’immersant·e qu’elle peut se réaliser et prendre forme. Chaque expérience est donc unique et chaque histoire vécue est différente. Les espaces et objets virtuels sont néanmoins orientés et hiérarchisés par un·e créateur·rice. Le rôle de l’immersant·e est donc à minima préétabli. Les lois qui régissent le monde virtuel, les contraintes de perception et d’action, le corps qui nous est donné, les espaces-temps qui nous sont proposés nous guident, déterminent différents destins, circonscrivent nos actions sur le monde virtuel et nous invitent ainsi à adopter un comportement réfléchi préalablement par un·e créateur·rice. Ce·tte dernier·ère et l’immersant·e ne peuvent donc pas se passer l’un·e de l’autre. Ils·elles sont interdépendant·e·s, co-construisent une histoire ensemble et devraient ainsi pouvoir partager l’autorité de l'œuvre narrative. C’est dans ce contexte que le projet de création _The Buster Experience_ propose un paradigme de recherche pour étudier ce phénomène de co-création et d’autorité partagée.

Dans le film _Sherlock Jr._[\[12\]](#_ftn12), le personnage interprété par Buster Keaton se précipite à l’intérieur de l’écran de cinéma pour sauver sa bien-aimée. Malheureusement pour lui, au moment où il saute dans l’écran, une coupe de montage le transporte dans un autre lieu. Le personnage est entré dans le film mais pas là où il aimerait être. Il se retrouve piégé par le montage du film. Il s’ensuit une série de coupes entraînant le personnage dans de multiples endroits. Le montage devient alors un obstacle pour le personnage qui subit ces ruptures spatiales contre son gré. Alors que les environnements changent autour de lui de manière imprévue, le corps du personnage, « non coupé » par le montage, figure une continuité entre les plans (Figure 3).

<div class="figure" id="remi_fig2">
<div class="scrollyAlone" id="fig2a" data-img-link="remi/fig2/fig2a/fig2a_" data-img-type="jpg" data-img-nbr="130" data-vitesse="50" data-w="800" data-h="600"></div>
<div class="scrollyAlone" id="fig2b" data-img-link="remi/fig2/fig2b/fig2b_" data-img-type="jpg" data-img-nbr="130" data-vitesse="50" data-w="800" data-h="600"></div>
<div class="scrollyAlone" id="fig2c" data-img-link="remi/fig2/fig2c/fig2c_" data-img-type="jpg" data-img-nbr="130" data-vitesse="50" data-w="800" data-h="600"></div>
<img src="/uploads/remi/fig2/fig2a.gif" class="gif-page-column" loading="lazy">
<img src="/uploads/remi/fig2/fig2b.gif" class="gif-page-column" loading="lazy">
<img src="/uploads/remi/fig2/fig2c.gif" class="gif-page-column" loading="lazy">

</div>
<p class="figcaption">Figure 3. Buster Keaton, <em>Sherlock Jr.</em>, 1924, noir et blanc, muet, 35 mm, 44 min.</p>

Un jeu s’installe alors entre le héros et un·e « monteur·se invisible », sorte de « magicien·ne d’Oz » opérant dans l’ombre de la diégèse. Buster Keaton, par son corps et son talent d’acteur, rend les imprévisions spatiales ludiques, cohérentes et fait en sorte qu'elles soient perçues comme une continuité narrative. Le personnage burlesque, surpris de ses propres maladresses influencées par les changements d’espace, développe un regard extérieur sur l’expérience qu’il vit, lui donnant un point de vue sur son propre corps.

Et si, comme Buster Keaton, nous pouvions entrer dans un film et vivre une continuité narrative à travers un espace-temps inconnu et fragmenté ? C’est le pari du dispositif de réalité virtuelle _The Buster Experience._ Il s’inspire de cette séquence visionnaire de 1924 préfigurant la sensation d'une personne immergée dans une expérience de réalité virtuelle en étant à la fois spectatrice et actrice d'un récit qui la transporte involontairement d'une scène à l'autre. Dans ce dispositif, un·e immersant·e entre dans l’écran (Figure 2) et va faire l’expérience de la fragmentation de l'espace-temps. Les ruptures spatiales sont enclenchées par un·e opérateur·rice extérieur·e qui « monte » l'expérience en temps réel (Figure 4). L’immersant·e parcourt des espaces dans lesquels sa présence est mise à l’épreuve par le·la monteur·se façonnant le film qu’il·elle veut voir.

<div id="remi_fig4" class="parallax figure">
<img class="layer-bg single-only" src="/uploads/remi/fig4/fig4-BG.PNG" loading="lazy" >
<img class="layer1 single-only" src="/uploads/remi/fig4/fig4-OL.PNG" loading="lazy" >
<img class="column-only" src="/uploads/remi/fig4/fig4_400.png" loading="lazy" >

</div>

<!-- <div id="remi_fig4" class="figure">
<img class="layer1 single-only"src="/uploads/remi/fig4/fig4.png" loading="lazy">
<img class="layer1 column-only" src="/uploads/remi/fig4/fig4_400.png" loading="lazy">

</div> --> <p class="figcaption">Figure 4. Le monteur (à droite) transportant arbitrairement l’immersant (à gauche).</p>

Selon la thèse actionniste[\[13\]](#_ftn13), c’est en agissant intentionnellement sur le monde que nous le réalisons. La thèse de l' « énaction » issue des travaux de Francisco Varela et de ses collaborateurs[\[14\]](#_ftn14) suggère que le phénomène de présence émergerait en percevant une transformation réussie des intentions en action. En réalité virtuelle, ce phénomène serait donc intimement lié à notre agentivité, c'est-à-dire à notre capacité d'agir et de laisser une empreinte dans l'environnement virtuel. Dans notre dispositif, la rupture de cette agentivité par le montage constituerait donc un moyen de réguler l'appartenance de l'immersant·e à l'univers diégétique.

La construction (ou la déconstruction) d’un personnage est donc certes menée par l’immersant·e (à la fois spectateur·rice, acteur·rice et actant·e) mais se retrouve « coupé·e » dans ses intentions contraintes par le.la « monteur.se » qui mène arbitrairement sa narration en temps réel en fonction de ses envies et des faits et gestes de l’immersant·e (Figure 5).

{{% divclass class="full-video" %}}
<video id="remi_fig5" controls loop class="videoPlay lazy" poster="/uploads/remi/fig5.png">
<source data-src="/uploads/remi/fig5.mp4"  type="video/mp4">
Sorry, your browser doesn't support embedded videos.
</video>
{{% /divclass %}}

<p class="figcaption">Figure 5. L'immersant et son point de vue dans l'expérience.</p>

Deux points de vue, deux espaces de représentation entrent alors en conflit. Chacun tente de s’adapter, d’anticiper et de donner du sens à cette narration qui se construit en temps réel. Une boucle empirique apparaît alors, construisant une nouvelle forme narrative où la construction d’un·e héros·ïne se fait dialectiquement. Les deux points de vue (l’un en immersion proprioceptive et l’autre en immersion extéroceptive) participent à établir un imaginaire partagé dans lequel chacun construit son rôle. Le niveau de présence de l’immersant·e dans les environnements virtuels est affecté par les coupes de montage modifiant en temps réel sa conscience de la médiation et du rôle qu’il·elle a à jouer.

Les erreurs, les incompréhensions, les ennuis, les frustrations, les surprises, les émerveillements... Chaque expérience subjective participe à redéfinir la forme du dispositif. Cette expérience fait l’hypothèse que, comme pour le montage cinématographique, c’est en mettant en friction les espaces, en bousculant le principe de continuité, qu’on pourra tomber sur des accidents remarquables et passionnants[\[15\]](#_ftn15). C’est en faisant émerger de la discontinuité là où l’on attendrait un prolongement (optique, temporel, spatial, expérientiel...) que pourraient se révéler des collisions signifiantes et des formes originales. Ces discontinuités devraient néanmoins s’organiser autour de structures intelligibles pour l’immersant·e dans l’idée d’une efficience narrative de l’expérience.

Un aspect intéressant de ce dispositif est qu’il peut provoquer ou être révélateur de situations héroïques. En réalité virtuelle, les stimuli sensoriels que nous percevons peuvent tromper notre jugement et nous donner de vraies sensations de vertige, de vulnérabilité, de danger ou d’adversité. Nous sommes donc amenés à être exposés, à nous mettre en jeu. Or une attitude héroïque émerge souvent d’une prise de risque. En immergeant une personne dans des situations où elle se sentirait vulnérable, le·la créateur·rice pourrait ainsi l’inciter à réagir et à être héroïque. Plus on se sentirait présent dans un environnement virtuel, plus notre attitude héroïque serait alors intuitive et naturelle et non un rôle joué et attribué par ce que Jean-Marie Schaeffer nomme la feintise ludique (« pour de faux » permettant l'immersion mimétique dans l'univers fictionnel[\[16\]](#_ftn16)). La présence serait alors garante de l'authenticité du courage comme de la lâcheté de l’immersant·e au regard l'expérience qu’il traverse.

En fonction de leurs actions et réactions à ce qu’il advient autour d’eux, les immersant·e·s font évoluer en temps réel la représentation de ce qu’ils incarnent. La mise au défi du statut initial de l’immersant·e par le·la créateur·rice serait donc un levier narratif dans l’évolution du point de vue de l’immersant·e sur son identité diégétique. La construction ou déconstruction du·de la héros·ïne devient alors paradigmatique dans l’étude de la distance entre l’immersant·e et le monde de la diégèse.

Le dispositif _The Buster Experience_ permet au·à la monteur·se de moduler le « voyage » de l’immersant·e. Selon ses humeurs, ses caractères, ses obsessions, ses intérêts personnels, sa cruauté, son humour, il·elle influe sur le « destin » de l'immersant·e, parsemant sa route d’obstacles ou d’appuis. Ce qui n’est pas sans rappeler l’ascendance des déesses et dieux grecs jouant de leurs puissances divines pour mettre à l’épreuve les héros·ïnes dans leur voyage, participant ainsi à la création des mythes. Le·la monteur·se de notre dispositif serait alors une sorte de démiurge de l’environnement virtuel.

Au cours de l'expérience, l'immersant·e va passer de l'ignorance à la conscience de cette force cachée qui fragmente arbitrairement son espace-temps et modifie son expérience narrative. Dans une expérience performative, le·la monteur·se commence par se manifester vocalement dans l’espace sonore de l’immersant·e. Petit à petit, un lien se crée et vient même jusqu’à se matérialiser par l’apparition de l’image du démiurge dans certains environnements virtuels (Figure 6). Cette apparition vient créer un portail entre les deux réalités (virtuelle et physique) reliant ainsi les deux imaginaires, modifiant les présences et les statuts des deux participants. Le·la monteur·se se révèle alors comme personnage de la diégèse incluant par la même occasion le monde physique du dispositif dans l’univers fictionnel. L’espace narratif créé se matérialise alors au-delà de la simulation numérique laissant présager une persistance dans le monde réel des identités héroïques révélées par l’expérience.

<div id="remi_fig6" class="figure">
<img class="single-only" src="/uploads/remi/fig6/fig6.png" loading="lazy">
<img class="column-only" src="/uploads/remi/fig6/fig6_400.png" loading="lazy">
</div>
<p class="figcaption">Figure 6. L'apparition du monteur dans l'environnement virtuel.</p>

[\[1\]](#_ftnref1) Philippe Fuchs, Guillaume Moreau, Malika Auvray et École nationale supérieure des mines de Paris, _Le traité de la réalité virtuelle_, Presses de l’Ecole des mines, Paris, 2006.

[\[2\]](#_ftnref2) _ibid._

[\[3\]](#_ftnref3) Thomas Schubert, Frank Friedmann, Holger Regenbrecht, « Embodied presence in virtual environments », _In Ray Paton & Irene Neilson (Eds.), Visual Representations and Interpretations_, Springer, Londres, 1999, p. 269-278.

[\[4\]](#_ftnref4) Martijn J. Schuemie, Peter Van der Straaten, Merel Krijn, Charles A.P.G. Van der Mast « Research on Presence in Virtual Reality: A Survey », _CyberPsychology & Behavior_ 4, no 2, avril 2001, p. 183-201.

[\[5\]](#_ftnref5) Mel Slater, Martin Usoh, Anthony Steed, « Depth of presence in virtual environments », _Presence: Teleoperators and Virtual Environments_, vol. 3, no. 2, 1994, p. 130–144.

[\[6\]](#_ftnref6) Giuseppe Riva, Fabrizia Mantovani, Claret Samantha Capideville, Alessandra Preziosa \[et al.\], « Affective interactions using virtual reality: the link between presence and emotions » _Cyberpsychology & behavior : the impact of the Internet, multimedia and virtual reality on behavior and society_, vol. 10, no. 1, 2007, p. 45–56.

[\[7\]](#_ftnref7) Patrice Bouvier, _La présence en réalité virtuelle, une approche centrée utilisateur_, Thèse de doctorat, Université Paris-Est, sous la dir. de Gilles Bertrand, 2009.

[\[8\]](#_ftnref8) Mel Slater, « A Note on Presence Terminology », _Emotion_, vol. 3, Londres, 2003, p. 1–5.

[\[9\]](#_ftnref9) Philippe Quéau, _Le virtuel: vertus et vertiges_, Champ Vallon, Institut national de l’audiovisuel, Paris, 1993.

[\[10\]](#_ftnref10) Samuel Taylor Coleridge, _Biographia Literaria; or, Biographical sketches of my literary life and opinions,_ Londres, 1817.

[\[11\]](#_ftnref11) Denis Diderot, Sabine Chaouche, _Paradoxe sur le comédien_. GF Flammarion, Paris, 2000.

[\[12\]](#_ftnref12) Buster Keaton, _Sherlock Jr._, 1924, noir et blanc, muet, 35 mm, 44 min.

[\[13\]](#_ftnref13) Alva Noë, _Varieties of presence_, Mass: Harvard University Press, Cambridge, 2012.

[\[14\]](#_ftnref14) Francisco Varela, Evan Thompson, Eleanor Rosch, _The embodied mind: Cognitive science and human experience,_ The MIT Press, Cambridge, 1991.

[\[15\]](#_ftnref15) Vincent Amiel, _Esthétique du montage, 4e édition, revue et augmentée_, Armand Colin, Malakoff, 2017.

[\[16\]](#_ftnref16) Jean-Marie Schaeffer, _Pourquoi la fiction ?_, Le Seuil, Paris, 1999.