+++
author = "Dimitri Martin Genaudeau"
bio = "Dimitri Martin Genaudeau a étudié la philosophie et l’histoire du cinéma à l’Université Paris 1 Panthéon-Sorbonne et à la City University de Hong Kong, il a passé un an à Prague dans la section de réalisation documentaire de la FAMU avant de rejoindre le doctorat SACRe à la Fémis en 2018 où il mène depuis une thèse de recherche et de création sur le cinéma burlesque ; il est membre du laboratoire SACRe."
categories = ["cinema", "analyse"]
contribution = true
coverVideo = "video-hulot.mp4"
cover = "GIFHulot.gif"
creditsPhoto = "<em>Les Vacances de Monsieur Hulot</em> © Les Films de Mon Oncle – SPECTA Films C.E.P.E.C., 1953. <br> <em>Playtime </em> © Les Films de Mon Oncle – SPECTA Films C.E.P.E.C., 1967."
date = ""
identifiant = "dimitri-2"
subtitle = ""
title = "Personnages ou spectateurs&#8239;: à&nbsp;qui profite la&nbsp;démocratie burlesque de Jacques Tati&#8239;? "
weight = 2

+++


> Ce que j’ai essayé, pour ma part, c’est de prouver et faire voir que, dans le fond, tout le monde était amusant. Il n’est pas besoin d’être un comique pour faire un gag[\[1\]](#_ftn1).
> {{% footer %}}Jacques Tati{{% /footer %}}

# I. Le gag émancipé

Au moment de la sortie des _Vacances de Monsieur Hulot_ en 1953, et plus tard à l’occasion de diverses interviews, Jacques Tati présente sa conception du burlesque comme un profond renouvellement du comique traditionnel, « une tentative absolument différente, un autre genre de comique[\[2\]](#_ftn2) » : dans ses films, le déroulement des gags échappe à l’ascendant exclusif du héros, les seconds rôles et les figurants sont autant de relais de l’action comique et le burlesque s’ouvre en conséquence à ce que Tati appelle le « gag naturel », celui que l’on rencontre quotidiennement dans les petites maladresses de la vie courante et qui frappe sans discernement « riches et pauvres, grands et petits, malades et bien portants » – en somme, un gag « démocratique[\[3\]](#_ftn3) ».

_Exemple_ : Monsieur Hulot, au volant d’un tacot pétaradant, tombe en panne aux abords d’un cimetière où se tient un enterrement. Voulant réparer son engin il sort de son coffre divers outils et laisse tomber une chambre à air sur un tapis de feuilles humides : les feuilles se collent au pneu qui se transforme en couronne, laquelle est pieusement acceptée par l’ordonnateur des pompes funèbres.

<video controls preload="none" class="video-figure lazy" poster="/uploads/dimitri-2_hulot_poster.png">
    <source data-src="/uploads/dimitri-2_hulot.mp4" type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>

Commentant cet exemple, Tati se compare à Chaplin pour souligner l’originalité de sa mise en scène : « Charlot, dit-il, aurait collé lui-même les feuilles sur la chambre, transformé la chambre en couronne et elle aurait été acceptée de la même façon par le garçon qui s’occupait du service. » Avec Tati c’est le geste du garçon des pompes funèbres, acceptant le pneu comme une couronne, qui donne son sens au gag et non l’étourderie de Monsieur Hulot qui, lui, « n’invente jamais rien[\[4\]](#_ftn4) » – là où Charlot submerge le monde de sa présence, Hulot s’absente et le gag s’émancipe de son emprise : « le monde est rendu comique par l’absence de comique de Hulot[\[5\]](#_ftn5) ».

Si la comparaison avec Chaplin est éclairante, l’originalité de cette scène est moins manifeste si l’on convoque Buster Keaton ou Harry Langdon qui apparaissent de façon plus évidente comme les initiateurs de la tradition du héros passif ; il est vrai par ailleurs, comme le souligne Barthélémy Amengual, que « personne n’est drôle _en soi_, puisque nul n’est seul au monde[\[6\]](#_ftn6) ». En réalité, ce n’est pas tellement l’effacement du personnage que l’évanescence des gags qui distingue le burlesque de Jacques Tati : la structure condensée du gag traditionnel qui, sitôt amorcé, appelle une chute, est ici émiettée entre les différentes scènes, certains gags sont tout simplement tronqués, amputés du final attendu, d’autres s’estompent en d’infinis ricochets étalés sur la durée du film d’une manière tout à fait nouvelle – l’exemple, commode, que le réalisateur choisit pour faire l’exégèse de son œuvre est à cet égard peu représentatif du film, les causes et les effets étant souvent plus disjoints que dans cette scène. C’est la singularité radicale du cinéma de Jacques Tati que de prendre constamment le risque de ne pas faire rire, en préférant la poésie des harmonies secrètes à la lisibilité du gag. « Il est bien clair, écrit Michel Chion, que si Tati refuse quelque chose dans le jeu habituel du comique c’est le matraquage. Chez lui, un gag doit se débrouiller seul[\[7\]](#_ftn7). » On admettra que le gag ne saurait s’émanciper des structures qui corsètent son développement ordinaire sans que ne soit contenu, d’une manière ou d’une autre, l’empire qu’exerce le héros sur la rigolade, mais la nouveauté n’est pas ici dans l’attitude du personnage vis-à-vis du gag, comme le pense Tati, mais plutôt dans celle du cinéaste vis-à-vis du rire : sans doute en partie délivré de l’impératif commercial qui dirigeait l’industrie où exerçaient ses illustres prédécesseurs américains, pour qui le rire de la salle avait parfois le dernier mot de la mise en scène (lors des fameuses _previews_), Jacques Tati reste l’unique exemple d’un cinéaste comique dont il semble que l’ambition première n’ait pas été de faire rire – « il ne cherche pas à imposer le rire mais se contente de le proposer[\[8\]](#_ftn8) ». Une fois affranchi de l’autorité du rire, le gag avait besoin d’un écrin assorti aux richesses de ses multiples éclats ; après avoir tourné _Mon Oncle_ en 1958, Tati devait parfaire sa création dans _Playtime_ (1967), en même temps qu’il préciserait le sens de son projet de démocratisation du comique.

# II. Le plan démocratique

Quatorze ans après _Les Vacances de Monsieur Hulot_, le style du cinéaste s’est indéniablement radicalisé : dans _Playtime_, le personnage principal disparaît presque complètement dans la foule des figurants où l’on aperçoit même de <span class="keyword" data-video-1="fauxhulot_0-0.mp4" data-video-2="fauxhulot_0-1.mp4">faux Hulot</span> – des doubles dont Tati parsème son film pour mieux marquer la déchéance du héros comique traditionnel et ainsi attirer l’attention du spectateur « sur toutes les autres figures et sur tout le décor[\[9\]](#_ftn9) » – ; la trame narrative qui guidait encore de façon lointaine _Les Vacances_ et _Mon Oncle_ a presque totalement disparu, _Playtime_ déploie une série de gags qui se répondent selon des variations et des correspondances inextricables, éclatées entre les différentes séquences du film et presque impossible à démêler lors d’un premier visionnage ; enfin, Tati généralise l’usage du plan d’ensemble, c’est la véritable innovation du film : l’échelle adoptée permet de chorégraphier simultanément plusieurs gags au sein d’un même plan ; non seulement le héros burlesque n’a plus le monopole du gag mais il perd en plus sa prérogative de « personnage de premier plan[\[10\]](#_ftn10) » – là encore s’exprime le souci du cinéaste : démocratiser.

<p class="indications"><a href="/textes/personnages-ou-spectateurs/">Il est préférable de lire cet article en plein écran pour profiter des animations. <svg version="1.1" class="full-screen" xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 215.35 215.35"
        style="enable-background:new 0 0 215.35 215.35;" xml:space="preserve">
        <g>
          <path d="M7.5,65.088c4.143,0,7.5-3.358,7.5-7.5V25.61l48.305,48.295c1.465,1.464,3.384,2.196,5.303,2.196
		c1.92,0,3.84-0.732,5.304-2.197c2.929-2.929,2.929-7.678-0.001-10.606L25.604,15.002h31.985c4.142,0,7.5-3.358,7.5-7.5
		c0-4.142-3.357-7.5-7.5-7.5H7.5c-4.143,0-7.5,3.358-7.5,7.5v50.087C0,61.73,3.357,65.088,7.5,65.088z" />
          <path d="M207.85,150.262c-4.143,0-7.5,3.358-7.5,7.5v31.979l-49.792-49.792c-2.93-2.929-7.678-2.929-10.607,0
		c-2.929,2.929-2.929,7.678,0,10.606l49.791,49.791h-31.977c-4.143,0-7.5,3.358-7.5,7.5c0,4.142,3.357,7.5,7.5,7.5h50.086
		c4.143,0,7.5-3.358,7.5-7.5v-50.084C215.35,153.62,211.992,150.262,207.85,150.262z" />
          <path d="M64.792,139.949L15.005,189.74v-31.978c0-4.142-3.357-7.5-7.5-7.5s-7.5,3.358-7.5,7.5v50.086c0,4.142,3.357,7.5,7.5,7.5
		h50.084c4.142,0,7.5-3.358,7.5-7.5s-3.357-7.5-7.5-7.5H25.611l49.788-49.793c2.929-2.929,2.929-7.678-0.001-10.607
		C72.471,137.02,67.722,137.02,64.792,139.949z" />
          <path d="M207.85,0.002h-50.086c-4.143,0-7.5,3.358-7.5,7.5c0,4.142,3.357,7.5,7.5,7.5h31.979l-48.298,48.301
		c-2.929,2.929-2.929,7.678,0.001,10.607c1.464,1.464,3.384,2.196,5.303,2.196c1.919,0,3.839-0.733,5.304-2.197l48.298-48.301v31.98
		c0,4.142,3.357,7.5,7.5,7.5c4.143,0,7.5-3.358,7.5-7.5V7.502C215.35,3.359,211.992,0.002,207.85,0.002z" />
        </g>
      </svg>
      </a></p>



<video preload="none" controls class="video-figure lazy" poster="/uploads/dimitri2_playtime_poster.png">
    <source data-src="/uploads/dimitri2_playtime.mp4" type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>


_Exemple_ : Vue d’ensemble sur un vaste hall – le décor est impersonnel, composé en nuance de gris et de bleus glacés, c’est peut-être un couloir d’hôpital, mais ce serait tout aussi bien un aéroport ou le siège d’une grande entreprise. <span class="keyword" data-video-1="coupledupremierplan_1-0.mp4">Un couple est assis au premier plan</span>  sur une banquette située en bas à gauche du cadre, l’épouse, visiblement inquiète, murmure quelques recommandations inintelligibles à son mari. Leur discussion est entrecoupée par les allées et venues de différents personnages à l’arrière-plan dont l’observation attentive fournit au spectateur les premiers indices sur le lieu où se passe l’action : <span class="keyword" data-video-1="hommeauchariotdeservice_2-0.mp4">un homme en tablier conduit un chariot de service</span> où sont entreposés assiettes et couverts, une sage femme porte <span class="keyword" data-video-1="lebebe_3-0.mp4">un bébé enveloppé dans un lange</span>  tandis qu’une femme vêtue de beige pousse un parent âgé dans <span class="keyword" data-video-1="lefauteuilroulant_4-0.mp4">un fauteuil roulant</span>. Serions-nous dans un hôpital ? Le style vestimentaire de l’homme qui sort des toilettes au début du plan suivant, chaussé de pantoufles noires, <span class="keyword" data-video-1="hommeserviette_5-0.mp4">une serviette enroulée autour de la tête</span>, semble confirmer provisoirement cette intuition mais la suite de la séquence révèle toute autre chose…

À supposer qu’il existe un spectateur capable de suivre simultanément chacun des fils de cette trame arborescente, Tati nous offre en effet la possibilité, dans les plans qui suivent, de réévaluer point par point notre analyse en regardant la scène depuis son contrechamp – nous n’étions pas dans un couloir d’hôpital mais dans le hall d’un aéroport international : lorsque la sage femme nous apparaît de face, nous comprenons que ce n’était pas un bébé qu’elle portait dans ses bras mais <span class="keyword" data-video-1="lebebe_3-0.mp4" data-video-2="lapiledeserviette_3-1.mp4">une pile de serviettes</span> dont on la voit approvisionner le distributeur des toilettes, elle n’est donc pas sage femme mais agente d’entretien, un deuxième visionnage permettra de distinguer le landau gris, peu visible, du couple au premier plan, d’où s’élèvent les geignements du bébé ; de même, la femme en beige ne poussait pas un fauteuil roulant mais un simple <span class="keyword" data-video-1="lefauteuilroulant_4-0.mp4" data-video-2="leportebagages_4-1.mp4">porte-bagages</span> sur lequel étaient empilées plusieurs valises recouvertes d’un châle ; l'homme au chariot de service ne transportait pas le repas des malades mais allait approvisionner <span class="keyword" data-video-1="hommeauchariotdeservice_2-0.mp4" data-video-2="hommeauchariotdeservice_2-1.mp4">le bar de l’aéroport</span>, quant à celui qui sortait des lavabos en chaussons, nous l’apercevons plus tard rejoindre une femme vêtue à l’Orientale : son habillement n’avait donc rien d’un négligé de toilette, la serviette qu’il arborait sur le crâne était en réalité <span class="keyword" data-video-1="hommeserviette_5-0.mp4" data-video-2="hommeauturban_5-1.mp4">un turban</span>.

Pour Jacques Tati, « l’imagination vient au secours de l’observation[\[11\]](#_ftn11) », le contexte lacunaire (sommes-nous dans un hôpital ou un aéroport ?) provoque une situation éminemment suggestive dans laquelle chaque élément de l’image recèle un gag en puissance ; dans la scène qui nous occupe, le sens de certaines actions est volontairement trouble de sorte que l’hypothèse de l’hôpital puisse se maintenir suffisamment longtemps pour donner à relire les premières minutes du film à la lumière des plans suivants, le gag apparaît dans la comparaison de ces deux lectures possibles de la scène. Nous ne savons pas, par exemple, si l’inquiétude de la femme au premier plan concerne un <span class="keyword" data-video-1="coupledupremierplan_1-0.mp4" data-video-2="coupledupremierplandepart_1-1.mp4">départ en voyage</span> ou l'hospitalisation imminente de son mari, de même, l’<span class="keyword" data-video-1="officierfaitlescentpas_6-0.mp4" data-video-2="officierenretard_6-1.mp4">officier</span> qui fait les cent pas dans le couloir s’impatiente-t-il du retard d’un collègue qui compromet son départ en avion ou se fait-il du mauvais sang parce que sa femme s’apprête à donner naissance dans l’une des chambres adjacentes où disparaît celle que nous prenons au début pour une sage femme ? <span class="keyword" data-video-1="vieillesdamespretregarconaveclesfleurs_7-0.mp4" data-video-2="vieillesdamespretregarconaveclesfleurs_7-1.mp4">Les vieilles dames affligées, le prêtre et le garçon portant une gerbe de fleurs rouges</span> qui font leur entrée à la suite du monsieur en chaussons sont-ils de simples passants qui se croisent par hasard dans le hall d’un aéroport, ou se trouvent-ils ici réunis à l’occasion d’un décès ?

Avant que ne soit révélée la véritable nature du lieu où se déroule la scène, Tati utilise l’incertitude du spectateur pour accroître son acuité à ces détails et ainsi l’habituer à suivre l’action au sein du plan d’ensemble qui demeurera l’échelle réglementaire pour tout le reste du film. Cette circulation du sens est donc avant tout une circulation du regard que Tati favorise par de multiples procédés de stimulation attentionnelle : <span class="keyword" data-video-1="coupledupremierplanobserve_1-2.mp4">le couple du premier plan</span>, se tournant pour regarder les passants et commentant leur défilé à voix basse, aiguise la vigilance du spectateur aux événements de l’arrière-plan ; l’étrangeté de ces <span class="keyword" data-video-1="silhouettesimmobiles_8.mp4">silhouettes immobiles</span> tout au fond du cadre participe elle aussi d’une vision intranquille de l’image ; l’usage de touches de couleur vive détonne sur le fond gris uniforme et agit comme une véritable signalétique (la balayette rouge de <span class="keyword" data-video-1="agententretien_9.mp4">l’agent d’entretien</span> qui cherche en vain quelque poussière dans cet espace immaculé), enfin, la désynchronisation du son et de l’image ou <span class="keyword" data-video-1="gagsonore_10.mp4">les effets d’amplification sonore</span> qui font entendre telle action se déroulant au fond du décor aussi puissamment que ce qui se passe au premier plan, force le spectateur à une postsynchronisation mentale (quelle est donc la source du son que j’entends ?) et ainsi à pénétrer plus avant dans l’image.

Si cette mise en scène, par l’effort d’attention qu’elle exige, met en péril la lisibilité des gags et donc le rire lui-même, elle offre en revanche au spectateur « une liberté nouvelle : celle de pouvoir goûter ou manquer l’effet, qui n’est jamais assené[\[12\]](#_ftn12) ». L’usage simultané des différents procédés de sollicitation attentionnelle, appliqués de façon concomitante à plusieurs personnages répartis dans le plan, éteint le potentiel « autoritaire » de la mise en scène : Tati ne dirige pas le regard du spectateur sur le mode d’une circulation _linéaire_, il le disperse, en lui offrant divers points de focalisation[\[13\]](#_ftn13) – ce dernier est libre de choisir la trame qu’il veut suivre, l’action qu’il veut voir, le personnage qu’il veut adopter comme héros temporaire, enfin, ce à quoi il veut rire.

Si le plan d’ensemble, qui rend possible la coïncidence des gags, est véritablement un plan « démocratique », ce n’est peut-être pas à la manière dont l’entend Tati ; certes, grâce à lui, « le droit de faire rire est désormais reconnu à chacun sur l’écran (quelle que soit sa place dans le film et sa fonction sociale)[\[14\]](#_ftn14) », mais on pourrait néanmoins objecter que cet égalitarisme a quelque chose de factice dans la mesure où il ne se traduit pas par l’entrée, brusque et soudaine, d’une quelconque réalité sociale au sein de l’univers de Tati : le monde de _Playtime_ est entièrement artificiel, les figures qui s’y déplacent sont des archétypes et si elles disent quelque chose de notre réalité c’est par la vertu de la caricature plus que par leur authenticité – loin de faire disparaître son héros, Tati a créé un monde à son image dans lequel chaque situation, chaque geste, chaque recoin de décors appellent l’« hulotisme », la maladresse et le gag ; il le dit lui-même, sans relever le paradoxe, lorsqu’il affirme vouloir « supprimer le vedettariat du personnage comique principal » et offrir « la possibilité pour chacun d’avoir sa demi-heure d’hulotisme[\[15\]](#_ftn15) », prodigalité qui ne tolère d’altérité que conformée à une identité préconçue.

On pourra juger que Jacques Tati se fait là une idée insuffisamment ambitieuse de la démocratie, mais la sentence serait injuste si on ne disait qu’il est aussi de tous les cinéastes celui qui s’est fait la plus haute opinion de son spectateur, sans quoi il n’eût pas tant demandé de son intelligence et de son attention. La structure de _Playtime_ est « aussi décevante qu’elle est riche, écrit Jean-André Fieschi, (décevante à la mesure même de son incroyable richesse, laquelle ne va pas sans quelque obscure frustration) », il en découle « une exigence tout à fait nouvelle vis-à-vis d’un spectateur que le cinéma, d’ordinaire, essaye plutôt de combler par des moyens plus simples et plus éprouvés[\[16\]](#_ftn16) », cette exigence est une liberté acquise (le droit de regarder où l’on veut et de rire quand on le souhaite) certes au détriment de la fraternité du rire (tous en chœur – en cela le film est « décevant ») mais incontestablement pour le profit de la diversité et de la « richesse » des expériences possibles face à l’œuvre. En ce sens, le plan d’ensemble ne permet pas tant, comme le souhaitait Tati, une démocratisation du gag (l’héroïsation de l’homme ordinaire), qu’il instruit une démocratisation du rire : ce ne sont pas les personnages mais les spectateurs qui sont ici concernés et l’agora démocratique de Jacques Tati ne se tient pas dans le hall de l’aéroport mais bien dans la salle de cinéma.

***

[\[1\]](#_ftnref1) Jacques Tati, André Bazin, François Truffaut, « Entretien avec Jacques Tati », _Cahiers du cinéma_, n°83, mai 1958, p. 2.

[\[2\]](#_ftnref2) Bathélémy Amengual, « L’étrange comique de Monsieur Tati », _Cahiers du cinéma_, n° 32, février 1954, p. 33.

[\[3\]](#_ftnref3) Marc Dondey, _Tati_, Éditions Ramsay, Paris, 1989, p. 9.

[\[4\]](#_ftnref4) Jacques Tati, André Bazin et François Truffaut, _loc. cit._

[\[5\]](#_ftnref5) _Ibid._, p. 5.

[\[6\]](#_ftnref6) Barthélémy Amengual, _loc. cit._

[\[7\]](#_ftnref7) Michel Chion, _Jacques Tati_, Éditions des Cahiers du cinéma, Collection « Auteurs », Paris, 1987, p. 21.

[\[8\]](#_ftnref8) Emmanuel Dreux, « Burlesque : quoi de neuf ? À propos de _Playtime_ de Jacques Tati (1967) et de _The Party_ de Blake Edwards (1968) », _L’Art du cinéma_, n° 12, octobre 1996, p. 11.

[\[9\]](#_ftnref9) Michel Chion, _op. cit._, p. 18.

[\[10\]](#_ftnref10) François Ede, Stéphane Goudet, _Playtime_, Éditions des Cahiers du cinéma, Paris, 2002, p. 143.

[\[11\]](#_ftnref11) Jacques Tati, Jean-André Fieschi, Jean Narboni, _Cahiers du cinéma_, n°199, mars 1968, p. 15.

[\[12\]](#_ftnref12) François Ede, Stéphane Goudet, _op. cit._, p. 145.

[\[13\]](#_ftnref13) À ce sujet voir Jean-Marie Schaeffer, _L’Expérience esthétique_, Éditions Gallimard, Collection « NRF Essais », Paris, 2015, pp. 68-69.

[\[14\]](#_ftnref14) François Ede, Stéphane Goudet, _op. cit._

[\[15\]](#_ftnref15) Jacques Tati, Serge Daney, Jean-Jacques Henry, Serge Le Péron, « Entretiens avec Jacques Tati », _Cahiers du cinéma_, n°303, septembre 1979, pp. 17-18.

[\[16\]](#_ftnref16) Jean-André Fieschi, « Le carrefour Tati », _Cahiers du cinéma_, n°199, mars 1968, p. 25.