+++
author = "Natalia Baudoin"
bio = "Natalia Baudoin est artiste designer bolivienne et vénézuélienne, actuellement doctorante SACRe au sein des groupes Symbiose et Soft Matters de ENSADLab. Sa thèse « Design Convergent, Bricologies Symbiotiques » étudie la valorisation de l’artisanat traditionnel en France et en Amérique Latine par le design. Son travail de recherche-création consiste à interroger les domaines d’application de ces savoirs artisanaux autochtones pour les intégrer à notre système de production tout en respectant leur culture et leur cosmologie. Elle travaille également en partenariat avec des banques solidaires en vue de favoriser l’intégration socio-économique des communautés d’artisans d’Amérique Latine."
categories = ["pratique", "design"]
class = "has-aside-content"
contribution = true
cover = "natalia-3/NBaudoin-cover.jpg"
creditsPhoto = "Figure 1 | Photo: Gisbert 1963. Source: Gisbert, T. Arze, S. Cajías, M. <em>Arte Textil y Mundo Andino</em>, éd. Plural, La Paz, Bolivie, 2015, Fig 40.<br>Figure 2 | Source: Gisbert, T. Arze, S. Cajías, M. <em>Arte Textil y Mundo Andino</em>, éd. Plural, La Paz, Bolivie, 2015, p. 29.<br> Figure 3 | Source: Gisbert, T. Arze, S. Cajías, M. <em>Arte Textil y Mundo Andino</em>, éd. Plural, La Paz, Bolivie, 2015, Fig.17."
date = ""
identifiant = "natalia-3"
subtitle = ""
title = "Savoirs artisanaux <span style=\"hyphens: none; white-space: nowrap;\">et co-création</span>"
weight = 3

+++
{{% divclass class="group" %}}
{{% divclass class="group-content" %}}

L'artisanat dans le _Sud global_ est très souvent hérité de cultures anciennes, et indissociable de la cosmologie des peuples qui le pratiquent. L’analyse étymologique du mot cosmologie s’avère révélatrice de ce lien profond entre pratiques artisanales et cosmologie.

En effet, la cosmologie décrit les lois générales qui gouvernent l’univers, c’est-à-dire une « théorie générale de la matière dans l'espace-temps[1](#_ftn1) ». Elle va de pair avec la cosmogonie, du grec cosmo- « monde » et gon- « engendrer », un récit mythologique qui explique la formation de l’univers. La racine commune à ces deux mots _cosmo-_ dérive du mot _cosmos_ qui selon la définition d’Alain Ray exprime _« originellement une idée d'ordre, de mise en ordre, diversement réalisée dans les sens d'« ornement », « forme, organisation d'une chose »._[2](#_ftn2) _»._

Ce détour étymologique nous permet d’affirmer que les mots _cosmos_ et _ornement_ sont étroitement associés. De ce constat nous pouvons déduire que c’est probablement à travers l’artisanat, une pratique matérielle qui fixe et transmet des codes ornementaux, que l’organisation du savoir et des lois de l’univers au sein de diverses cultures s’est articulée. La relation entre les pratiques artisanales et la cosmologie est donc très ancienne. En effet, dans certaines cultures millénaires, la transmission de savoirs passe par des formes d’artisanat. Cette notion de _mise en ordre_ se retrouve clairement dans l'art textile en Bolivie. Selon le missionnaire jésuite Bernabé Cobo :

> Viracocha est le créateur et celui qui met en ordre, qui dans l'une de ses quatre manifestations est lié à l'art du vêtement et du textile, qui ont à voir avec la mise en ordre de l'humanité puisque les différents peuples diffèrent précisément par le vêtement, cela distingue chaque groupe ethnique et le relie à sa propre huaca[3](#_ftn3) montrant les symboles de sa lignée et de son origine. Cobo explique que Viracocha a mis en ordre le monde andin en submergeant les hommes dans la terre, depuis Tiahuanaco, le centre de toutes les origines, pour les envoyer refaire surface à différents endroits sous forme de huacas. Chaque ville porte les vêtements de sa propre huaca. C'est une conception qui tente de réconcilier un dieu supérieur avec les sanctuaires multiples et dispersés[4](#_ftn4).


{{% /divclass %}}
{{% /divclass %}}

{{% divclass class="group" %}}


<div class="group-aside group-only-aside single-only">
<figure>
<img src="/uploads/natalia-3/NBaudoin-Fig1.jpg" loading="lazy">
<figcaption>Figure 1: Enfants chipayas montrant les quipus de leur usage.</figcaption>
</figure>
<figure>
<img src="/uploads/natalia-3/NBaudoin-Fig2.jpg" loading="lazy">
<figcaption>Figure 2: Guide de lecture selon Sansevero.</figcaption>
</figure>
<figure class="sticky-aside" id="natalia-3_fig-3">
<img src="/uploads/natalia-3/NBaudoin-Fig3.jpg" loading="lazy" class="figure-desktop">
<img src="/uploads/natalia-3/NBaudoin-Fig3_column.jpg" loading="lazy" class="figure-phone">

<figcaption>Figure 3: Textile de Llallagua, Bolivie.</figcaption>
</figure>
</div>



<!-- Commentaires de mise en page: L'image apparaît sur la droite à côté de la citation qui commence par "_C'est un paysage mobile..." . L'image disparaît progressivement lorsqu'elle arrive à la fin de la citation de Raul Cabra "laissant la place à des objets hybrides ou nouveaux qui incarnaient un nouveau moment dans le temps."_ -->

{{% divclass class="group-content" %}}

D'autre part, dans le monde andin, l'art textile est également une forme d'écriture. En témoignent les _quipus_, un système de comptabilité textile basé sur des nœuds datant de l'empire Inca, qui est encore utilisé aujourd'hui. Certains chercheurs affirment qu'il s'agit même d'une forme de langage écrit (Figures 1 et 2).


<div class="aside-center">
{{% figure class="" img="natalia-3/NBaudoin-Fig1" %}}Figure 1: Enfants chipayas montrant les quipus de leur usage.{{% /figure %}}
{{% figure class="" img="natalia-3/NBaudoin-Fig2" %}}Figure 2: Guide de lecture selon Sansevero.{{% /figure %}}
</div>

Ainsi, de par son lien à la cosmologie, la production artisanale est conçue de manière holistique et est profondément liée à son contexte. Le geste technique reflète donc le rapport à la nature, le patrimoine culturel, les interactions économiques: en somme, la vision du monde de ceux qui le pratiquent. Selon DeNicola et Wilkinson-Weber, « l'artisanat, comme l'histoire, est un outil que les gens utilisent pour négocier leurs fonctions et leur place dans l'environnement physique et social[5](#_ftn5) ».


Dans certains tissus des Andes, il est possible de lire les caractéristiques physiques de l'environnement et même le statut social de la personne qui l'a tissé. Dans _Arte Textil y Mundo Andino_, les auteures illustrent ce phénomène à travers l'exemple de la zone minière de Llallagua en Bolivie (Figure 3):

<div class="aside-center">
{{% figure class="" img="natalia-3/NBaudoin-Fig3" %}}Figure 3: Textile de Llallagua, Bolivie.{{% /figure %}}
</div>

> C'est un paysage mobile qui répond aux nouveaux schémas sociaux et économiques où les moyens de transport sont représentés, des lamas aux hélicoptères , passant par des motos, des voitures, des trains, des camions. Ceux-ci alternent avec des monstres mythologiques, des étoiles incas, des branches d'olivier, des fleurs baroques, des danseurs, des groupes familiaux, etc. Insérés dans leurs respectifs _tocapus_[6](#_ftn6), ils forment des enchaînements de figures d'une grande variété et inventivité, montrant que le textile n'est pas un art stagnant et archaïque, mais quelque chose de vivant qui répond à la société bolivienne actuelle[7](#_ftn7).

Cet exemple témoigne de la relation étroite entre l'artisanat et son contexte. Il n'est pas seulement défini par les personnes qui la pratiquent mais aussi par leur situation géographique, par leurs croyances religieuses, par d'autres activités économiques régionales ainsi que par leur environnement social. Son évolution est directement liée à l'évolution des artisans en tant qu'individus ou en tant que groupe social.

Le cas de Llallagua nous montre de manière très palpable que les pratiques artisanales traditionnelles ne sont pas statiques mais plutôt fluctuantes et perméables. C'est pourquoi elles s'adaptent aux changements et à l'évolution de leur contexte, permettant leur continuité dans le temps. Elles doivent s'adapter pour se transmettre.

> On entend par traditionnel ce qui est « basé sur la tradition» , cela qualifie un objet d'usage ancien, une pratique ou un événement qui se produit régulièrement, c'est-à-dire une action qui se réalise et se transmet dans le temps. Par conséquent, l’artisanat traditionnel est une pratique manuelle qui est réalisée et transmise au fil du temps[8](#_ftn8).

Ce caractère dynamique de l'artisanat traditionnel permettra la création de ce que Raúl Cabra appelle des _intersections génératives_[9](#_ftn9). Ces intersections sont des moments de l'histoire d'une société dans laquelle les conditions optimales existent pour que des changements se produisent dans _les façons traditionnelles de faire_. Cabra illustre le concept en expliquant comment le contexte socio-économique mexicain peu après la conquête a permis à l'artisanat et aux traditions de ce pays d'être profondément influencés par les traditions asiatiques.

Chaque année, à partir de 1565, le navire connu sous le nom de "La Nao de Chine" ou le galion de Manille arrivait d'Asie au port d'Acapulco chargé de 1,100 tonnes de produits asiatiques très riches et sophistiqués. Avant d'être transportés par voie terrestre jusqu'à la côte atlantique, et de là à Cadiz ou à Séville, des objets et textiles de luxe étaient exposés lors d'une foire de deux mois, tenue à Acapulco. Lors de cette foire, artisans et fabricants de tout le Mexique ont rencontré pour la première fois une multitude d'objets de tradition et de fabrication asiatiques exceptionnelles, et dans certains cas ils ont même pu rencontrer les artisans qui les ont fabriqués. Selon Cabra, ce contact s'est avéré très génératif, car il a influencé la culture matérielle mexicaine, y compris l'artisanat, la nourriture et les vêtements.

> _La technique de la laque japonaise a été ingénieusement adoptée et appliquée à l'ornementation de calebasses, les transformant de pots utilitaires en objets de luxe. De même, le "rebozo", le chale mexicain par excellence portée par de nombreuses femmes autochtones aujourd'hui, serait un hybride du Manton de Manile espagnol, du sari indien et du tissu solaire porté par les femmes mexicaines au moment de l'arrivée de Cortés à la capitale aztèque de Tenochtitlan. De cette manière, les traditions ont changé par un point d'intersection, laissant la place à des objets hybrides ou nouveaux qui incarnaient un nouveau moment dans le temps_[10](#_ftn10)**_._**

{{% /divclass %}}
{{% /divclass %}}

{{% divclass class="group" %}}
{{% divclass class="group-content" %}}

Si, d'une part, les lignes commerciales de la colonie espagnole ont influencé les pratiques traditionnelles mexicaines; les modèles de développement industriel et la mondialisation ont d’autre part créé une rupture avec les pratiques artisanales ancestrales en Amérique Latine. Adelia Borges soutient qu'entre les années 60’s et 80’s, la dévaluation des savoirs artisanaux en Amérique Latine face à l'industrie mondiale croissante était telle qu'une vision s'est développée dans laquelle les traditions manuelles étaient synonymes d'un passé arriéré, de sous-développement et la pauvreté qu'il fallait laisser tomber pour un avenir prometteur, réalisable grâce à la production industrielle avec des machines avec la promesse d'entrer dans le hall des pays développés[11](#_ftn11).

La concurrence avec les produits industriels importés de Chine a non seulement introduit, dans certains cas, des changements formels dans les pratiques artisanales[12](#_ftn12), mais elle a également généré une distorsion dans l'appréciation de la valeur des objets et du travail manuel.

> _Entre 2011 et 2012, j'ai travaillé avec la communauté des vanniers de la ville de Copacabana dans la province de Córdoba en Argentine_[13](#_ftn13). _Don Julio Quinteros, un artisan de 84 ans, était le seul de toute la communauté capable de fabriquer des objets avec le point le plus fin et le plus difficile à faire. Lorsqu'on a demandé à Don Julio pourquoi les autres jeunes artisans ne tissaient pas comme lui, il expliquait qu'aucun d'entre eux ne voulait apprendre à le faire parce que ce n'était pas rentable. Un panier de point gros et de grande taille se vend plus facilement et plus cher qu'un panier ou un objet fait avec un point plus fin et plus petit. Cependant, le panier de point plus fin prend beaucoup plus de temps de réalisation. Don Julio a expliqué que les acheteurs ne comprenaient pas qu'il était plus difficile de fabriquer le petit panier, et qu'ils préféraient acheter un panier plus grand à un prix inférieur. La plupart des clients venant de la ville ne distinguaient pas la différence de qualité du tressage (le tressage fin est aussi plus résistant car plus compact) et avec l'argument de la taille ils avaient tendance à dévaloriser le travail des artisans. En raison de l'âge avancé de Don Julio et de son état de santé délicat, son savoir-faire mourra probablement avec lui car aucun des jeunes artisans ne voulait apprendre de lui à fabriquer des tressages plus complexes_[14](#_ftn14).

A cela s'ajoutent des pratiques d'appropriation culturelle qui copient et commercialisent des produits issus du savoir-faire ancestral des communautés. Au cours des dix dernières années, les pratiques d'appropriation culturelle sont devenues monnaie courante, notamment dans l'industrie de la mode. L'ONG mexicaine Impacto[15](#_ftn15) a enregistré au moins deux douzaines de cas de plagiat rien qu'au Mexique depuis 2009[16](#_ftn16), dénonçant, entre autres, de grandes marques comme Zara, Carolina Herrera, Hermès ou Louis Vuitton. Ces marques couvrent des secteurs du marché mondial auxquels les artisans ne peuvent aspirer, générant des millions de dollars de royalties, sans reconnaissance de l'origine culturelle des produits et sans que les organisations d’artisans producteurs participent au modèle économique.

Il est évident que dans ces conditions, il est très difficile pour les artisans de faire vivre leurs familles. Pour cette raison et à cause des difficultés qu'ils rencontrent pour atteindre les bons segments de marché pour leurs produits, les artisans sont associés aux couches pauvres de la population. Par conséquent, les savoirs artisanaux traditionnels sont encore aujourd'hui souvent considérés comme des pratiques marginales, inappropriées et décontextualisées du monde à la fois socialement et économiquement.

Malgré ce contexte, les artisans ont un besoin urgent d’ajouter de la valeur à leurs produits tout en respectant leur culture, leurs valeurs et leur cosmologie. Cependant, dans un système industrialisé, basé sur une approche hyper-économique, comment ces pratiques peuvent-elles trouver leur place? L'alliance avec le design a apporté quelques réponses à cette question. Une approche classique du design peut contribuer à la valorisation de la production artisanale en concevant des objets qui utilisent ces techniques manuelles. Cependant, cela ne suffit pas pour répondre aujourd'hui aux besoins de développement socio-économique des producteurs.

Certaines pratiques de design se concentrent sur les aspects sociaux inhérents au processus de conception. Elles se positionnent, dans certains cas, dans le cadre de l'innovation sociale, que Manzini définit comme un ensemble d’idées qui répondent à des besoins sociaux qui créent en même temps de nouvelles relations ou formes de collaboration. Ainsi, ce que l'on définirait comme design pour l'innovation sociale devrait remettre en question le positionnement des professionnels techniques par rapport aux groupes d'artisans. «_Le design pour l'innovation sociale est tout ce que le design expert fait pour activer, maintenir et guider les processus de changement social qui mènent à la durabilité \[...\] il s'agit de contributions à un débat social sur ce qu'il faut faire et comment le faire_[17](#_ftn17). »

Dans le cadre des projets de développement, c'est une question de la plus haute importance qui déterminera la nature des relations entre designers et artisans. Par exemple, une approche classique du design mettra l'artisan dans le rôle d'un simple exécuteur du projet du designer, créant une relation designer-producteur. Ce modèle de production, ajouté à la position sociale (marginale) des artisans a établi une relation hiérarchique entre les designers et les artisans, dans laquelle la connaissance des designers a plus de valeur que celle des artisans. Au contraire, un travail de co-creation dans des conditions égalitaires requiert le consensus des deux parties. C'est ce qui permettra aux artisans de s'approprier et de poursuivre le travail entamé lors des échanges. Dans ce modèle de production, l'artisan peut s'impliquer davantage dans la conception d'un projet, dont l'objectif est d'établir une relation symétrique entre le designer et l'artisan et de créer des connaissances à partir de l'échange réalisé.

Janete Costa, designer brésilienne spécialisée dans le travail avec les artisans, explique les défis du processus de co-création de la manière suivante:

> _On ne peut pas avoir un projet fini avant de connaître les artisans. Il faut créer un consensus entre votre opinion et la leur. Et ils doivent comprendre la raison de l'intervention pour pouvoir continuer le travail plus tard. \[…\] : «Lorsque les designers abordent l'artisanat et les artisans, ils doivent se mettre à leur niveau car (les designers) n'ont pas la capacité de faire quoi que ce soit. J'admire quelqu'un qui fait des paniers ou de la broderie et même je peux concevoir quelque chose, mais je ne peux rien faire (manuellement). Cela prendrait des années de formation. Le design est au même niveau, les deux nécessitent de nombreuses années de formation_[18](#_ftn18).

Ce que montre le témoignage de Janete Costa, c'est que le travail de co-création permet de rompre avec la hiérarchie designer-artisan, questionnant ainsi les modèles de production classiques. Costa met en avant la valeur du savoir-faire de l'artisan, le considérant comme un expert au même titre que le designer, générant une relation symétrique. Selon Cabra, cette répartition égalitaire du pouvoir nous permet d'aller au-delà de l'appropriation de la technique artisanale et établit une véritable collaboration entre artisans et designers.

C'est par consensus que la co-création s'inscrit dans le concept _d'hybride_ comme espace de force créatrice dans lequel les différences et les oppositions se négocient, au-delà d'être résolues, en faisant quelque chose de nouveau. «J’entends par hybridation des processus socioculturels dans lesquels des structures ou des pratiques discrètes, qui existaient de façon séparée, se combinent pour engendrer de nouvelles structures, de nouveaux objets et de nouvelles pratiques[_19_](#_ftn19)_._ »

Selon Nestor García Canclini, l'hybride naît de la créativité individuelle et collective non seulement dans les arts, mais aussi dans la vie quotidienne et le développement technologique. Cela implique une dynamique d'inclusion et de rejet, une lutte active entre entités, qui aboutit à l'émergence d'une nouvelle structure. Le modèle de co-création crée de nouvelles structures de collaboration plus équitables.

Travailler avec des artisans soulève également des problèmes liés à la propriété intellectuelle. Le travail réalisé dans un processus de co-création se situe comme une pratique qui, en reconnaissant le savoir-faire de l'artisan, le protège de l'appropriation culturelle. Aujourd'hui, de nombreux designers qui refusent de participer à ces logiques prédatrices ont commencé à remettre en question et à réformer leur propre pratique, passant d'une logique d'imposition (la signature imposée) à une logique de collaboration. Le processus de co-création implique la reconnaissance de droits d'auteur collectifs, et éloigne donc le designer de sa position centrale. Tout comme l'artisanat, le design a évolué en réponse à son nouveau contexte.

Face à la multiplicité des cas d'appropriation culturelle, des mouvements se sont créés pour renforcer les communautés d'artisans producteurs. Par exemple, _l'Initiative sur les Droits de Propriété Intellectuelle Culturelle_ est une organisation qui promeut la reconnaissance des droits de propriété intellectuelle culturelle pour les artisans qui sont les gardiens et les transmetteurs de vêtements traditionnels, de dessins traditionnels et de techniques de fabrication traditionnelles[20](#_ftn20).

L'organisation vise à agir comme médiateur entre les intérêts des entreprises de mode et ceux des artisans et des communautés créatives traditionnels. Ainsi, il propose un modèle d'entreprise de distribution des bénéfices pour favoriser des collaborations socialement et culturellement durables entre artisans et créateurs contemporains de la mode, basé sur une répartition équitable des droits de propriété intellectuelle et des droits de propriété intellectuelle culturelle. Pour cela, il propose deux outils, _la règle des 3C_ et la création de _marques commerciales culturelles_[21](#_ftn21). La première établit trois règles pour travailler avec les artisans: le consentement (libre, préalable et informé, de la communauté artisanale, autochtone ou locale), le crédit (reconnaissance de la communauté d'origine et d'inspiration) et la compensation (monétaire ou non monétaire). Le seconde est un mécanisme qui certifie une commercialisation autorisée et / ou la source des produits culturels, des expressions culturelles traditionnelles et des expressions du folklore d'une communauté, d'une tribu ou d'un groupe.

Ces modèles de collaboration reposent sur une volonté de reconnaître d'autres formes de pensée que celles imposées par le système capitaliste qui, suivant l'idéal moderniste d'un monde universel, tend à effacer les identités et les cultures locales. En ce sens, le design peut être un instrument d'intégration sociale et économique, établissant des méthodologies de création basées sur une relation symétrique et une éthique de travail qui respecte les compétences des producteurs. Selon García Canclini :

> _La première condition pour distinguer les occasions et les limites de l’hybridation est de ne pas transformer l’art et la culture en ressources pour le réalisme magique de la compréhension universelle. Il s’agit plutôt de les placer dans le champ instable, conflictuel, de la traduction et de la “trahison”. Les recherches artistiques sont clés dans cette tâche si elles parviennent à être à la fois langage et vertige_[22](#_ftn22).

C'est ainsi qu'il faut comprendre la collaboration entre le design et l'artisanat: complexe et pleine de frictions. L'accepter avec ses nuances est ce qui peut transformer le modèle de co-création en opportunité.

{{% /divclass %}}
{{% /divclass %}}

***

[1](#_ftnref1) Alain Rey, _Dictionnaire Historique de la Langue Française_, ed. Le Robert, Paris, 2016.

[2](#_ftnref2) _Idem._

[3](#_ftnref3) Les _Huacas_ sont des deités particulières adorées par un _ayllu_, comunauté ou peuple. Elles désignent les astres desquels les ayllus descendaient.

[4](#_ftnref4) Teresa Gisbert, Silvia Arze, Martha Cajías, _Arte Textil y Mundo Andino_, éd. Plural, La Paz, 2015, p. 8.

[5](#_ftnref5) Alicia Ory DeNicola, Clare Wilkinson-Weber, Critical Craft, _Technology Globalization and capitalism_, ed. Bloomsburry, Londres, 2016, p1.

[6](#_ftnref6) Un tocapu o tocapo est un ensembe de carrés avec une décoration géométrique, généralement polychromes, qui sont tissé ou brodés dans des textiles, peint sur des vases et sur des quero (verres cérémoniaux en bois), utilisés pendant la période Inca. (Wikipedia).

[7](#_ftnref7) Teresa Gisbert, Silvia Arze, Martha Cajías, _op. cit._, p. 11.

[8](#_ftnref8) Natalia Baudoin, "Crafting for Change. Dos experiencias de creación participativa en Francia y Argentina", _Economía Creativa_ n°13, mai-octobre 2020, p.75.

[9](#_ftnref9) Raul Cabra, "Oax-i-fornia: Generative Intersections and the Design of Craft", _Iridescent_, 2012, p 17.

[10](#_ftnref10) Ibid, p18.

[11](#_ftnref11) Adelia Borges, "Craft revitalization as a change agent in Latin America", _Making Futures Journal (vol. 3) : Interfaces between craft knowledge and design: new opportunities for social innovation and sustainable practice_, 2013.

[12](#_ftnref12) Selon Adelia Borges, les artisans ont commencé à copier les formes et les «motifs» des objets industriels. “_Des scènes de neige ou d'ours polaires moelleux, de baies et d'autres mets uniques de l'hémisphère nord qui sont apparus sur une variété de produits artisanaux._ "

[13](#_ftnref13) Copacabana est un village dans lequel tous sont vanniers, utilisant une technique autochtone des Comechingones, mais où aucun des vannier ne se reconnait comme appartenant aux Comechingones.

[14](#_ftnref14) Natalia Baudoin, _op. cit._, p. 78-79.

[15](#_ftnref15) [http://impacto.org.mx/<wbr>portfolio-items/<wbr>impacto-comunicacion/](http://impacto.org.mx/portfolio-items/impacto-comunicacion/)

[16](#_ftnref16) Guadalupe Fuentes López, "En 7 años, 23 marcas plagiaron el diseño autóctono de México, y no hay una sola denuncia: activistas", _SinEmbargo_, 2019, disponible en ligne : [https://www.sinembargo.mx/<wbr>22-06-2019/<wbr>3599883](https://www.sinembargo.mx/22-06-2019/3599883) (consulté le 03/12/2020).

[17](#_ftnref17) Ezio Manzini, _Cuando todos diseñan. Una introducción al diseño para la innovación social_, ed. Experimentha Teoría, Madrid, 2015.

[18](#_ftnref18) Janete Costa interviewée para AU – Arquitetura e Urbanismo magazine [http://www.revistau.com.br/<wbr>arquitetura-urbanismo/<wbr>163/<wbr>artigo63519-2.asp](http://www.revistau.com.br/arquitetura-urbanismo/163/artigo63519-2.asp) citée dans Adelia Borges, _Design and Craft. The Brazilian Path_, ed. Terceiro Nome, San Pablo, 2011.

[19](#_ftnref19) Néstor García Canclini, _Cultures hybrides. Stratégies pour entrer et sortir de la modernité_, trad. Francine Bertrand Conzalez, ed. Les Presses de l’Université Laval, Quebec, 2010, p. 19.

[20](#_ftnref20) [https://www.cultural<wbr>intellectual<wbr>property.com/](https://www.culturalintellectualproperty.com/ "https://www.cultural<wbr>intellectual<wbr>property.com/")

[21](#_ftnref21) [https://www.cultural<wbr>intellectual<wbr>property.com/<wbr>mission](https://www.culturalintellectualproperty.com/mission)

[22](#_ftnref22) Néstor García Canclini, _op. cit._, p. 39.