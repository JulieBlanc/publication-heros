+++
author = "Anouk Phéline"
bio = "Ancienne élève de l'ENS Ulm, Anouk Phéline est doctorante en histoire et théorie du cinéma au sein du laboratoire SACRe (ENS-PSL) en cotutelle avec l’Università Degli Studi di Milano. Sa thèse, sous la direction d’Antoine de Baecque et d'Elena Dagrada, est consacrée à l’analyse génétique du film _Voyage en Italie_ de Roberto Rossellini (1954) et s’accompagne de la réalisation d’un essai audiovisuel."
categories = ["analyse", "cinema"]
class = "center-content"
contribution = true
cover = "anouk/fig5.jpg"
creditsPhoto = "Figures 1, 2, 3, 4, 5, 6, 7, 8 et 9 : Roberto Rossellini, <em>Voyage en Italie</em>, 1954 © Coproduction Office. <br> Figures 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 et 23 : Anouk Phéline, <em>Voyage à Maiori</em>, 2021 © Anouk Phéline."
date = ""
identifiant = "anouk"
subtitle = ""
title = "À la rencontre des figurants de *Voyage en&nbsp;Italie*"
weight = 4

+++
Interrogé dans les _Cahiers du cinéma_ sur _Voyage en Italie_, près de dix ans après sa réalisation en 1953, Roberto Rossellini déclare : « J’ai cherché à montrer les peuples méditerranéens, les Latins, comme ils sont dans la réalité et non pas comme les voient les Anglo-Saxons ou les gens du Nord qui viennent nous voir comme des animaux au zoo[1](#_ftn1) ». Une affirmation qui nous incite à revoir le film depuis ses marges, narratives et visuelles, à rebours de l'intrigue sentimentale centrée sur un couple de touristes anglais, les Joyce. Ce _duo_ d'étrangers en voyage à Naples est interprété par l'acteur britannique George Sanders - connu pour ses rôles de séducteur chez Douglas Sirk ou Mankiewicz -, et par l'icône hitchcockienne d'origine suédoise Ingrid Bergman, épouse et muse de Rossellini depuis _Stromboli_ (1950). En Italie, ils se découvrent _étrangers_ l'un à l'autre comme à ce pays qu'ils parcourent sans le comprendre. Pourtant, si l'on en croit le cinéaste, les véritables protagonistes du film ne seraient pas les deux stars _made in_ Hollywood mais la cohorte de personnages secondaires et de figurants italiens qui apparaissent en contrepoint. Ces "héros" très discrets viendraient incarner à l'écran « les Latins comme ils sont dans la réalité » - ou plutôt les Latins tels que les voit Rossellini, réalisateur romain épris de Naples depuis sa jeunesse.

L'opposition qu'il dresse entre les très anglais Alexander et Katherine Joyce - ces « gens du Nord » -, et un peuple méditerranéen emblématique de la latinité, gouverne une partition des rôles entre acteurs professionnels et non professionnels. Rossellini reprend ainsi dans _Voyage en Italie_ une méthode éprouvée de son cinéma néo-réaliste pour matérialiser différents régimes de représentation au sein même de la fiction. Il fait jouer leur propre rôle aussi bien aux nobles napolitains invitant les Joyce à dîner qu’aux ouvriers des fouilles archéologiques de Pompéi ou aux guides qui font visiter à Bergman les principaux sites naturels et culturels de la région. Par petites touches, le film brosse un panorama complexe de la population napolitaine, avec ses hiérarchies sociales et symboliques, loin des types exotiques traditionnellement associés au sud de l’Italie par les récits de voyage[2](#_ftn2).

{{% figure class="" img="anouk/fig1" %}}Figure 1 : La soirée chez le duc de Lipoli (en réalité, chez le duc Lucio Caracciolo d'Aquara). {{% /figure %}}

{{% figure class="" img="anouk/fig2" %}}Figure 2 : La visite au musée archéologique national de Naples. {{% /figure %}}

{{% figure class="" img="anouk/fig3" %}}Figure 3 : Les ouvriers pratiquant des fouilles à Pompéi. {{% /figure %}}

Outre ces personnages qui prennent une part active au récit et disent quelques répliques, plusieurs traversées de Naples en voiture permettent à Rossellini de capter sur le vif les regards caméra des passants dans les rues de la ville. J’ai retravaillé ces fragments documentaires dans un essai audiovisuel intitulé [_Regard contre regard_](https://vimeo.com/anoukpheline/regardcontreregard) (2020) afin de mettre en évidence la confrontation avec l’altérité consubstantielle à un tel dispositif de tournage. Par le montage, Rossellini orchestre un subtil renversement de perspective : à leur tour, les Latins fixent ceux qui « viennent \[les\] voir comme des animaux au zoo ». Voilà donc qu'ils leur résistent et leur font face _dans l'image_. Ce sont eux qui soudain _nous_ regardent.

Dans la dernière scène du film, le cinéaste affirme pleinement cette volonté de donner à voir son peuple non seulement à travers quelques figures isolées mais dans sa dimension collective. Tout un village y célèbre en grande pompe une procession à la Vierge, forçant les Joyce à sortir de leur voiture pour se mêler à la foule. Une femme crie alors au miracle et Katherine se voit emportée par les « torrents de peuple[2bis](#_ftn2bis) » qui affluent vers la Madone. S’agit-il d’un document presque anthropologique sur ce rite religieux et culturel particulièrement important dans la région, ou d’une pure et simple mise en scène pour les besoins du film ? Rossellini produit-il un mythe ou montre-t-il ce qui est ? Se peut-il qu’il fasse les deux à la fois ?

<figure class="procession-group">
<div class="procession-container">
<div class="procession-img">
<img src="/uploads/anouk/fig4.jpg" class="single-only" loading="lazy">
<img src="/uploads/anouk/fig4_column.jpg" class="column-only" loading="lazy">
</div>
<div class="procession-video">
<video controls loop class="videoPlay lazy" poster="/uploads/anouk/fig4.jpg">
<source data-src="/uploads/anouk/procession1_crop_800.mp4" type="video/mp4">
Sorry, your browser doesn't support embedded videos.
</video>
<button class="click-unmuted">Cliquez pour entendre le son</button>
</div>
</div>
<figcaption>Figure 4 : La scène finale de la procession.</figcaption>
</figure>

<figure class="procession-group">
<div class="procession-container">
<div class="procession-img">
<img src="/uploads/anouk/fig5.jpg" class="single-only" loading="lazy">
<img src="/uploads/anouk/fig5_column.jpg" class="column-only" loading="lazy">
</div>
<div class="procession-video">
<video controls loop class="videoPlay lazy" poster="/uploads/anouk/fig5.jpg">
<source data-src="/uploads/anouk/procession2_crop_800.mp4" type="video/mp4">
Sorry, your browser doesn't support embedded videos.
</video>
<button class="click-unmuted">Cliquez pour entendre le son</button>
</div>
</div>
<figcaption>Figure 5 : Ingrid Bergman emportée par la foule.</figcaption>
</figure>

Le seul témoignage direct sur cette procession dont nous disposions dans la somme des publications sur _Voyage en Italie_ est celui du chef opérateur Enzo Serafin, et de sa femme Marisa, dans un entretien avec Annick Bouleau daté de 1989. Trente-six ans après le tournage, Serafin évoque spontanément la scène finale du film tant il fut frappé par son caractère quasiment documentaire, qu'il juge révélateur de la méthode rossellinienne :

> <span class="name">Enzo Serafin</span> Pour la séquence finale, on lui avait dit \[à Rossellini\] qu’il y avait cette procession ; c’était à Maiori, près d’Amalfi. Dans un certain sens, _Voyage en Italie_ est presque un documentaire. Parce que lui, grâce à ses amitiés, à son habileté de charmeur de serpent, savait toujours là où il allait se passer quelque chose. \[…\]
>
> <span class="name">Marisa Serafin</span> C’est comme le miracle à la fin de la procession.
>
> <span class="name">Enzo Serafin</span> Il y a quelqu’un qui ne marchait pas et qui s’est mis à marcher.
>
> <span class="name">Annick Bouleau</span> Donc ce n’est pas une scène jouée ?
>
> <span class="name">Enzo Serafin</span> Non, ce n’est pas une scène qui fait partie de la mise en scène du film. Mais Rossellini était tellement lié à la magie (rires) qu’il pouvait arriver n’importe quoi ! Tous les gens que vous voyez dans la scène, c’est un vrai public, il n’y a ni comparses ni figurants ! Mais c’est vrai aussi qu’il y a beaucoup de miracles dans cette région-là[3](#_ftn3)...

Leurs réponses croisées semblent catégoriques. Non seulement Rossellini aurait filmé une procession bien réelle — et non pas organisée pour l’occasion —, mais il n’aurait jamais interféré avec l’évènement. La "magie" du moment aurait ainsi consisté en l’enregistrement des faits _tels qu'ils sont_, sans préparation, scénarisation ni figuration. Ce compte-rendu est pourtant contradictoire avec les nombreux _travellings_ et mouvements de grue nécessaires à la réalisation de la scène, qui ont forcément demandé la mise en place d’une lourde infrastructure. L’intrication physique entre les acteurs et le public suppose aussi qu’il y ait eu une coordination d’ensemble pendant le tournage. C’est face à ces incohérences que l’idée d'effectuer une enquête de terrain a commencé à voir le jour : et s’il était encore possible de recueillir les souvenirs de certains participants à la procession ? En regardant la séquence attentivement, j’ai remarqué la présence de nombreux enfants et jeunes gens qui devaient avoir aujourd’hui entre soixante-quinze et quatre-vingt-dix ans. J’ai alors pensé qu’en me rendant sur les lieux, à Maiori, je pourrais exhumer la mémoire collective de ce petit village de la côte amalfitaine et, peut-être, éclaircir le mystère du « miracle » qui s’y est produit.

{{% figure class="" img="anouk/fig6" %}}Figure 6 : Enfants de Maiori participant à la procession finale.
{{% /figure %}}

{{% figure class="" img="anouk/fig7" %}}Figure 7 : Jeunes gens de Maiori participant à la procession finale.
{{% /figure %}}

Le hasard a voulu que Maiori vienne à moi lors de mon premier séjour de recherche à Rome pour consulter les archives relatives à la genèse de _Voyage en Italie_.<span style="white-space: nowrap;">M. Crescenzo</span> Paolo <span style="white-space: nowrap;">Di Martino</span>, l’archiviste qui supervisait la salle d’étude à l’Archivio dello Stato, s’est avéré avoir une connaissance intime de mon sujet de recherche, liée à ses origines _maioresi_. Si lui-même n’était pas né au moment du tournage, il m’a confié le récit que sa famille lui en avait transmis oralement :

> <span class="name">Crescenzo Di Martino</span> Mes grands-parents, mon oncle et une bonne partie de ma famille ont participé à la scène. À cette époque Rossellini, avait débarqué à Maiori et y avait trouvé son village de cœur. Il disait toujours que c’était un pays de fous, qui était agréable à vivre et à regarder. Quand il a dû organiser la scène de la procession, il a pensé à celle du 15 août parce que c’est la fête patronale de Maiori, célébrée en grande pompe, en l’honneur de la Madonna Assunta. Mais le prévôt de la collégiale de Maiori l’en a empêché parce que cela aurait pu être interprété comme un blasphème. Alors il s’est replié sur la Madonna Addolorata pour la procession. Les gens ont crié “Miracle ! Miracle !” et, d’après le souvenir qu’on en garde à Maiori, quelqu’un a bousculé Ingrid Bergman et l’a presque jetée à terre.
>
> <span class="name">Anouk Phéline</span> Alors la procession a été organisée par Rossellini ?
>
> <span class="name">Crescenzo Di Martino</span> Oui.
>
> <span class="name">Anouk Phéline</span> À Maiori, y a-t-il encore aujourd’hui des personnes âgées qui se souviennent de la scène ?
>
> <span class="name">Crescenzo Di Martino</span> Oui, je peux vous signaler un nom : Agostino Ferraiuolo, qui connaît tout le village. Les vieux _maioresi_ pourront vous dire qui a participé, par exemple l'agent de la circulation Torelli, qui était une véritable institution à Maiori. Il y en a d’autres, on pourrait les reconnaître un par un. Les participants ont reçu un dédommagement, ils étaient très satisfaits. Quand ils voyaient Rossellini, ils lui disaient toujours : quand est-ce que tu fais un autre miracle[4](#_ftn4) ?

{{% figure class="" img="anouk/fig8" %}}Figure 8 : L'agent de la circulation Gennaro Torelli jouant son propre rôle. {{% /figure %}}

{{% figure class="" img="anouk/fig9" %}}Figure 9 : La statue de la Madonna Addolorata.
{{% /figure %}}

Le processus de création décrit par mon interlocuteur différait radicalement de ce que j’avais lu. Pour trancher entre les deux versions, il fallait repérer et recouper des sources supplémentaires. Le dépouillement des revues cinématographique que j’avais entrepris pour documenter la genèse du film m’a conduite à retrouver un précieux entrefilet : « À Maiori, toujours sur la côte amalfitaine, Rossellini a organisé une Procession et a fait redonner la traditionnelle fête de l’Assunta pour son _Viaggio in Italia_. Mille lires ont été distribuées à tous ceux qui récitaient le Rosaire[5](#_ftn5) ». La presse de l’époque confirmait donc l’exactitude de la tradition orale _maiorese_ rapportée par Crescenzo Di Martino. Restait à rencontrer sur place certains des "enfants" de _Voyage en Italie_, comme je les avais surnommés... L’aide de <span style="white-space: nowrap;">M. Agostino</span> Ferraiuolo, cicérone local et grand connaisseur de l’histoire du village, a été décisive pour cette recherche des témoins directs et indirects du tournage. Plusieurs séjours à Maiori m’ont ainsi permis de réaliser une série d'entretiens filmés avec eux afin de collecter des archives orales, avant qu'il soit trop tard. Je voudrais présenter ici quelques portraits tirés de ce riche matériau audiovisuel et donner la parole à l’un des _maioresi_ interviewés, M. Mario Della Mura :

> « Pour _Viaggio in Italia_, j’ai eu la chance d’être chargé de trouver des figurants mais aussi de figurer moi-même dans le film. \[…\] Le principal acteur était Don Pasquale Citarella qui jouait un miraculé se remettant à marcher. \[…\] Ce n’était pas véridique mais c’était si bien fait que le public a adhéré. Ce n’était plus une fiction, il y a eu une complète association des gens de Maiori qui se sentaient heureux d’observer cet évènement miraculeux[6](#_ftn6) ».

Selon lui, la fiction créée par le cinéaste aurait produit un tel _effet de réel_ que les figurants, oubliant leur rôle, seraient devenus un "vrai public", comme le disait le chef opérateur Enzo Serafin. Ainsi, en s'efforçant de montrer « les Latins comme ils sont », Rossellini aurait finalement offert à toute une communauté la possibilité de s’auto-représenter, telle qu'elle se voit et s'imagine.

{{% figure class="" img="anouk/fig10" %}}Figure 10 : Mario Della Mura, le 18 septembre 2020 à Maiori. Né en avril 1932, il avait vingt ans quand il a participé au tournage de _Voyage en Italie_.{{% /figure %}}

{{% figure class="" img="anouk/fig11" %}}Figure 11 : Mario Della Mura, au centre, dans la dernière scène du film.{{% /figure %}}

{{% figure class="" img="anouk/fig12" %}}Figure 12 : Salvatore Abbate, le 16 septembre 2020, à Maiori. Il avait douze ans quand il a participé au tournage de _Voyage en Italie_.{{% /figure %}}

{{% figure class="" img="anouk/fig13" %}}Figure 13 : Salvatore Abbate dans la scène finale de _Voyage en Italie_, appuyé contre le mur derrière son étal de citronnade, en haut à droite de l'image.{{% /figure %}}

{{% figure class="" img="anouk/fig14" %}}Figure 14 : Italo Tramontano, le 18 septembre 2020, à Maiori. Il avait douze ans quand il a assisté à la dernière scène de _Voyage en Italie_.{{% /figure %}}

{{% figure class="" img="anouk/fig15" %}}Figure 15 : Bonaventura Di Martino, le 18 septembre 2020, à Maiori. Il avait huit ans quand il a assisté à la dernière scène de _Voyage en Italie_.{{% /figure %}}

{{% figure class="" img="anouk/fig16" %}}Figure 16 : Gioacchino Di Martino, le 11 mai 2019, à Maiori. Il avait neuf ans quand il a participé à la procession de _Voyage en Italie_ comme figurant, avec sa classe de l'école des sœurs dominicaines.{{% /figure %}}

{{% figure class="" img="anouk/fig17" %}}Figure 17 : Photo de classe de Gioacchino Di Martino à l'école des sœurs dominicaines. Le jeune Gioacchino est le deuxième en partant de la gauche, au deuxième rang.{{% /figure %}}

{{% figure class="" img="anouk/fig18" %}}Figure 18 : Mario Civale, le 19 septembre 2020, à Maiori. Il avait quinze ans quand il a participé à la procession de _Voyage en Italie_ avec l'association de jeunesse Azione Cattolica.{{% /figure %}}

{{% figure class="" img="anouk/fig19" %}}Figure 19 : Mario Civale, Andrea Scannapieco et Gioacchino Di Martino, le 19 septembre 2020, à Maiori. Andrea Scannapieco a aussi participé à la procession de _Voyage en Italie_ avec l'Azione Cattolica.{{% /figure %}}

{{% figure class="" img="anouk/fig20" %}}Figure 20 : Le prêtre Don Nicola Mammato, le 17 septembre 2020, à Maiori, sur la Piazza Raffaele D'Amato. Il se rappelle son père, Baldassare Mammato, figurant dans _Voyage en Italie_.{{% /figure %}}

{{% figure class="" img="anouk/fig21" %}}Figure 21 : Antonio Mammato, le 18 septembre 2020, à Maiori, sur le Corso Reginna. Il se rappelle son père, Baldassare Mammato, figurant dans _Voyage en Italie_.{{% /figure %}}

{{% figure class="" img="anouk/fig22" %}}Figure 22 : Geraldo Buonocore et Ida Del Pizzo, le 20 septembre 2020, à Maiori. Ils avaient dix-sept ans quand ils ont assisté au tournage de _Voyage en Italie_.{{% /figure %}}

{{% figure class="" img="anouk/fig23" %}}Figure 23 : Agostino Ferraiuolo, le 17 septembre 2020, à Maiori. Spécialiste de l'histoire locale, il recueille la mémoire des _maioresi_ depuis des décennies et m'a servi de guide. Qu'il en soit remercié ! {{% /figure %}}

***

[1](#_ftnref1) « Entretien avec Roberto Rossellini », propos recueillis par Jean Douchet, Jean Domarchi et Fereydoun Hoveyda, _Cahiers du cinéma_, n° 133, juillet 1962, cité dans _Roberto Rossellini_, sous la direction d’Alain Bergala et Jean Narboni, Ed. Cahiers du Cinéma, 1990, p. 25.

[2](#_ftnref2) Pour une analyse détaillée de ces stéréotypes, voir la notion de « southernism » dans Giorgio Bertellini, _Italy in Early American Cinema: Race, Landscape, and the Picturesque_, Indiana University Press, Bloomington, 2010.

[2bis](#_ftnref2bis) L'expression est employée par le plus emblématique des voyageurs français en Italie, l'écrivain Marie-Henri Beyle, pour qualifier le public du Teatro San Carlo à Naples, dans Stendhal, _Rome, Naples et Florence_ \[1826\], édité par Pierre Brunel, Paris, Gallimard, coll. « Folio », 2004, p. 318.

[3](#_ftnref3) « Enzo Serafin », entretien recueilli et traduit par Annick Bouleau, dans Alain Bergala, _Voyage en Italie de Roberto Rossellini_, Alain Bergala, Crisnée : Editions Yellow Now, collection « Long métrage », 1990, p. 131.

[4](#_ftnref4) Entretien avec Crescenzo Paolo di Martino à Rome, le 29 janvier 2019. Enregistrement sonore traduit par l’auteure.

[5](#_ftnref5) Antonio Piumelli, « Cinecittà e dintorni », _Film d'oggi,_ n° 15, 15 avril 1953, p. 16. Cet article recoupe celui de Mario Torre dans _L’espresso del Sud_, datant du début avril 1953, republié en facsimilé par Michele Schiavino dans _Costa Diva. Le vie del cinema,_ édition extraordinaire, Comunità Montana amalfitana, 2000.

[6](#_ftnref6) Entretien avec Mario Della Mura à Maiori, le 15 septembre 2020. Enregistrement audiovisuel réalisé par l’auteure.