+++
author = "Jenny Teng"
bio = "Réalisatrice d’un documentaire <em>Tours d’Exil</em> en 2009,  et de cours métrages de fiction <em>Le Plongeon du Homard</em> et <em>Loin de Gangnam</em>, en passant par des vidéos pour le chorégraphe Rachid Ouramdane dans les pièces <em>Surface de Réparation</em>, <em>Loin</em> et des <em>Témoins debouts</em>. Les questions centrales de mon travail sont celles du déracinement, de la mémoire, du présent post-traumatique, qui ont pour prolongement celle de la transmission et de la notion du « chez soi ». Diplômée de Science Po et de la Femis en réalisation, je présente aujourd’hui un projet de recherche dans le cadre de la formation SACRE-FEMIS intitulée : « Le cinéma peut il nous consoler ? » Entre le cinéma, esthétique et sociologie, la thèse se consacre à la minorité chinoise du Cambodge qui a migré en France et aux Etats-Unis.  Quelles sont ses représentations du génocide khmer rouge ? Quelle histoire se fabriquent la deuxième génération pour combler ce « trou » mémoriel, cette déflagration spatio-temporelle ? Le terrain de recherche se situera auprès de cette génération d’exilés des années 70-80, ainsi que de leurs enfants, génération hétéroclite en quête d’une boussole. "
categories = ["cinema", "pratique"]
class = "center-content"
contribution = true
cover = "jenny/image1_palmiers.jpg"
creditsPhoto = "Interview Boune, Gorgone, La Fémis, 96', 2021.  "
date = ""
identifiant = "jenny"
subtitle = "Marche nocturne avec mon cousin le&nbsp;long de Judah Street"
title = "Les survivants, mes&nbsp;héros&#8239;?"
weight = 11

+++
> Je me suis intéressé à l'absence de destin de mon « héros »...  
> Comment est-ce… quand on est déterminé de l'extérieur, quand on se voit assigné un destin ?  
> J'ai donc essayé d'écrire une histoire négative du développement,  
> montrant non comment on devient ce qu'on est,  
> mais comment on devient ce qu'on n'est pas.  
> Et dans ce projet, la question n'a pas été pour moi  
> celle d'un destin individuel,  
> mais celle de l'absence de destin comme condition de masse[\[1\]](#_ftn1).  
> {{% footer %}}Imre Kertesz{{% /footer %}}

Quand il écrit trente ans après la sortie des camps d’Auschwitz et de Buchenwald, _Être sans Destin_ en 1975, Imre Kertesz cherche dans ce roman autobiographique, à restituer non pas un « soi », mais au contraire l'impossibilité du « soi ». C'est donc une définition de l'identité en creux, en hors champ, quand la barbarie et le non sens ont orienté la vie dans un sens qui échappe entièrement aux individus.

Lors de ma récolte de récits de vie auprès de membres de ma famille émigrés en Californie, 40 ans après le génocide des Khmers rouges auquel ils avaient survécu, je découvre des personnes que je ne connaissais pas, personnalités complexes habitées par des forces de vie immenses.

Contrairement à Kertesz, j'étais étonnée de me confronter lors de ces rencontres non à une impossibilité de « soi », mais précisément à la  possibilité "d'invention de  soi", à travers la mise en mots, le récit détaillé d’épisodes chaotiques et tragiques.

Ambivalence de la relation du cinéaste au témoin survivant : plus ce dernier a traversé d’épreuves dont il peut relater les péripéties et détails, plus l’entretien est a priori stimulant. Ce n’est qu’une impression. La distance nécessaire à la réflexivité, la distance juste que le héros met entre lui et son passé bousculé, effondré, celle-ci n’a rien à voir avec la quantité ni la nature des obstacles affrontés au quotidien.

Se présente d’ailleurs une grande difficulté au montage, celle de rendre sensible ce que l’on ressent au contact réel d’une personne dont  la trajectoire nous bouleverse: la force et l'endurance, la témérité, la patience avec laquelle elle a traversé les épreuves.

Et malgré cette rencontre et notre désir de lui donner forme, quelque chose résiste. Nous restons la plupart du temps impuissants à la "mettre en récit", à restituer cette force de  vie et ce parcours extraordinaire, soit pendant le tournage même (à cause de la manière dont s'agencent les questions et les réponses- à accorder nos rythmes, nos enjeux) soit au cours du montage.

Chez l'autre, les outils linguistiques peuvent faire défaut pour exprimer ce qu'il veut, puisque l'entretien ne se fait pas dans sa langue natale, et la mienne aussi, mais que je ne maîtrise pas. Souvent c'est l’émotion qui déborde, soulevant la confusion, la résistance, la distance. Quand je sens qu'une rencontre "fonctionne", c'est comme si on traverse ensemble un territoire qu'on est en train de créer en temps réel, avec ses frontières et ses bordures, ses espaces de partage et de silence. On dépasse l'interaction connue avec tout ce qu'ils charrient de représentations sociales communes. Ce n'est plus mon cousin, je ne suis plus sa cousine, ni la fille de sa tante, ni l'éternelle étudiante de la famille. Dans cette harmonie éphémère, on oublie qui l'on est, ce qu'on représente pour l'autre et ce qu'il représente pour nous, pour se laisser transporter dans le récit épique. Dans son élan narratif,  le survivant se transforme un moment en héros.

Je connais les dates, j’ai lu les témoignages du Père Ponchaud et les enquêtes détaillées de l’historien Chandler, j’ai vu les films de Rithy Panh, retrouvé des vieilles archives de propagande. Toutes ces images et ces informations sur le fonctionnement du régime de Pol Pot volent en éclat au contact de sa parole. Les contours de son récit individuel et ceux de mes connaissances par rapport à cette histoire collective se trament et se confondent dans une séquence qui prend corps au présent.

23 octobre 2019. Marche nocturne aux côtés de mon cousin Boune, avec qui j’ai passé beaucoup de temps enfant. Je le redécouvre l’instant de cette traversée déserte du quartier central d’Inner Sunset, où il partage une maison avec son ex femme, Phonita. Je le suis en caméra portée, il marche d’un pas lent, régulier. Mes questions lui parviennent faiblement, emportées par le diablo, vent chaud et sec de la baie de San Francisco. A quelques pas de la maison, il s’arrête sous le panneau signalétique « Judah Street ». Il dit, entrecoupé du grincement strident du câble car, son souvenir de la beauté du Cambodge, magnétique, mêlé à celui de l’horreur et de sa destruction totale. Le plan séquence dure 5’27 et a été monté entier.

Il raconte le quotidien d’un enfant qui a vu la brutalité et la mort en face sans l’avoir comprise. et qui ne l’a pas comprise sur le moment. Le film naît du trajet qu’il a parcouru depuis, c'est-à dire, pendant ces quarante dernières années, au cours desquelles il a abandonné l’idée de donner un sens à cette barbarie. La conscience s’aiguise pour nous deux : ce qui reste à accepter, ce n’est ni les atrocités commises par les Khmers Rouges ni la disparition des membres de sa famille, mais le fait que cette inhumanité restera pour toujours incompréhensible.

Malgré ce renoncement, de ce bout-à-bout d’une heure ou deux d’entretien a jailli une vérité. Celle de refuser de comprendre ce qui n’est pas compréhensible, de laisser le spectateur aussi perdu et perplexe face à une cicatrice de 20 cm sur l’avant bras, qu’une mère désespérée assène à son enfant de 7 ans avec un couteau de cuisine, pour lui apprendre à ne plus jamais désobéir au régime.

Cette incompréhension en gros plan, sous le panneau de Judah street, c’est celle de l’enfant face à une mère qui est devenue barbare dans un régime tout aussi barbare. Elle s’est laissée contaminer par la violence du régime génocidaire, il ne peut ni lui pardonner ni abandonner sa lutte: construire une relation de confiance avec ses enfants.

<div class="inline-elems">
<img src="/uploads/jenny/image1_palmiers.jpg" loading="lazy" class="single-only">
<img src="/uploads/jenny/image1_palmiers_400.jpg" loading="lazy" class="column-only">
<div class="elem-texte">
<p>
When I remember my childhood, <br>
I remember happiness.  <br>
I remember how green the country was.  <br>
I remember how lively the people were.  <br>
I remember how good food was.
</p>
</div>
</div>

<div class="inline-elems">
<img src="/uploads/jenny/image2_rueSF.jpg" loading="lazy" class="single-only">
<img src="/uploads/jenny/image2_rueSF_400.jpg" loading="lazy" class="column-only">
<div class="elem-texte">
<p>
I roll in a mug, like a pig, and i get up from the mud, <br>
the water washes the mud, feel so clean and refreshing.<br>
The air is so crisp, the cloud so white and soft.<br>
You know, Life.<br>
But then, you see how mean and evil people was to each other.
</p>
</div>
</div>

<div class="inline-elems">
<img src="/uploads/jenny/image3_profil_boune.jpg" loading="lazy" class="single-only">
<img src="/uploads/jenny/image3_profil_boune_400.jpg" loading="lazy" class="column-only">
<div class="elem-texte">
<p>
During the war, it was nothing but death and destruction.<br>
But even with death and destruction,<br>
we had the mansoun rain.<br>
When the water on the lily petals,<br>
the sound makes a music.
</p>
</div>
</div>

<div class="inline-elems">
<img src="/uploads/jenny/image5_judah_street.jpg" loading="lazy" class="single-only">
<img src="/uploads/jenny/image5_judah_street_400.jpg" loading="lazy" class="column-only">
<div class="elem-texte">
<p>
We were hungry, they were starving us.<br>
I was walking around the area, and I saw a cucumber.<br>
I looked around, I picked the cucumber and I ate it as fast as possible. By the time I got back to my hut, the soldiers already came. They pull us to the public square for execution. For execution.<br>
They were making an annoucement of execution for stealing. And my mother beg and beg, for forgiveness.
</p>
</div>
</div>

<div class="inline-elems">
<img src="/uploads/jenny/image6_boune_cicatrice.jpg" loading="lazy" class="single-only">
<img src="/uploads/jenny/image6_boune_cicatrice_400.jpg" loading="lazy" class="column-only">
<div class="elem-texte">
<p>
And the wife of the commander of the base overheard us. She came and my mother said she was a seamstress, she could sew for her, and the wife said : let them go !<br>
My mum drag me by the arm, back to the hut, she take a knife, a « pang toh », she pull my hand out, and she slip. I was really skinny. It went all the way to the bone. You can see the bone.
</p>
</div>
</div>

Le montage du film a été guidé par les étapes du voyage des rescapés. Chacun de ces parcours ressemble à une épopée avec ses temps propres, ses pivots dramatiques, ses pertes et ses surprises.

Malgré les singularités de chaque trajectoire, une structure dramatique collective se dessine : l’expulsion des villes vers les campagnes, puis la vie quotidienne qui s’installe dans un régime totalitaire, enfin le départ depuis les camps de réfugiés pour l’étranger.

Lorsque le régime ordonne l’évacuation des villes, il est alors impossible de comprendre ce qui va se passer. Tous les détails sont restés en mémoire comme des indices où l’agonie et la faim pouvaient déjà se lire.

_J’ai pris ma couverture en laine, et j’ai bien fait, il faisait si froid la nuit, quand on a évacué Phnom Penh._

Après l’exode vers les campagnes, d’abord aux alentours puis de plus en plus loin jusqu’au nord du pays à la frontière thaïlandaise, ils réalisent la brutalité du nouveau monde et son irréversibilité : les choses ne redeviendront plus jamais comme avant. Ils ne retrouveront ni leur maison ni leur famille. Le choc de cette prise de conscience soudaine, sourde, est amorti par l’épuisement et la nécessité de se nourrir, de s’habiller, de dormir, tout en travaillant à la rizière ou aux champs du matin au soir. Pas de place pour l’introspection ni pour l’atermoiement, encore moins pour la révolte. Mais à l’intérieur, quelque chose gronde. Ils me diront cette colère quarante ans après. Alors qu’ils ne pensent pas survivre, ils mettent de côté tout sentiment de soi. Les épreuves sont atomiques, coriaces, impensables. Elles atteignent la chair et le souffle. L’âme cherche des refuges impossibles.

Le dernier chapitre du film relate la sortie dans les camps de réfugiés. Un sentiment ambivalent habite alors les survivants. Abîmés jusqu’à la moelle, ils constatent pourtant qu’ils sont encore debout, miraculeusement capables de tenir sur leurs deux jambes, capables peut-être même d’affronter la suite : l’exil et l’installation dans un nouveau pays où il faudra tout reprendre de zéro… apprendre une nouvelle langue, trouver un travail, se faire de nouveaux amis.

Une cousine m’interpelle : « Comment évaluer aujourd‘hui ce que l’horreur nous a appris ? »

_Ainsi la violence demeure. Le mal qu’on m’a fait est en moi. Il est là, puissant. Il me guette. Il faut bien des années, bien des rencontres, bien des larmes, bien des lectures pour le dompter. Je n’aime pas ce matin sanglant et, trente ans après, je n’aime pas le raconter : ce n’est pas la honte, c’est l’hésitation. \[2\]_

Pour Rithy Panh, c’est un poids pour les survivants de raconter. Ce n’est pas qu’ils en sont incapables, mais ils savent qu’en partageant leur histoire, l’autre pourra la remanier et la mettre en scène. C’est prendre le risque de se faire déposséder une nouvelle fois de leur histoire. La raconter implique de déformer les faits, en accentuant certains évènements, en créant des saillies artificielles dans le tissu continu de la vie quotidienne.

Inconsciemment, le cinéma cherche à révéler cette figure du survivant héros. C’est parce qu’ils ont vu de leurs propres yeux la barbarie, à laquelle ils ont échappé de peu, et qu’ils se sont relevés de tout, qu’ils nous intéressent. Le montage organise le récit dans ce sens, poussant la personne du témoin jusqu’aux extrémités de sa parole, jusqu’aux rebords de son être, pour le mouler, le faire entrer dans l’étoffe du héros.

Mon cousin Boune ne peut pas être réduit à l’épisode du concombre volé. Par hasard, cette séquence nocturne a été montée exactement à la 45ème minute, au milieu du film, comme si elle avait pris toute seule, de manière organique, sa place de climax dramatique.

Boune n’est peut-être pas un héros mais je me suis rendue compte que s’il avait une place centrale dans le projet et dans le film, c’est parce que son récit est exemplaire. Il a arrêté son pas dans cette rue sombre de San Francisco pour crier l’injustice infligée à tous les enfants dont l’enfance a été dérobée par le régime Khmer rouge. Il a pris la parole au nom de tous, son récit vaut pour tous.

***

[\[1\]](#_ftnref1) « Le vingtième siècle est une machine à liquider permanente », entretien d'Imre Kertész avec Gerhard Moser (traduit de l'allemand par Bernard Franco) dans _Parler des camps, penser les génocides_, textes réunis par Catherine Coquio, Albin Michel, Paris, 1999, p. 90.

[\[2\]](#_ftnref2) Rithy Panh avec Christophe Bataille, _L’Élimination,_ Grasset, Paris, 2012