+++
author = "Léandre Bernard-Brunel"
bio = "Diplômé des Beaux-Arts de Paris et historien de formation, Léandre Bernard-Brunel développe un travail à la lisière des arts plastiques et du cinéma. Ses films et installations vidéos ont été montrés notamment au festival Cinéma du réel, au Musée des beaux-arts d’Orléans, à la Maison européenne de la photographie ou encore à la Biennale de Belleville. Sa recherche par la pratique dans le cadre du doctorat SACRe/PSL se compose de trois récits phénoménologiques abordant pratiques vernaculaires, strates de paysages urbains, traités de géométrie, poésie et textures du temps en Inde."
categories = ["analyse", "cinema"]
class = "center-content"
contribution = true
cover = "leandre-2/Evangelia-Kranioti_cover.jpg"
creditsPhoto = "Evangelia Kranioti, <em>Obscuro Barroco</em>, Tropical Underground, France, Grèce, 59 minutes, 2018."
date = ""
identifiant = "leandre-2"
subtitle = "Retour sur *Obscuro Barroco* un film d’Evangelia Kranioti"
title = "Dévoration"
weight = 17

+++
Le carnaval, ce monde inversé, qui va traverser le globe pour s’épanouir à son plus haut degré au Brésil est la scène où se déploie le film d’Evangelia Kranioti. Photographe, artiste plasticienne, elle est l’auteure d’un précédent film, _Exotica, Erotica, Etc_[1](#_ftn1). Ce premier long-métrage était le fruit d’une immersion sur des cargos marchands parcourant le monde. Comme dans l’opus précédent, la cinéaste grecque scelle sa présence avec ceux qu’elle filme dans un souci brûlant de vivre ces autres vies qui deviennent alors siennes. Dans ce miroir de l’incarnation et de la carnation, sa caméra — aimante à plus d’un titre — parvient à saisir au gré des flots de processions, le flottement des regards qui furtivement s’échappent des corps carnavalesques.

{{% figure class="" img="leandre-2/Evangelia-Kranioti" %}}{{% /figure %}}


[_Obscuro Barroco_](https://vimeo.com/259275959) est un objet d’infinies transformations et de dévorations. La bouche de Luana Muniz, figure incontournable du monde de la nuit à Rio de Janeiro est la porte d’entrée dérobée de cette incandescence[2](#_ftn2). Le film s’ouvre par le ballet suave d’une végétation tropicale luxuriante en lutte avec la brise marine et cédant sous une pluie lourde. La beauté théâtrale de ces arbres sempervirents est si intense qu’elle en parait artificielle. S’énonce, dès ces images, la promesse d’un renversement de perspective où cette nature elle-même participe à l’esprit de farce. Après tout, ces feuillages, peut-être nous les avons déjà ingérés ? Ils seront tantôt le décorum pailleté d’un char de carnaval, tantôt le véhicule en volute d’un paradis artificiel qu’une bonne âme guérisseuse soufflera à notre visage.

Ce qui se joue dans ce film, c’est notre faculté à la métempsycose, cette capacité inouïe et étrange que nous avons à pouvoir faire migrer notre âme vers, sur et enfin dans d’autres corps. Une véritable affaire de cinéma. Comment pourrait-on oublier cette sublime larme filant sur une peau poudrée ? Au milieu de cette foule aux mille éclats, un Auguste lunaire semble avoir perdu ses ailes et son désir. Il lévite — ironie douce — dans les télécabines suspendues de Rio qui surplombent les ruelles des favélas. Silhouette triste de Commedia, tâche lactée, il sera notre fil muet terrestre, lui autrefois si céleste. Sa déambulation, à contretemps, arrête nos regards : une main de Fatima en pendentif, un tatouage de Ganesh sur un bras, un groupe d’enfants épuisés après une transe stroboscopique.

Au milieu de ses moments de fêtes collectives au coeur d’une ville mutante, la figure de Luana Muniz réveille des territoires intimes : le sien et celui du monde _queer,_ trans et cisgenre de la cité. De bout en bout, la voilà qui happe la caméra, insuffle sa gestuelle, la relâche pour la saisir à nouveau. Sans nul doute, c’est elle qui conduit la danse, plante carnivore, comme elle se qualifie, maîtrisant les processus d’entropie.

La force d’Evangelia Kranioti est d’avoir accepté cet envoûtement et de lui avoir offert en échange un texte magnifique de Julia Kristeva : _Etrangers à nous-mêmes_[3](#_ftn3). On jurerait que celui-ci émane de Luana Muniz, qu’il est son témoignage introspectif, jusqu’à découvrir sa source dans le générique. Ce texte plane par dessus la ville comme autour du corps de son interprète à la voix suave, grave et spectrale. Luana Muniz, décédée depuis, donne tant à voir, car derrière la nudité offerte à la caméra, se révèle toujours et encore le millefeuille d’autres métamorphoses à venir. En exposant ainsi un monde où les corps sont des trappes sans fin ouvertes sur mille autres corps possibles, Evangelia Kranioti convoque un geste profondément baroque et résolument politique.

Aussi, lorsque le film glisse du carnaval aux protestations contre le pouvoir conservateur, l’on saisit immédiatement que l’un et l’autre s’enchâssent puissamment. L’affranchissement des genres et des identités, le bonheur du travestissement permanent ou festif, tendent vers le même horizon que l’exigence démocratique : le refus d’un _fatum_ anthropologique. En amont de cette bascule politique, des corps réinventés pour l’occasion par la cinéaste, magiques et fluorescents, réactivent dans l’obscurité une partition sensuelle où toutes les prédations semblent enfin abolies.

Le « Novo Mundo » en néon, vu sur le toit d’un hôtel, invite alors à saisir subtilement la force du métissage et son lien ambigu et douloureux au Baroque, jeu de miroitements infinis. Celui-ci nait trois générations après la découverte des Amériques, conséquence des reflets troublants entre ces mondes d’altérité. La dévoration ici réciproque et joyeuse à l’oeuvre dans la chair même du film d’Evangelia Kranioti témoigne de la promesse d’une perpétuelle réinvention des rites, des formes et des signes pour faire guérison. L’étonnant face à face chorégraphique final le confirme par un _play-back_ renversant. Ici, la bouche de l’héroïne du film attrape au vol les paroles d’_Une vie en rose_ remixée. La voici alors aussitôt avalée par la _camera obscura_ d’Evangelia Kranioti. À moins que ça ne soit l’inverse.

« Nous sommes tous des cannibales. Après tout, le moyen le plus simple d'identifier autrui à soi-même, c'est encore de le manger », écrivait Claude Lévi-Strauss il y a vingt-cinq ans dans la _Républicca_[4](#_ftn4).

Léandre Bernard-Brunel, 31 janvier 2018


***

[1](#_ftnref1)Evangelia Kranioti, _Exotica, Erotica, Etc,_ Aurora Film, France, Grèce, 73 minutes, 2015.

[2](#_ftnref2)Evangelia Kranioti, _Obscuro Barroco,_ Tropical Underground, France, Grèce, 59 minutes, 2018. [https://vimeo.com/259275959](https://vimeo.com/259275959 "https://vimeo.com/259275959")

[3](#_ftnref3) Julia Kristeva, _Etrangers à nous-mêmes,_ Fayard, Paris, 1988.

[4](#_ftnref4) Claude Lévi-Strauss, _« Siamo tutti cannibali »_, _La Repubblica_, 10 octobre 1993.