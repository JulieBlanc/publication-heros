+++
date = ""
title = "Crédits"

+++

Une publication du groupe de recherche de&nbsp;La&nbsp;Fémis, réalisée au&nbsp;sein du&nbsp;programme [«&#8239;Sciences, Arts, Création, Recherche&#8239;» (SACRe,&nbsp;PSL)](http://sacre.psl.eu/).

![](/uploads/logo-sacre.png)

<span class="credit">Contributeurs et contributrices</span> Anouk Baldassari-Phéline, Natalia Baudoin, Léandre Bernard-Brunel, Maxime Berthou, Amanda Gann, Christophe Guérin, Clémence Hallé, Dimitri Martin Genaudeau, Rémi Sagot-Duvauroux, Joseph Minster, Clément Schneider, Jenny Teng, Barbara Turquier

<span class="credit">Direction du projet éditorial</span> Barbara Turquier

<span class="credit">Conception graphique</span> Julie Blanc, avec Christophe Guérin et Natalia Baudoin

<span class="credit">Développement du site</span> [Julie Blanc](https://julie-blanc.fr/) (EUR ArTeC / Université Paris 8 - EA349 / EnsadLab)

<span class="credit">Typographie</span> _Rosenthal_ de David Jonathan Ross <br>_PT Sans_ de ParaType

<span class="credit">Code source du site</span> [https://gitlab.com/JulieBlanc/publication-heros](https://gitlab.com/JulieBlanc/publication-heros)

<span class="credit">Remerciements</span> Nathalie Coste Cerdan, Sandra Delacourt, Clément Marguerite, Fabien Milochevitch, Caroline San Martin

<span class="credit">Date de publication</span> 2021

<span class="credit">Contact</span> b.turquier@femis.fr

  